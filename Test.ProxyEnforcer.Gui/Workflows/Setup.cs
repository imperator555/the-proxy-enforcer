﻿using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects;

namespace Test.ProxyEnforcer.Gui.Workflows
{
    internal class Setup_
    {
        private readonly ProxyEnforcerGuiApp app;

        private Setup_(ProxyEnforcerGuiApp app)
        {
            this.app = app;
        }

        public static Setup_ Setup(ProxyEnforcerGuiApp app)
        {
            return new Setup_(app);
        }

        public void ClearSystemProxy()
        {
            var configTab = app.MainWindow.ProxyConfigsTab;
            configTab.AddPreset(Preset.EmptySystemProxy);
            configTab.UseProxyButton.Click();
        }

        public void EnforceSystemProxy(ProxyConfig proxy)
        {
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.AddPreset(new Preset("enforceSystemPreset",
                new PluginState[] { new PluginState("System", true) },
                proxy));
            configsTab.Plugins.TrySelectPlugin("System");
            configsTab.Settings.PluginProperties.SetProperty(PluginPropertyType.EnforcingEnabled, true);
            configsTab.Settings.PluginProperties.SetProperty(PluginPropertyType.EnforcingCheckInterval, 50);
            configsTab.UseProxyButton.Click();
        }
    }
}
