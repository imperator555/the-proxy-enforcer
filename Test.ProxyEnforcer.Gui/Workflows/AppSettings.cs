﻿using System;
using System.IO;

namespace Test.ProxyEnforcer.Gui.Workflows
{
    internal static class AppSettings
    {
        public static void PurgeConfig()
        {
            File.Delete(GetPortableConfigPath());
            File.Delete(GetLocalConfigPath());
        }

        public static void CreateInvalidConfig()
        {
            File.WriteAllText(GetPortableConfigPath(), "invalidXML");
            File.WriteAllText(GetLocalConfigPath(), "invalidXML");
        }

        public static void ReplacePortableConfigWith(string fileName)
        {
            File.Copy($"../../../TestData/{fileName}", GetPortableConfigPath(), true);
            File.Copy($"../../../TestData/default.appdata.xml", GetLocalConfigPath(), true);
        }

        private static string GetPortableConfigPath() =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"ProxyEnforcer\config.xml");

        private static string GetLocalConfigPath() =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"ProxyEnforcer\appdata.store");
    }
}
