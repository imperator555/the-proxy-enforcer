﻿using Test.ProxyEnforcer.Gui.Objects;
using Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab;

namespace Test.ProxyEnforcer.Gui.Workflows
{
    internal class GoWorkflow
    {
        private readonly ProxyEnforcerGuiApp app;

        private GoWorkflow(ProxyEnforcerGuiApp app)
        {
            this.app = app;
        }

        public static GoWorkflow Go(ProxyEnforcerGuiApp app)
        {
            return new GoWorkflow(app);
        }

        public ProxyConfigs ToProxyConfigsTabAndSelect(string preset)
        {
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.Presets.TrySelectConfigByName(preset);

            return configsTab;
        }
    }
}
