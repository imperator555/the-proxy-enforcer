﻿using Test.ProxyEnforcer.Gui.Objects;

namespace Test.ProxyEnforcer.Gui.Workflows
{
    internal static class Startup
    {
        public static ProxyEnforcerGuiApp StartAppWithSimpleConfig()
        {
            AppSettings.ReplacePortableConfigWith("simple.xml");
            return ProxyEnforcerGuiApp.Start();
        }

        public static ProxyEnforcerGuiApp StartAppWithoutConfig()
        {
            AppSettings.PurgeConfig();
            return ProxyEnforcerGuiApp.Start();
        }

        public static ProxyEnforcerGuiApp StartAppWithInvalidConfig()
        {
            AppSettings.CreateInvalidConfig();
            return ProxyEnforcerGuiApp.Start();
        }
    }
}
