﻿using ProxyEnforcer.Network;
using ProxyEnforcer.Network.Proxy;

namespace Test.ProxyEnforcer.Gui.Workflows
{
    internal class External
    {
        public static void SetSystemProxy(string address, int port)
        {
            ProxyService.SetSystemProxyConfig(new ProxyConfig()
            {
                Address = address,
                Port = port,
                Enabled = true
            });
        }
    }
}
