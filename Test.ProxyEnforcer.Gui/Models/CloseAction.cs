﻿namespace Test.ProxyEnforcer.Gui.Models
{
    internal enum CloseAction
    {
        CloseApp = 0,
        CloseWindow = 1,
        Prompt = 2
    }
}
