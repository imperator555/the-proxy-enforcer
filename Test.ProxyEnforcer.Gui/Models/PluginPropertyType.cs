﻿using System.Collections.Generic;
using System.Linq;

namespace Test.ProxyEnforcer.Gui.Models
{
    internal class PluginPropertyType
    {
        public static PluginPropertyType EnforcingEnabled = new PluginPropertyType("Use enforcing:", "Boolean");
        public static PluginPropertyType EnforcingCheckInterval = new PluginPropertyType("Enforcing check interval:", "Number");

        private PluginPropertyType(string name, string type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; }
        public string Type { get; }

        public static PluginPropertyType FindByName(string name)
        {
            return All()
                .Where(x => x.Name == name)
                .FirstOrDefault();
        }

        private static IEnumerable<PluginPropertyType> All()
        {
            return new List<PluginPropertyType>()
            {
                EnforcingEnabled,
                EnforcingCheckInterval
            };
        }
    }
}
