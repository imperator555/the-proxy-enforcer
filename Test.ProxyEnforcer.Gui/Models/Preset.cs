﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.ProxyEnforcer.Gui.Models
{
    internal readonly struct Preset : IEquatable<Preset>
    {
        public Preset(string name, IList<PluginState> plugins, ProxyConfig proxyConfig)
        {
            Name = name;
            Plugins = plugins;
            ProxyConfig = proxyConfig;
        }

        public string Name { get; }

        public IList<PluginState> Plugins { get; }

        public ProxyConfig ProxyConfig { get; }

        public static Preset EmptySystemProxy
        {
            get
            {
                return new Preset("EmptySystemProxy",
                    new PluginState[] { new PluginState("System", true) },
                    new ProxyConfig(false, "", "", false, "", Array.Empty<string>()));
            }
        }

        public bool Equals(Preset other)
        {
            return Name == other.Name &&
                Plugins.SequenceEqual(other.Plugins) &&
                ProxyConfig.Equals(other.ProxyConfig);
        }
    }
}
