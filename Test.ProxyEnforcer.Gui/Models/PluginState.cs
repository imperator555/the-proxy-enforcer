﻿using System;

namespace Test.ProxyEnforcer.Gui.Models
{
    internal readonly struct PluginState : IEquatable<PluginState>
    {
        public PluginState(string name, bool enabled)
        {
            Name = name;
            Enabled = enabled;
        }

        public bool Enabled { get; }

        public string Name { get; }

        public bool Equals(PluginState other)
        {
            return Name == other.Name && Enabled == other.Enabled;
        }
    }
}
