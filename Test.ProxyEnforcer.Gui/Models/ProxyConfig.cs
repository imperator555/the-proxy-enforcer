﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.ProxyEnforcer.Gui.Models
{
    internal readonly struct ProxyConfig : IEquatable<ProxyConfig>
    {
        public ProxyConfig(bool enabled, string address, string port, bool useAutoConfigScript, string autoConfigScript, IList<string> exceptions)
        {
            Enabled = enabled;
            Address = address;
            Port = port;
            UseAutomaticConfigScript = useAutoConfigScript;
            AutoConfigScript = autoConfigScript;
            Exceptions = exceptions;
        }

        public bool Enabled { get; }

        public string Address { get; }

        public string Port { get; }

        public bool UseAutomaticConfigScript { get; }

        public string AutoConfigScript { get; }

        public IList<string> Exceptions { get; }

        public bool Equals(ProxyConfig other)
        {
            return Enabled == other.Enabled &&
                Address == other.Address &&
                Port == other.Port &&
                UseAutomaticConfigScript == other.UseAutomaticConfigScript &&
                AutoConfigScript == other.AutoConfigScript &&
                Exceptions.SequenceEqual(other.Exceptions);
        }

        public bool IsDefault()
        {
            return !Enabled &&
                Address == string.Empty &&
                Port == string.Empty &&
                !UseAutomaticConfigScript &&
                AutoConfigScript == string.Empty &&
                ContainsDefaultEmptyException();
        }

        private bool ContainsDefaultEmptyException()
        {
            return Exceptions.Count == 1 && Exceptions[0] == string.Empty;
        }
    }
}
