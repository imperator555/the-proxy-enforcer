﻿using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects;
using Test.ProxyEnforcer.Gui.Workflows;

namespace Test.ProxyEnforcer.Gui.Cases
{
    internal class StartupTests
    {
        [Test]
        public void AppShouldStartWithoutConfigFiles()
        {
            using var app = Startup.StartAppWithoutConfig();

            Assert.IsTrue(app.MainWindow.Exists());
        }

        [Test]
        public void WhenStartingWithoutConfig_ConfigListShouldContainAnEmptyConfig()
        {
            using var app = Startup.StartAppWithoutConfig();
            var proxyTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsTrue(proxyTab.Presets.TrySelectConfigByName("no proxy"));

            Assert.IsTrue(proxyTab.Settings.GetConfig().IsDefault());
        }

        [Test]
        public void WhenTheConfigFileGetsCorrupted_AppShouldStartWithDefaultProxyConfigs()
        {
            using var app = Startup.StartAppWithInvalidConfig();
            var configTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsTrue(configTab.IsDefault());
        }

        [Test]
        public void WhenTheConfigFileGetsCorrupted_AppShouldStartWithDefaultSettings()
        {
            using var app = Startup.StartAppWithInvalidConfig();
            var settingsTab = app.MainWindow.SettingsTab;

            Assert.IsTrue(settingsTab.IsDefault());
        }

        [Test]
        public void WhenExitingTheAppWithCloseCloseAction_ItShouldTerminate()
        {
            using var app = Startup.StartAppWithInvalidConfig();
            var settingsTab = app.MainWindow.SettingsTab;
            settingsTab.SetDefaultCloseAction(CloseAction.CloseApp);

            app.MainWindow.CloseButton.Click();

            Assert.IsFalse(ProxyEnforcerGuiApp.IsRunning());
        }
    }
}
