﻿using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Workflows;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class NpmProxyPluginTests
    {
        [Test]
        public void WhenApplyingNpmPlugin_ShouldUpdateNpmProxyConfig()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("npm", "test.test.hu", "8080");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("npm");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]
        public void WhenApplyingEmptyProxyConfig_ShouldClearNpmProxyConfig()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("npm", "", "");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("npm");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }
    }
}
