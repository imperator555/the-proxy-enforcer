﻿using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Workflows;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class NugetProxyPluginTests
    {
        [Test]
        public void WhenApplyingNugetPlugin_NugetProxyConfigShouldBeUpdated()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("nuget", "test.test.hu", "8080");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("nuget");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]
        public void WhenApplyingEmptyProxyConfig_NugetProxyConfigShouldBeCleared()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("nuget", "", "");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("nuget");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }
    }
}
