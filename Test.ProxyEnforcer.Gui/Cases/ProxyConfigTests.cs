﻿using NUnit.Framework;
using System.Collections.Generic;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects;
using Test.ProxyEnforcer.Gui.Workflows;
using static Test.ProxyEnforcer.Gui.Workflows.GoWorkflow;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class ProxyConfigTests
    {
        [Test]
        public void WhenAddingANewPreset_ItShouldAppearInThePresetList()
        {
            AppSettings.PurgeConfig();
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            configsTab.NewPresetButton.Click();

            Assert.IsTrue(configsTab.Presets.Contains("new proxy"));
        }

        [Test]
        public void WhenAddingMorePresets_AllGeneratedNamesShouldBeUnique()
        {
            AppSettings.PurgeConfig();
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            configsTab.NewPresetButton.Click();
            configsTab.NewPresetButton.Click();
            configsTab.NewPresetButton.Click();

            Assert.IsTrue(configsTab.Presets.Contains("new proxy"));
            Assert.IsTrue(configsTab.Presets.Contains("new proxy_2"));
            Assert.IsTrue(configsTab.Presets.Contains("new proxy_3"));
        }

        [Test]
        public void SelectingAPreset_ShouldFillThePluginsSection()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            Assert.AreEqual(5, configsTab.Plugins.PluginsList.Count);
            Assert.IsTrue(configsTab.Plugins.IsPluginChecked("System"));
        }

        [Test]
        public void SelectingAPreset_ShouldFillTheSettingsSection()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            Assert.AreEqual("testPreset", configsTab.Settings.Name.Text);
            Assert.AreEqual("example.com", configsTab.Settings.Address.Text);
            Assert.AreEqual("http://auto.example.com", configsTab.Settings.AutomaticConfigurationScriptUrl.Text);
        }

        [Test]
        public void WhenNoPresetsAreSelected_TheDeleteButtonShouldBeInactive()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsFalse(configsTab.DeletePresetButton.IsEnabled);
        }

        [Test]
        public void WhenNoPresetsAreSelected_TheLoadSystemButtonShouldBeInactive()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsFalse(configsTab.LoadSystemProxyButton.IsEnabled);
        }

        [Test]
        public void WhenNoPresetsAreSelected_TheApplyPresetButtonShouldBeInactive()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsFalse(configsTab.UseProxyButton.IsEnabled);
        }

        [Test]
        public void WhenAPresetIsSelected_TheDeleteButtonShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            Assert.IsTrue(configsTab.DeletePresetButton.IsEnabled);
        }

        [Test]
        public void WhenAPresetIsSelected_TheLoadSystemButtonShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            Assert.IsTrue(configsTab.LoadSystemProxyButton.IsEnabled);
        }

        [Test]
        public void WhenAPresetIsSelected_TheApplyPresetButtonShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            Assert.IsTrue(configsTab.UseProxyButton.IsEnabled);
        }

        [Test]
        public void OnStartup_TheSaveAndDiscardConfigsButtonsShouldBeInactive()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            var configsTab = app.MainWindow.ProxyConfigsTab;

            Assert.IsFalse(configsTab.SaveConfigButton.IsEnabled);
            Assert.IsFalse(configsTab.DiscardConfigChangesButton.IsEnabled);
        }

        [Test]
        public void WhenChangingTheCommonProxySettings_TheSaveButtonShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            configsTab.Settings.Name.Text = "Changed";

            Assert.IsTrue(configsTab.SaveConfigButton.IsEnabled);
        }

        [Test]
        public void WhenChangingThePluginSelection_TheSaveButtonShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");

            configsTab.Plugins.GetPlugin("System").CheckBox.Toggle();

            Assert.IsTrue(configsTab.SaveConfigButton.IsEnabled);
        }

        [Test]
        public void WhenDiscardingChanges_AllPresetsShouldRevertBackToTheLastSavedState()
        {
            // init application
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");
            var startingPreset = configsTab.GetCurrentlySelectedPreset();

            // change some values on the selected preset
            configsTab.Plugins.PluginsList[0].CheckBox.Toggle();
            configsTab.Plugins.PluginsList[1].CheckBox.Toggle();
            configsTab.Settings.Name.Text = "xxx";
            configsTab.Settings.Address.Text = "xxxAddress";

            // discard
            configsTab.DiscardConfigChangesButton.Click(Decision.Yes);
            configsTab.Presets.TrySelectConfigByName("testPreset");
            var discardedPreset = configsTab.GetCurrentlySelectedPreset();

            Assert.AreEqual(startingPreset, discardedPreset);
        }

        [Test]
        public void WhenSavingChanges_ChangesShouldBeAvailableOnNextProgramStart()
        {
            Preset? savedPreset = null;

            using (var app1 = Startup.StartAppWithSimpleConfig())
            {
                var configsTab1 = Go(app1).ToProxyConfigsTabAndSelect("testPreset");

                // change some values on the selected preset
                configsTab1.Plugins.PluginsList[0].CheckBox.Toggle();
                configsTab1.Plugins.PluginsList[1].CheckBox.Toggle();
                configsTab1.Settings.Address.Text = "xxxAddress";
                configsTab1.Settings.AutomaticConfigurationScriptUrl.Text = "xxx";
                configsTab1.SaveConfigButton.Click();
                savedPreset = configsTab1.GetCurrentlySelectedPreset();
            }

            using var app2 = ProxyEnforcerGuiApp.Start();
            var configsTab2 = Go(app2).ToProxyConfigsTabAndSelect("testPreset");
            var startingPreset = configsTab2.GetCurrentlySelectedPreset();

            Assert.AreEqual(savedPreset, startingPreset);
        }

        [Test]
        public void WhenDiscardingChanges_SaveAndDiscardButtonsShouldBeInactive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");
            configsTab.Plugins.PluginsList[0].CheckBox.Toggle();

            configsTab.DiscardConfigChangesButton.Click(Decision.Yes);

            Assert.IsFalse(configsTab.SaveConfigButton.IsEnabled);
            Assert.IsFalse(configsTab.DiscardConfigChangesButton.IsEnabled);
        }

        [Test]
        public void WhenSavingchanges_SaveAndDiscardButtonsShouldBeInactive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");
            configsTab.Plugins.PluginsList[0].CheckBox.Toggle();

            configsTab.SaveConfigButton.Click();

            Assert.IsFalse(configsTab.SaveConfigButton.IsEnabled);
            Assert.IsFalse(configsTab.DiscardConfigChangesButton.IsEnabled);
        }

        [Test]
        public void WhenAddingANewPreset_ItsPluginListShouldBeInitialized()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.NewPresetButton.Click();
            configsTab.Presets.TrySelectConfigByName("new proxy");

            Assert.AreNotEqual(0, configsTab.Plugins.PluginsList.Count);
        }

        [Test]
        public void WhenChangingPluginProperties_SaveAndDiscardButtonsShouldBeActive()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");
            configsTab.Plugins.TrySelectPlugin("System");

            configsTab.Settings.PluginProperties.SetProperty(PluginPropertyType.EnforcingEnabled, true);

            Assert.IsTrue(configsTab.SaveConfigButton.IsEnabled);
            Assert.IsTrue(configsTab.DiscardConfigChangesButton.IsEnabled);
        }

        [Test]
        public void WhenAPluginThrowsAnErrorOnApply_ShouldShowAnErrorNotification()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = Go(app).ToProxyConfigsTabAndSelect("testPreset");
            configsTab.Plugins.TrySelectPlugin("debug");
            configsTab.Plugins.ApplyPluginState(new List<PluginState>() 
            { 
                new PluginState("debug", true),
                new PluginState("System", false),
                new PluginState("git global", false)
            });

            configsTab.UseProxyButton.Click();

            Assert.IsTrue(app.MainWindow.NotificationWindow.Text == "An error occured while setting proxy");
        }

        [Test]
        public void WhenAPluginThrowsAnErrorOnGettingCurrentProxy_ShouldShowAnErrorNotification()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("debug");

            Assert.IsTrue(app.MainWindow.NotificationWindow.Text == "An error occured while getting proxy details");
        }
    }
}
