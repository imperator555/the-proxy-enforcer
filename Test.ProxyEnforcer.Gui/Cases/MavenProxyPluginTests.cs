﻿using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Workflows;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class MavenProxyPluginTests
    {
        [Test]
        public void WhenApplyingMavenPlugin_ShouldUpdateMavenProxy()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("maven", "test.test.hu", "8080");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("maven");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]
        public void WhenApplyingEmptyProxyConfig_ShouldClearMavenProxy()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("maven", "", "");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("maven");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }
    }
}
