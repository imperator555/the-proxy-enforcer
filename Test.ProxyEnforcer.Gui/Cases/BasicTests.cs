using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Objects;

namespace Test.ProxyEnforcer.Gui.Cases
{
    public class BasicTests
    {
        [Test]
        public void AppShouldStart()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.Exists());
        }

        [Test]
        public void ProxyConfigsTabExists()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.ProxyConfigsTab.Exists());
        }

        [Test]
        public void CurrentProxyDetailsTabExists()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.CurrentProxyDetailsTab.Exists());
        }

        [Test]
        public void SystemRouteTableTabExists()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.SystemRouteTableTab.Exists());
        }

        [Test]
        public void SettingsTabExists()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.SettingsTab.Exists());
        }

        [Test]
        public void AboutTabExists()
        {
            using var app = ProxyEnforcerGuiApp.Start();
            Assert.IsTrue(app.MainWindow.AboutTab.Exists());
        }
    }
}