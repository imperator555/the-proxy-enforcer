﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects;
using Test.ProxyEnforcer.Gui.Workflows;
using static Test.ProxyEnforcer.Gui.Workflows.Setup_;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class SystemProxyPluginTests
    {
        [Test]
        public void WhenApplyingSystemPlugin_SystemProxyShouldBeUpdated()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            Setup(app).ClearSystemProxy();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.AddPreset(new Preset("newTestPreset",
                new PluginState[] { new PluginState("System", true) },
                new ProxyConfig(true, "test.test.hu", "8080", false, "", Array.Empty<string>())));
            configsTab.UseProxyButton.Click();

            ProxyConfig proxy = GetCurrentSystemProxyConfig(app);

            Assert.AreEqual(true, proxy.Enabled);
            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]
        public async Task WhenEnforcingIsSetToTrueOnSystemProxyPlugin_ChangesToTheSystemProxyShouldBeDiscarded()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            Setup(app).EnforceSystemProxy(new ProxyConfig(true, "test.test.hu", "8080", false, "", Array.Empty<string>()));
            var configsTab = app.MainWindow.ProxyConfigsTab;

            External.SetSystemProxy("nottest.test.hu", 1234);
            await Task.Delay(60);
            ProxyConfig proxy = GetCurrentSystemProxyConfig(app);

            Assert.AreEqual(true, proxy.Enabled);
            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]

        public async Task ApplyingDisabledEnforcingOnSystemPlugin_ShouldTurnEnforcingOff()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            Setup(app).EnforceSystemProxy(new ProxyConfig(true, "test.test.hu", "8080", false, "", Array.Empty<string>()));
            var configsTab = app.MainWindow.ProxyConfigsTab;

            configsTab.AddPreset(new Preset("disableSystemPreset",
                new PluginState[] { new PluginState("System", true) },
                new ProxyConfig(false, "", "", false, "", Array.Empty<string>())));
            configsTab.Plugins.TrySelectPlugin("System");
            configsTab.Settings.PluginProperties.SetProperty(PluginPropertyType.EnforcingEnabled, false);
            configsTab.Settings.PluginProperties.SetProperty(PluginPropertyType.EnforcingCheckInterval, 50);
            configsTab.UseProxyButton.Click();

            await Task.Delay(60);
            ProxyConfig proxy = GetCurrentSystemProxyConfig(app);

            Assert.AreEqual(false, proxy.Enabled);
            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }

        [Test]
        public void WhenSettingEmptyProxyConfig_SystemProxyShouldBeDisabled()
        {
            External.SetSystemProxy("nottest.test.hu", 1234);

            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.AddPreset(new Preset("enforceSystemPreset",
                new PluginState[] { new PluginState("System", true) },
                new ProxyConfig(false, "", "", false, "", Array.Empty<string>())));
            configsTab.UseProxyButton.Click();

            ProxyConfig proxy = GetCurrentSystemProxyConfig(app);

            Assert.AreEqual(false, proxy.Enabled);
            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }

        [Test]
        public void WhenProxyConfigContainsException_SystemProxyExceptionsShouldBeUpdated()
        {
            Assert.Fail("TODO");
        }

        [Test]
        public void WhenSystemProxyExceptionIsSet_AndProxyConfigDoesNotContainException_SystemProxyExceptionsShouldBeCleared()
        {
            Assert.Fail("TODO");
        }

        private static ProxyConfig GetCurrentSystemProxyConfig(ProxyEnforcerGuiApp app)
        {
            var currentProxyDetails = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetails.Plugins.TrySelectPlugin("System");
            return currentProxyDetails.Settings.GetConfig();
        }
    }
}
