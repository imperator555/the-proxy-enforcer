﻿using NUnit.Framework;
using Test.ProxyEnforcer.Gui.Workflows;

namespace Test.ProxyEnforcer.Gui.Cases
{
    [TestFixture]
    internal class GitProxyPluginTests
    {
        [Test]
        public void WhenApplyingGitPlugin_GitProxyConfigShouldBeUpdated()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("git global", "test.test.hu", "8080");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("git global");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("test.test.hu", proxy.Address);
            Assert.AreEqual("8080", proxy.Port);
        }

        [Test]
        public void WhenApplyingEmptyProxyConfig_GitProxyConfigShouldBeCleared()
        {
            using var app = Startup.StartAppWithSimpleConfig();
            var configsTab = app.MainWindow.ProxyConfigsTab;
            configsTab.SetProxyAddressAndPortForPlugin("git global", "", "");

            var currentProxyDetailsTab = app.MainWindow.CurrentProxyDetailsTab;
            currentProxyDetailsTab.Plugins.TrySelectPlugin("git global");
            var proxy = currentProxyDetailsTab.Settings.GetConfig();

            Assert.AreEqual("", proxy.Address);
            Assert.AreEqual("", proxy.Port);
        }
    }
}
