﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects
{
    internal class AboutTab : ElementBase
    {
        public AboutTab(AutomationElement automationElement) : base(automationElement)
        {
            automationElement.Click();
        }
    }
}
