﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects
{
    internal class SystemRouteTableTab : ElementBase
    {
        public SystemRouteTableTab(AutomationElement automationElement) : base(automationElement)
        {
            automationElement.Click();
        }
    }
}
