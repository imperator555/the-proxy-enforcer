﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.CurrentProxyDetailsTab
{
    internal class CurrentProxyDetails : ElementBase
    {
        public CurrentProxyDetails(AutomationElement automationElement) : base(automationElement)
        {
            automationElement.Click();
        }

        public CurrentProxyDetailsPlugins Plugins => new CurrentProxyDetailsPlugins(FindDescendantById("PluginList"));

        public CurrentProxyDetailsSettings Settings => new CurrentProxyDetailsSettings(FindDescendantById("ProxySettings"));
    }
}
