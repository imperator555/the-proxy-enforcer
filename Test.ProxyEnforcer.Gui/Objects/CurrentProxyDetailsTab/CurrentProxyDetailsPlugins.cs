﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.CurrentProxyDetailsTab
{
    internal class CurrentProxyDetailsPlugins : PluginListBase<CurrentProxyDetailsPluginItem>
    {
        public CurrentProxyDetailsPlugins(AutomationElement automationElement) : base(automationElement)
        {
        }
    }
}
