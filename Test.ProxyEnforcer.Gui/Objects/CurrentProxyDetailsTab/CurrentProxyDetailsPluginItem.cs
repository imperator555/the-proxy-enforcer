﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.CurrentProxyDetailsTab
{
    internal class CurrentProxyDetailsPluginItem : ElementBase, IPluginItem
    {
        private readonly Label label;
        private readonly ListBoxItem listBoxItem;

        public CurrentProxyDetailsPluginItem(ListBoxItem listBoxItem) : base(listBoxItem)
        {
            this.listBoxItem = listBoxItem;
            label = listBoxItem.FindFirstChild(cf => cf.ByClassName("Text")).AsLabel();
        }

        public bool IsChecked => false;
        public string Name => label.Text;

        public void Select()
        {
            listBoxItem.Click();
        }
    }
}
