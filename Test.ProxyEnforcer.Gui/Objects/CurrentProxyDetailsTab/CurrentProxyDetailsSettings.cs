﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.CurrentProxyDetailsTab
{
    internal class CurrentProxyDetailsSettings : ProxySettingsBase
    {
        public CurrentProxyDetailsSettings(AutomationElement automationElement) : base(automationElement)
        {
        }
    }
}