﻿using FlaUI.Core.AutomationElements;
using System.Collections.Generic;
using System.Linq;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    public class ProxyConfigsPresets : ElementBase
    {
        public ProxyConfigsPresets(AutomationElement automationElement) : base(automationElement)
        {

        }

        internal bool TrySelectConfigByName(string configName)
        {
            if (Contains(configName))
            {
                var configLabel = FindAllConfigs().Where(lbl => lbl.Text == configName).FirstOrDefault();
                configLabel.Click();
                return true;
            }

            return false;
        }

        internal bool Contains(string configName)
        {
            return FindAllConfigs().Any(x => x.Text == configName);
        }

        internal bool IsDefault()
        {
            var allConfigs = FindAllConfigs();

            return allConfigs.Count == 1 && Contains("no proxy");
        }

        internal string GetSelectedPreset()
        {
            return BaseElement.AsListBox().SelectedItem?.FindFirstDescendant("ProxyConfigName").AsLabel().Text;
        }

        private List<Label> FindAllConfigs()
        {
            return BaseElement.FindAllDescendants(x => x.ByAutomationId("ProxyConfigName"))
                .Select(element => element.AsLabel())
                .ToList();
        }
    }
}