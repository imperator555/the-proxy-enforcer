﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    internal class ProxyConfigsPluginItem : ElementBase, IPluginItem
    {
        private readonly Label label;
        private readonly ListBoxItem listBoxItem;

        public ProxyConfigsPluginItem(ListBoxItem listBoxItem) : base(listBoxItem)
        {
            this.listBoxItem = listBoxItem;
            CheckBox = listBoxItem.FindFirstChild(cf => cf.ByClassName("CheckBox")).AsCheckBox();
            label = listBoxItem.FindFirstChild(cf => cf.ByClassName("Text")).AsLabel();
        }

        public bool IsChecked => (bool)CheckBox.IsChecked;
        public string Name => label.Text;
        public CheckBox CheckBox { get; }

        public void Select()
        {
            listBoxItem.Select();
        }
    }
}
