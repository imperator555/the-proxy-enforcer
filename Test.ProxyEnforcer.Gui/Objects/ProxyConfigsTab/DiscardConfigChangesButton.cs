﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    internal class DiscardConfigChangesButton : ElementBase
    {
        public DiscardConfigChangesButton(Button automationElement) : base(automationElement)
        {

        }

        public bool IsEnabled => BaseElement.IsEnabled;


        internal void Click(Decision yesNo)
        {
            BaseElement.AsButton().Click();
            FindAnywhereByText("Discard changes")
                .FindAllDescendants()[yesNo == Decision.Yes ? 0 : 1]
                .AsButton()
                .Click();
        }
    }
}
