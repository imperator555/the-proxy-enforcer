﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    internal class ProxyConfigsSettings : ProxySettingsBase
    {
        public ProxyConfigsSettings(AutomationElement automationElement) : base(automationElement)
        {

        }

        public TextBox Name => BaseElement.Parent.FindFirstDescendant("Name").AsTextBox();

        public PluginProperties PluginProperties => new PluginProperties(BaseElement.Parent);

        internal void ApplySettings(ProxyConfig proxyConfig)
        {
            Enabled.IsChecked = proxyConfig.Enabled;
            Address.Text = proxyConfig.Address;
            Port.Text = proxyConfig.Port;
            UseAutomaticConfigurationScript.IsChecked = proxyConfig.UseAutomaticConfigScript;
            AutomaticConfigurationScriptUrl.Text = proxyConfig.AutoConfigScript;
            Exceptions.SetExceptionList(proxyConfig.Exceptions);
        }
    }
}