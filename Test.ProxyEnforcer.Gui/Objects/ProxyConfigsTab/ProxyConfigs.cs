﻿using FlaUI.Core.AutomationElements;
using System;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    internal class ProxyConfigs : ElementBase
    {
        public ProxyConfigs(AutomationElement automationElement) : base(automationElement)
        {
            automationElement.Click();
        }

        public Button NewPresetButton => FindDescendantByText("New preset").AsButton();
        public Button DeletePresetButton => FindDescendantByText("Delete preset").AsButton();
        public Button LoadSystemProxyButton => FindDescendantByText("Load System proxy").AsButton();
        public Button SaveConfigButton => FindDescendantByText("Save config").AsButton();
        public DiscardConfigChangesButton DiscardConfigChangesButton => new DiscardConfigChangesButton(FindDescendantByText("Discard config changes").AsButton());
        public Button UseProxyButton => FindDescendantByText("Apply preset").AsButton();
        public Button EnforceProxyButton => FindDescendantByText("Enforce proxy").AsButton();
        public Button ExportConfigButton => FindDescendantByText("Export config").AsButton();
        public Button ImportConfigButton => FindDescendantByText("Import config").AsButton();

        public ProxyConfigsPresets Presets => new ProxyConfigsPresets(FindDescendantById("ProxyConfigList"));
        public ProxyConfigsPlugins Plugins => new ProxyConfigsPlugins(FindDescendantById("PluginList"));
        public ProxyConfigsSettings Settings => new ProxyConfigsSettings(FindDescendantById("proxySettings"));

        public void AddPreset(Preset preset)
        {
            NewPresetButton.Click();
            Plugins.ApplyPluginState(preset.Plugins);
            Settings.Name.Text = preset.Name;
            Settings.ApplySettings(preset.ProxyConfig);
        }

        internal bool IsDefault()
        {
            if (Presets.IsDefault())
            {
                Presets.TrySelectConfigByName("no proxy");
                return Settings.IsDefault();
            }

            return false;
        }

        internal Preset? GetCurrentlySelectedPreset()
        {
            if (Presets.GetSelectedPreset() != null)
            {
                return new Preset(Presets.GetSelectedPreset(), Plugins.GetPluginStates(), Settings.GetConfig());
            }

            return null;
        }

        internal void SetProxyAddressAndPortForPlugin(string pluginName, string address, string port)
        {
            AddPreset(new Preset("newTestPreset",
                new PluginState[] { new PluginState(pluginName, true) },
                new ProxyConfig(true, address, port, false, "", Array.Empty<string>())));
            UseProxyButton.Click();
        }
    }
}
