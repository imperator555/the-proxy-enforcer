﻿using FlaUI.Core.AutomationElements;
using System.Collections.Generic;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab
{
    internal class ProxyConfigsPlugins : PluginListBase<ProxyConfigsPluginItem>
    {
        public ProxyConfigsPlugins(AutomationElement automationElement) : base(automationElement)
        {
        }

        internal void ApplyPluginState(IList<PluginState> plugins)
        {
            foreach (var plugin in plugins)
            {
                GetPlugin(plugin.Name).CheckBox.IsChecked = plugin.Enabled;
            }
        }
    }
}