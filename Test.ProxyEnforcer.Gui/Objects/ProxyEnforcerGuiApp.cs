﻿using FlaUI.Core;
using FlaUI.UIA3;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Test.ProxyEnforcer.Gui.Objects
{
    internal class ProxyEnforcerGuiApp : IDisposable
    {
        public const string MainWindowTitle = "The Proxy Enforcer";

        private readonly Application app;
        private readonly AutomationBase automation;

        private ProxyEnforcerGuiApp()
        {
            app = Application.Launch(@"..\..\..\..\ProxyEnforcer.Gui\bin\Debug\net5.0-windows\ProxyEnforcer.exe");
            automation = new UIA3Automation();
        }

        public static ProxyEnforcerGuiApp Start()
        {
            return new ProxyEnforcerGuiApp();
        }

        public static bool IsRunning() => Process.GetProcessesByName("ProxyEnforcer.exe").Length > 0;

        public MainWindow MainWindow
        {
            get
            {
                for (int i = 0; i < 20; i++)
                {
                    var topLevelWindows = app.GetAllTopLevelWindows(automation);

                    if (topLevelWindows.Length != 0)
                    {
                        var maybeMainWindow = topLevelWindows.Where(w => w.Title == MainWindowTitle).FirstOrDefault();

                        if (maybeMainWindow != default)
                        {
                            return new MainWindow(maybeMainWindow);
                        }
                    }

                    Thread.Sleep(100);
                }

                throw new ApplicationException("Unable to get main window");
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    automation.Dispose();
                    app.Kill();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
