﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Objects.Common;
using Test.ProxyEnforcer.Gui.Objects.CurrentProxyDetailsTab;
using Test.ProxyEnforcer.Gui.Objects.ProxyConfigsTab;

namespace Test.ProxyEnforcer.Gui.Objects
{
    internal class MainWindow
    {
        public MainWindow(Window w)
        {
            Window = w;
        }

        public Window Window { get; }
        public ProxyConfigs ProxyConfigsTab => new ProxyConfigs(Window.FindFirstDescendant("ProxyConfigsTab"));
        public CurrentProxyDetails CurrentProxyDetailsTab => new CurrentProxyDetails(Window.FindFirstDescendant("CurrentProxyDetailsTab"));
        public SystemRouteTableTab SystemRouteTableTab => new SystemRouteTableTab(Window.FindFirstDescendant("SystemRouteTableTab"));
        public SettingsTab SettingsTab => new SettingsTab(Window.FindFirstDescendant("SettingsTab"));
        public AboutTab AboutTab => new AboutTab(Window.FindFirstDescendant("AboutTab"));
        public Button CloseButton => Window.FindFirstDescendant(cf => cf.ByText("Close")).AsButton();
        public NotificationWindow NotificationWindow => new NotificationWindow(Window.ModalWindows[0]);

        public bool Exists() => Window != null;
    }
}
