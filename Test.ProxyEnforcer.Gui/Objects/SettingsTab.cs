﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Models;
using Test.ProxyEnforcer.Gui.Objects.Common;

namespace Test.ProxyEnforcer.Gui.Objects
{
    internal class SettingsTab : ElementBase
    {
        public SettingsTab(AutomationElement automationElement) : base(automationElement)
        {
            automationElement.Click();
        }

        public CheckBox RunAtSystemStartup => FindDescendantById("RunAtSystemStartup").AsCheckBox();
        public CheckBox NotifyOnChange => FindDescendantById("NotifyOnChange").AsCheckBox();
        public CheckBox StartMinimized => FindDescendantById("StartMinimized").AsCheckBox();
        public ComboBox DefaultCloseAction => FindDescendantById("DefaultCloseAction").AsComboBox();

        public void SetDefaultCloseAction(CloseAction value)
        {
            DefaultCloseAction.Select((int)value);
        }

        internal bool IsDefault()
        {
            return !(bool)RunAtSystemStartup.IsChecked &&
                (bool)NotifyOnChange.IsChecked &&
                (bool)StartMinimized.IsChecked;
        }
    }
}
