﻿using FlaUI.Core.AutomationElements;
using Test.ProxyEnforcer.Gui.Models;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    internal class ProxySettingsBase : ElementBase
    {
        public ProxySettingsBase(AutomationElement automationElement) : base(automationElement)
        {

        }

        public CheckBox Enabled => FindDescendantById("Enabled").AsCheckBox();
        public TextBox Address => FindDescendantById("Address").AsTextBox();
        public TextBox Port => FindDescendantById("Port").AsTextBox();
        public CheckBox UseAutomaticConfigurationScript => FindDescendantById("UseAutomaticConfigurationScript").AsCheckBox();
        public TextBox AutomaticConfigurationScriptUrl => FindDescendantById("AutomaticConfigurationScript").AsTextBox();
        public ProxyConfigExceptions Exceptions => new ProxyConfigExceptions(BaseElement);

        public ProxyConfig GetConfig()
        {
            return new ProxyConfig(
                (bool)Enabled.IsChecked,
                Address.Text,
                Port.Text,
                (bool)UseAutomaticConfigurationScript.IsChecked,
                AutomaticConfigurationScriptUrl.Text,
                Exceptions.GetExceptions());
        }

        internal bool IsDefault()
        {
            return GetConfig().IsDefault();
        }
    }
}
