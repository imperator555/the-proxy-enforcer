﻿using FlaUI.Core.AutomationElements;
using System;
using System.Collections.Generic;
using System.Linq;
using Test.ProxyEnforcer.Gui.Models;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    internal class PluginListBase<T> : ElementBase where T : IPluginItem
    {
        public PluginListBase(AutomationElement automationElement) : base(automationElement)
        {
        }

        public IList<T> PluginsList => FindPlugins();

        public bool IsPluginChecked(string pluginName)
        {
            var matchedPlugin = GetPlugin(pluginName);

            if (matchedPlugin == null)
            {
                throw new ApplicationException($"Unable to find plugin by the name: {pluginName}");
            }

            return matchedPlugin.IsChecked;
        }

        public T GetPlugin(string pluginName) =>
            FindPlugins().Where(p => p.Name == pluginName).FirstOrDefault();

        public IList<PluginState> GetPluginStates()
        {
            List<PluginState> result = new List<PluginState>();

            foreach (var pl in FindPlugins())
            {
                result.Add(new PluginState(pl.Name, pl.IsChecked));
            }

            return result.AsReadOnly();
        }

        private List<T> FindPlugins()
        {
            var result = new List<T>();
            var itemContainer = BaseElement.FindAllDescendants(cf => cf.ByClassName("ListBoxItem"));

            foreach (var item in itemContainer)
            {
                result.Add(CreateItem(item.AsListBoxItem()));
            }

            return result;
        }

        internal void TrySelectPlugin(string pluginName)
        {
            FindPlugins()
                .Where(plugin => plugin.Name == pluginName)
                .FirstOrDefault()
                .Select();
        }

        private static T CreateItem(ListBoxItem item)
        {
            return (T)Activator.CreateInstance(typeof(T), item);
        }
    }
}
