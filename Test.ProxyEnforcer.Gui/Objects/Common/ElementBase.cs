﻿using FlaUI.Core.AutomationElements;
using System.Collections.Generic;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    public class ElementBase
    {
        protected ElementBase(AutomationElement baseElement)
        {
            BaseElement = baseElement;
        }

        public AutomationElement BaseElement { get; }

        public bool Exists() => BaseElement != null;

        protected AutomationElement FindDescendantById(string automationId) => BaseElement.FindFirstDescendant(automationId);

        protected AutomationElement FindDescendantByText(string text) => BaseElement.FindFirstDescendant(cf => cf.ByText(text));

        protected AutomationElement FindChildById(string automationId) => BaseElement.FindFirstChild(automationId);

        protected AutomationElement FindChildByText(string text) => BaseElement.FindFirstChild(cf => cf.ByText(text));

        protected IList<AutomationElement> FindAllChildrenById(string automationId) => BaseElement.FindAllChildren(cf => cf.ByAutomationId(automationId));

        protected AutomationElement FindAnywhereByText(string text)
        {
            return FindRootElement().FindFirstDescendant(cf => cf.ByText(text));
        }

        private AutomationElement FindRootElement()
        {
            AutomationElement root = BaseElement;

            while (root.Parent != null && !(root is Window window && window.Title == ProxyEnforcerGuiApp.MainWindowTitle))
            {
                root = root.Parent;
            }

            return root;
        }
    }
}
