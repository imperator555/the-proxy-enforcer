﻿using FlaUI.Core.AutomationElements;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    internal class NotificationWindow : ElementBase
    {
        private readonly Label textLabel;

        public NotificationWindow(AutomationElement baseElement) : base(baseElement)
        {
            textLabel = BaseElement.FindFirstChild(cf => cf.ByClassName("Static")).AsLabel();
        }

        public string Text => textLabel.Text;
    }
}
