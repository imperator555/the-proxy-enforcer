﻿using FlaUI.Core.AutomationElements;
using System.Collections.Generic;
using System.Linq;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    public class ProxyConfigExceptions : ElementBase
    {
        public ProxyConfigExceptions(AutomationElement baseElement) : base(baseElement)
        {
        }

        internal bool IsEmpty()
        {
            var exceptions = FindAllExceptions();

            if (exceptions.Count == 1)
            {
                return exceptions[0].Text == string.Empty;
            }

            return false;
        }

        internal IList<string> GetExceptions()
        {
            List<string> result = new List<string>();

            foreach (var ex in FindAllExceptions())
            {
                result.Add(ex.Text);
            }

            return result.AsReadOnly();
        }

        internal void SetExceptionList(IList<string> exceptions)
        {
            foreach (var exception in exceptions)
            {
                WriteToEmptyTextbox(exception);
            }
        }

        private List<ProxyConfigExceptionItem> FindAllExceptions()
        {
            var result = new List<ProxyConfigExceptionItem>();
            var buttons = FindAllChildrenById("ExceptionDelete");
            var textboxes = FindAllChildrenById("ExceptionName");

            for (int i = 0; i < buttons.Count; i++)
            {
                var deleteButton = buttons[i].AsButton();
                var textbox = textboxes[i].AsTextBox();

                result.Add(new ProxyConfigExceptionItem(textbox, deleteButton));
            }

            return result;
        }

        private void WriteToEmptyTextbox(string exception)
        {
            FindAllExceptions()
                .Where(x => x.Text == string.Empty)
                .FirstOrDefault()
                ?.SetText(exception);
        }
    }
}