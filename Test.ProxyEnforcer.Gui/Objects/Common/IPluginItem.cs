﻿namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    internal interface IPluginItem
    {
        bool IsChecked { get; }
        string Name { get; }

        void Select();
    }
}
