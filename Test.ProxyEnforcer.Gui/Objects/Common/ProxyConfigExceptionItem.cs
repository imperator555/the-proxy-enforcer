﻿using FlaUI.Core.AutomationElements;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    internal class ProxyConfigExceptionItem
    {
        private readonly TextBox textBoxElement;
        private readonly Button deleteButtonElement;

        public ProxyConfigExceptionItem(TextBox textBoxElement, Button deleteButtonElement)
        {
            this.textBoxElement = textBoxElement;
            this.deleteButtonElement = deleteButtonElement;
        }

        public string Text => textBoxElement.Text;

        public void DeleteClick() => deleteButtonElement.Click();

        internal void SetText(string exception)
        {
            textBoxElement.Text = exception;
        }
    }
}