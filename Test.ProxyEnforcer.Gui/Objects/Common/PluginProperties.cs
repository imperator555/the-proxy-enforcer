﻿using FlaUI.Core.AutomationElements;
using System;
using Test.ProxyEnforcer.Gui.Models;

namespace Test.ProxyEnforcer.Gui.Objects.Common
{
    public class PluginProperties : ElementBase
    {
        public PluginProperties(AutomationElement automationElement) : base(automationElement)
        {

        }

        internal void SetProperty(PluginPropertyType property, object value)
        {
            switch (property.Type)
            {
                case "Number":
                    SetNumberProperty(property, (int)value);
                    break;

                case "Boolean":
                    SetBooleanProperty(property, (bool)value);
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private void SetNumberProperty(PluginPropertyType property, int value)
        {
            FindPluginProperty(property.Name).Value.Value.AsTextBox().Text = value.ToString();
        }

        private void SetBooleanProperty(PluginPropertyType property, bool value)
        {
            FindPluginProperty(property.Name).Value.Value.AsCheckBox().IsChecked = value;
        }

        private (Label Caption, AutomationElement Value)? FindPluginProperty(string name)
        {
            var captions = BaseElement.FindAllDescendants(cf => cf.ByAutomationId("PluginPropertyCaption"));
            var values = BaseElement.FindAllDescendants(cf => cf.ByAutomationId("PluginPropertyValue"));

            for (int i = 0; i < captions.Length; i++)
            {
                var caption = captions[i].AsLabel();
                var value = values[i];

                if (caption.Text == name)
                {
                    return (caption, value);
                }
            }

            return null;
        }
    }
}