## Overview
The Proxy Enforcer is a small utility to manage many applications' proxy settings in a single place. 

## Features
* Save and manage all proxies in a single place
* Supported applications: npm, git, nuget, maven
* Change many apps' proxy configuration with a single click from the taskbar
* You have the ability to enforce the system proxy, so no other applcation can change it
* Export your configuration and import it on a different machine

## Releases
v0.1: Initial release

v0.2: changes

* exception addresses added
* configuration import/export added
* proxy enforcing on startup added
* option added to close the app with the main window close button
* new icon
* bugfixes

v0.3: changes

* logging added
* ability to view and edit system route table added
* bugfixes

v0.4: changes

* added support to set proxy for git, npm, nuget and maven
* reworked "Proxy configs" tab
* removed start enforcing on startup feature
* can open settings files with a text editor from the settings tab
* limit the number of running instances to 1
* merged portable and local config files into one
* bugfixes

## Credits and licensing
The Proxy Enforcer is available under GNU GPLv3