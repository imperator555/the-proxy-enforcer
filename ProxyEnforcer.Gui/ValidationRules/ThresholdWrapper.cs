﻿using System.Diagnostics.CodeAnalysis;
using System.Windows;

namespace ProxyEnforcer.Gui.ValidationRules
{
    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Used in XAML code")]
    internal class ThresholdWrapper : DependencyObject
    {
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(int), typeof(ThresholdWrapper), new FrameworkPropertyMetadata(int.MaxValue));

        public int Value
        {
            get => (int)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }
    }
}
