﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.ValidationRules
{
    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Used in XAML code")]
    internal class IpAddressRule : ValidationRule
    {
        public string FieldName { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value is not string sValue || sValue.Length == 0)
            {
                return ValidationResult.ValidResult;
            }

            if (!MatchIPv4Format(sValue))
            {
                return new ValidationResult(false, $"{ FieldName }: Invalid IPv4 address");
            }

            return ValidationResult.ValidResult;
        }

        private static bool MatchIPv4Format(string value)
        {
            var result = Regex.Match(value, @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$");

            if (!result.Success)
            {
                return false;
            }

            var fragemnts = value.Split('.');

            foreach (var fragment in fragemnts)
            {
                var fragmentValue = int.Parse(fragment, CultureInfo.InvariantCulture);

                if (fragmentValue < 0 || fragmentValue > 255)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
