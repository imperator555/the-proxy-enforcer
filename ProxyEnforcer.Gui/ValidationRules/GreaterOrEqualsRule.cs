﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.ValidationRules
{
    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Used in XAML code")]
    internal class GreaterOrEqualsRule : ValidationRule
    {
        public ThresholdWrapper ThresholdWrapper { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string sValue = value as string;

            int metric;

            try
            {
                metric = int.Parse(sValue, cultureInfo);
            }
            catch (FormatException)
            {
                return new ValidationResult(false, $"Invalid integer: {sValue}");
            }
            catch (OverflowException)
            {
                return new ValidationResult(false, $"Value must be lower than {int.MaxValue}");
            }

            if (metric < ThresholdWrapper.Value)
            {
                return new ValidationResult(false, $"Value must be greater than or equal to {ThresholdWrapper.Value}");
            }

            return ValidationResult.ValidResult;
        }
    }
}
