﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.ValidationRules
{
    [SuppressMessage("Microsoft.Performance", "CA1812", Justification = "Used in XAML code")]
    internal class MandatoryRule : ValidationRule
    {
        public string FieldName { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, $"Missing mandatory value: { FieldName }");
            }

            return ValidationResult.ValidResult;
        }
    }
}
