﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProxyEnforcer.Gui.UiModel
{
    public class ProxyPlugin
    {
        public bool IsEnabled { get; set; }
        public string Name { get; set; }

        [XmlIgnore]
        public bool IsActive { get; set; }

        [XmlIgnore]
        public bool IsInactive => !IsActive;

        public IList<PluginProperty> Properties { get; set; } = new List<PluginProperty>();
    }
}
