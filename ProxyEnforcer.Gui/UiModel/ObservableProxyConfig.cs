﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;

namespace ProxyEnforcer.Gui.UiModel
{
    public class ObservableProxyConfig : ObservableProperty
    {
        private static int idSequence = 1;

        private bool isSetForSystemProxy;

        public ObservableProxyConfig()
        {
            UniqeId = idSequence++;
            Exceptions = new ObservableCollection<Item>();
            Plugins = new ObservableCollection<ProxyPlugin>();
            ValidateExceptionsList();
        }

        public ObservableCollection<Item> Exceptions { get; }

        public ObservableCollection<ProxyPlugin> Plugins { get; }

        [XmlIgnore]
        public IEnumerable<ProxyPlugin> EnabledPlugins => Plugins.Where(p => p.IsEnabled).ToList();

        private bool isDirty = true;
        [XmlIgnore]
        public bool IsDirty 
        { 
            get => isDirty; 

            private set
            {
                if (isDirty != value)
                {
                    isDirty = value;
                }
            }
        }

        public bool IsSetForSystemProxy
        {
            get => isSetForSystemProxy;

            set
            {
                if (isSetForSystemProxy != value)
                {
                    isSetForSystemProxy = value;
                    RaisePropertyChanged(nameof(IsSetForSystemProxy));
                }
            }
        }

        private bool allActiveProxiesMatchPreset;
        [XmlIgnore]
        public bool AllActiveProxiesMatchPreset
        {
            get => allActiveProxiesMatchPreset;
            set
            {
                if (allActiveProxiesMatchPreset != value)
                {
                    allActiveProxiesMatchPreset = value;
                    RaisePropertyChanged(nameof(AllActiveProxiesMatchPreset));
                }
            }
        }

        private bool someActiveProxiesMatchPreset;
        [XmlIgnore]
        public bool SomeActiveProxiesMatchPreset
        {
            get => someActiveProxiesMatchPreset;
            set
            {
                if (someActiveProxiesMatchPreset != value)
                {
                    someActiveProxiesMatchPreset = value;
                    RaisePropertyChanged(nameof(SomeActiveProxiesMatchPreset));
                }
            }
        }

        [XmlIgnore]
        public int UniqeId { get; set; }

        private string name;
        public string Name
        {
            get => name;

            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }

        private bool enabled;
        public bool Enabled
        {
            get => enabled;

            set
            {
                if (enabled != value)
                {
                    enabled = value;
                    RaisePropertyChanged(nameof(Enabled));
                }
            }
        }

        private bool automaticDetect;
        public bool AutomaticDetect
        {
            get => automaticDetect;

            set
            {
                if (automaticDetect != value)
                {
                    automaticDetect = value;
                    RaisePropertyChanged(nameof(AutomaticDetect));
                }
            }
        }

        private string address;
        public string Address
        {
            get => address ?? string.Empty;

            set
            {
                if (address != value)
                {
                    address = value;
                    RaisePropertyChanged(nameof(Address));
                }
            }
        }

        private int? port;
        public int? Port
        {
            get => port;

            set
            {
                if (port != value)
                {
                    port = value;
                    RaisePropertyChanged(nameof(Port));
                }
            }
        }

        private bool useAutomaticConfigurationScript;
        public bool UseAutomaticConfigurationScript
        {
            get => useAutomaticConfigurationScript;

            set
            {
                if (useAutomaticConfigurationScript != value)
                {
                    useAutomaticConfigurationScript = value;
                    RaisePropertyChanged(nameof(UseAutomaticConfigurationScript));
                }
            }
        }

        private string autoConfigScript;

        public string AutomaticConfigurationScript
        {
            get => autoConfigScript ?? string.Empty;

            set
            {
                if (autoConfigScript != value)
                {
                    autoConfigScript = value;
                    RaisePropertyChanged(nameof(AutomaticConfigurationScript));
                }
            }
        }

        [XmlIgnore]
        public bool IsSystemProxyConfig { get; set; }

        public void AddException(string exception = "")
        {
            AddExceptionWithoutValidation(exception);
            ValidateExceptionsList();
        }

        public override string ToString()
        {
            return $"[ObservableProxyConfig: Name={Name}; Enabled={Enabled}; AutomaticDetect={AutomaticDetect}; " +
                $"Address={Address}; port={Port}; UseAutomaticConfigurationScript={UseAutomaticConfigurationScript}; " +
                $"AutomaticConfigurationScript={AutomaticConfigurationScript}]";
        }

        internal void MapProperties(ObservableProxyConfig c)
        {
            Address = c.address;
            AutomaticConfigurationScript = c.autoConfigScript;
            AutomaticDetect = c.automaticDetect;
            Enabled = c.enabled;
            IsSetForSystemProxy = c.isSetForSystemProxy;
            Port = c.port;
            UseAutomaticConfigurationScript = c.useAutomaticConfigurationScript;
            SetExceptions(c.Exceptions);
        }

        internal void RemoveException(Item itm)
        {
            itm.PropertyChanged -= OnExceptionChanged;
            Exceptions.Remove(itm);
        }

        internal bool Compare(ObservableProxyConfig other)
        {
            if (other == null)
            {
                return false;
            }

            if (other.AutomaticDetect != AutomaticDetect)
            {
                return false;
            }

            if (other.Enabled != Enabled)
            {
                return false;
            }

            if (other.UseAutomaticConfigurationScript != UseAutomaticConfigurationScript)
            {
                return false;
            }

            if (other.Address != Address || other.Port != Port)
            {
                return false;
            }

            if (other.AutomaticConfigurationScript != AutomaticConfigurationScript)
            {
                return false;
            }

            // in every other case we consider the configs equal
            return true;
        }

        internal void MarkSaved(bool raisePropertyChanged = false)
        {
            var previousValue = IsDirty;

            IsDirty = false;

            if (previousValue && raisePropertyChanged)
            {
                RaisePropertyChanged(nameof(IsDirty));
            }
        }

        protected override void RaisePropertyChanged(string property)
        {
            if (property != nameof(IsDirty))
            {
                IsDirty = true;

                base.RaisePropertyChanged(nameof(IsDirty));
            }

            base.RaisePropertyChanged(property);
        }

        private void OnExceptionChanged(object sender, PropertyChangedEventArgs e)
        {
            ValidateExceptionsList();
            RaisePropertyChanged(nameof(Exceptions));
        }

        private void AddExceptionWithoutValidation(string exception)
        {
            Item item = new Item(exception);
            item.PropertyChanged += OnExceptionChanged;
            Exceptions.Add(item);
        }

        private void AddExceptionWithoutValidation(Item item) => Exceptions.Add(item);

        private void SetExceptions(IEnumerable<Item> items)
        {
            Exceptions.Clear();
            foreach (var item in items)
            {
                AddExceptionWithoutValidation(item);
            }
        }

        private void ValidateExceptionsList()
        {
            if (!HasEmptyExceptionAtTheEnd())
            {
                RemoveEmptyExceptions();

                // add an empty item to the end of the list
                AddExceptionWithoutValidation("");
            }
        }

        private void RemoveEmptyExceptions()
        {
            Exceptions.RemoveAll(x => !x.NonEmpty);
        }

        private bool HasEmptyExceptionAtTheEnd()
        {
            if (!Exceptions.Any())
            {
                return false;
            }

            var last = Exceptions[^1];

            if (last.Text.Trim().Length > 0)
            {
                return false;
            }

            return EmptyExceptions().Count() == 1;
        }

        private IEnumerable<Item> EmptyExceptions() => Exceptions.Where(x => x.Text.Trim().Length == 0).ToList();
    }
}
