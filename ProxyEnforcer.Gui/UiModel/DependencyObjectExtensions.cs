﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.UiModel
{
    internal static class DependencyObjectExtensions
    {
        public static bool IsValid(this DependencyObject obj)
        {
            // The dependency object is valid if it has no errors and all
            // of its children (that are dependency objects) are error-free.
            return !Validation.GetHasError(obj) &&
                LogicalTreeHelper.GetChildren(obj)
                    .OfType<DependencyObject>()
                    .All(IsValid);
        }

        public static IEnumerable<string> GetAllValidationErrors(this DependencyObject obj)
        {
            var list = new List<string>();

            list = LogicalTreeHelper.GetChildren(obj).OfType<DependencyObject>()
                .Select(GetAllValidationErrors)
                .Aggregate(list, (seq1, seq2) => { seq1.AddRange(seq2); return seq1; });

            return Validation.GetErrors(obj)
                .Select(error => (string)error.ErrorContent)
                .Concat(list);
        }
    }
}
