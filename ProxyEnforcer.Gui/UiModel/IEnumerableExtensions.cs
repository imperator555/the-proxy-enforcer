﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProxyEnforcer.Gui.UiModel
{
    public static class IEnumerableExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection)
        {
            ObservableCollection<T> result = new ObservableCollection<T>();

            foreach (var item in collection)
            {
                result.Add(item);
            }

            return result;
        }
    }
}
