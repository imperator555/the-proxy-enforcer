﻿namespace ProxyEnforcer.Gui.UiModel
{
    public class Item : ObservableProperty
    {
        private string text;

        private Item() { }

        public Item(string value)
        {
            text = value;
        }

        public string Text
        {
            get => text;

            set
            {
                text = value;
                RaisePropertyChanged(nameof(Text));
                RaisePropertyChanged(nameof(NonEmpty));
            }
        }

        public bool NonEmpty => Text.Trim().Length > 0;
    }
}
