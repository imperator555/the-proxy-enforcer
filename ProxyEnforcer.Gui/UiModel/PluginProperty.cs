﻿namespace ProxyEnforcer.Gui.UiModel
{
    public class PluginProperty : ObservableProperty
    {
        private string value;

        public PluginProperty(string name, string value, string type)
        {
            Name = name;
            this.value = value;
            Type = type;
        }

        public string Name { get; }
        public string Value
        {
            get => value;
            set
            {
                this.value = value;
                RaisePropertyChanged(nameof(Value));
            }
        }

        public string Type { get; }
    }
}
