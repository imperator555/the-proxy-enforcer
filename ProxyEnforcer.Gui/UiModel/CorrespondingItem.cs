﻿using System.Windows;

namespace ProxyEnforcer.Gui.UiModel
{
    internal class CorrespondingItem
    {
        private static readonly DependencyProperty correspondingItemProperty =
                DependencyProperty.RegisterAttached("CorrespondingItem", typeof(Item), typeof(CorrespondingItem), new PropertyMetadata(null));

        public static void SetCorrespondingItem(DependencyObject obj, Item value)
        {
            obj.SetValue(correspondingItemProperty, value);
        }

        public static Item GetCorrespondingItem(DependencyObject obj)
        {
            return (Item)obj.GetValue(correspondingItemProperty);
        }
    }
}
