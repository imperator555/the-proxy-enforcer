﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace ProxyEnforcer.Gui.UiModel
{
    public static class ObservableCollectionExtensions
    {
        public static void RemoveAll<T>(this ObservableCollection<T> collection, Func<T, bool> predicate)
        {
            var itemsToRemove = collection.Where(predicate).ToList();

            foreach (var itemToRemove in itemsToRemove)
            {
                collection.Remove(itemToRemove);
            }
        }
    }
}
