﻿using ProxyEnforcer.Gui.Services.ProxyPlugins.System;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ProxyEnforcer.Gui.Converters
{
    public class PluginPropertyNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string propertyName = value as string;

            return propertyName switch
            {
                nameof(ProxyCheckInterval) => "Enforcing check interval",
                nameof(EnforcingEnabled) => "Use enforcing",
                _ => propertyName
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
