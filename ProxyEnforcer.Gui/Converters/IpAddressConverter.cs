﻿using ProxyEnforcer.Gui.ValidationRules;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Windows.Controls;
using System.Windows.Data;

namespace ProxyEnforcer.Gui.Converters
{
    public class IpAddressConverter : IValueConverter
    {
        private readonly IpAddressRule validator = new IpAddressRule();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IPAddress ip = value as IPAddress;

            return ip?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is not string sValue || sValue.Length == 0)
            {
                return null;
            }

            return Parse4DigitAddress(sValue);
        }

        private IPAddress Parse4DigitAddress(string value)
        {
            if (validator.Validate(value, CultureInfo.InvariantCulture) != ValidationResult.ValidResult)
            {
                return null;
            }

            var numbers = value.Split('.').Select(x => byte.Parse(x, CultureInfo.InvariantCulture)).ToArray();

            return new IPAddress(numbers);
        }
    }
}
