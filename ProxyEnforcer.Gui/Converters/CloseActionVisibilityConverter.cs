﻿using ProxyEnforcer.Gui.Services.Settings.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace ProxyEnforcer.Gui.Converters
{
    public class CloseActionVisibilityConverter : IValueConverter
    {
        private const string CloseAppString = "Close the application";
        private const string CloseWindowString = "Close main window only";
        private const string PromptString = "Prompt for action";

        public static List<string> PossibleValues =>
            new List<string>(new string[] { CloseAppString, CloseWindowString, PromptString });

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((CloseAction)value) switch
            {
                CloseAction.CloseApp => CloseAppString,
                CloseAction.CloseWindow => CloseWindowString,
                _ => PromptString,
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((string)value) switch
            {
                CloseAppString => CloseAction.CloseApp,
                CloseWindowString => CloseAction.CloseWindow,
                _ => CloseAction.Prompt,
            };
        }
    }
}
