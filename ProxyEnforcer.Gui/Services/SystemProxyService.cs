﻿using ProxyEnforcer.Gui.Services.ProxyPlugins.System;
using ProxyEnforcer.Gui.UiModel;

namespace ProxyEnforcer.Gui.Services
{
    internal interface ISystemProxyService
    {
        event SystemProxyPlugin.SystemProxyChangedEventHandler OnSystemProxyChanged;

        ObservableProxyConfig GetSystemProxyConfig();
        void SetSystemProxyConfig(ObservableProxyConfig proxy);
    }

    internal class SystemProxyService : ISystemProxyService
    {
        private readonly SystemProxyPlugin system = new SystemProxyPlugin();

        public event SystemProxyPlugin.SystemProxyChangedEventHandler OnSystemProxyChanged
        {
            add
            {
                system.OnSystemProxyChanged += value;
            }

            remove
            {
                system.OnSystemProxyChanged -= value;
            }
        }

        public ObservableProxyConfig GetSystemProxyConfig() =>
            system.GetActiveProxyConfig();

        public void SetSystemProxyConfig(ObservableProxyConfig proxy) =>
            system.SetProxyConfig(proxy);
    }
}
