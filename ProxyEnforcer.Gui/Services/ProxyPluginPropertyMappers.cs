﻿using ProxyEnforcer.Gui.Services.ProxyPlugins;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal static class ProxyPluginPropertyMappers
    {
        internal static IList<UiModel.PluginProperty> ToUiProxyProperties(IEnumerable<Settings.Model.PluginProperty> properties)
        {
            return properties
                .Select(prop => new UiModel.PluginProperty(prop.Name, prop.Value, prop.Type))
                .ToList();
        }

        internal static IList<UiModel.PluginProperty> ToUiProxyProperties(IEnumerable<ProxyPluginProperty<object>> properties)
        {
            return properties
                .Select(prop =>
                        new UiModel.PluginProperty(prop.Name, prop.Value.ToString(), prop.Type.ToString()))
                    .ToList();
        }

        internal static IEnumerable<Settings.Model.PluginProperty> ToSettingsPluginProperties(IList<UiModel.PluginProperty> properties)
        {
            return properties
                .Select(prop => new Settings.Model.PluginProperty()
                {
                    Name = prop.Name,
                    Value = prop.Value,
                    Type = prop.Type
                })
                .ToList();
        }
    }
}
