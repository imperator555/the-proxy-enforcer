﻿using System.Security.Principal;

namespace ProxyEnforcer.Gui.Services
{
    internal static class Utils
    {
        public static bool IsUserElevated() =>
            WindowsIdentity.GetCurrent().Owner.IsWellKnown(WellKnownSidType.BuiltinAdministratorsSid);
    }
}
