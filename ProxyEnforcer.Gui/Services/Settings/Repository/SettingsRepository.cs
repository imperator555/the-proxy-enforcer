﻿using NLog;
using ProxyEnforcer.Gui.Services.Settings.Adapter;
using ProxyEnforcer.Gui.Services.Settings.Model;
using System;
using System.Runtime.Serialization;

namespace ProxyEnforcer.Gui.Services.Settings.Repository
{
    internal class SettingsRepository
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly SettingsAdapter settingsAdapter = new();
        private readonly PortableSettingsAdapter portableSettingsAdapter = new();
        private readonly RunAtStartupAdapter runAtStartupAdapter = new("ProxyEnforcer");

        internal AppSettings LoadAppSettings()
        {
            ConfigFileSettings cfgSettings = null;
            bool resetSettings = false;

            try
            {
                cfgSettings = settingsAdapter.LoadObject();
            }
            catch (Exception e) when (e is SerializationException || e is XmlObjectException)
            {
                resetSettings = true;
                logger.Error(e, "An error occurend while parsing the settings files");
            }

            AppSettings settings = new();
            settings.RunAtStartup = runAtStartupAdapter.IsCurrentStartupEnabled();

            if (resetSettings)
            {
                SaveAppSettings(settings);
            }
            else
            {
                settings = new(cfgSettings.PortableSettings, cfgSettings.LocalSettings);
            }

            return settings;
        }

        internal void SaveAppSettings(AppSettings settings)
        {
            PortableSettings portable = MapPortableSettings(settings);
            LocalSettings local = MapLocalSettings(settings);
            settingsAdapter.SaveObject(new ConfigFileSettings()
            {
                LocalSettings = local,
                PortableSettings = portable
            });

            if (runAtStartupAdapter.IsCurrentStartupEnabled() != settings.RunAtStartup)
            {
                if (settings.RunAtStartup)
                {
                    runAtStartupAdapter.AddApplicationToCurrentUserStartup();
                }
                else
                {
                    runAtStartupAdapter.RemoveApplicationFromCurrentUserStartup();
                }
            }
        }

        internal void SavePortableSettingsTo(AppSettings settings, string fileName)
        {
            PortableSettings portable = MapPortableSettings(settings);

            portableSettingsAdapter.SaveObject(portable, fileName);
        }

        internal AppSettings LoadPortableAppSettingsFrom(string fileName)
        {
            PortableSettings portable = portableSettingsAdapter.LoadObject(fileName);
            var localSettings = settingsAdapter.LoadObject();
            localSettings.PortableSettings = portable;

            return new AppSettings(localSettings.PortableSettings, localSettings.LocalSettings);
        }

        internal static string GetSettingsLocation() =>
            SettingsAdapter.GetFileLocation();

        private static LocalSettings MapLocalSettings(AppSettings appSettings)
        {
            return new LocalSettings()
            {
                LastActiveProxyName = appSettings.LastActiveProxyName,
                LastSystemProxyConfig = appSettings.LastSystemProxyConfig,
                DefaultCloseAction = appSettings.DefaultCloseAction,
                EnforcingEnabled = appSettings.EnforcingEnabled,
                HidingLocalRoutesOnRouteTableEnabled = appSettings.HidingLocalRoutesOnRouteTableEnabled,
                SystemProxyAutoRefreshEnabled = appSettings.SystemProxyAutoRefreshEnabled,
                SettingsEditorApp = appSettings.SettingsEditorApp
            };
        }

        private static PortableSettings MapPortableSettings(AppSettings appSettings)
        {
            var portable = new PortableSettings()
            {
                StartMinimized = appSettings.StartMinimized,
                ShowNotificationOnProxyChange = appSettings.ShowNotificationOnProxyChange
            };

            portable.ProxyList.AddRange(appSettings.ProxyList);
            return portable;
        }
    }
}
