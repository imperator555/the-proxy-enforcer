﻿using NLog;
using ProxyEnforcer.Gui.Services.Settings.Model;
using System;

namespace ProxyEnforcer.Gui.Services.Settings.Adapter
{
    internal class SettingsAdapter : XmlAdapter<ConfigFileSettings>
    {
        private const string ConfigFileName = "config.xml";
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();

        internal override ConfigFileSettings LoadObject(string fileName = null)
        {
            try
            {
                return base.LoadObject(GetConfigFileAbsolutePath(ConfigFileName));
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw new XmlObjectException("Failed to load portable settings", e);
            }
        }

        internal override void SaveObject(ConfigFileSettings obj, string fileName = null)
        {
            try
            {
                base.SaveObject(obj, GetConfigFileAbsolutePath(ConfigFileName));
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw new XmlObjectException("Failed to save portable settings", e);
            }
        }

        internal static string GetFileLocation() => GetConfigFileAbsolutePath(ConfigFileName);
    }
}
