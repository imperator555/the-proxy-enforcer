﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ProxyEnforcer.Gui.Services.Settings.Adapter
{
    public class XmlAdapter<T>
    {
        internal virtual T LoadObject(string file)
        {
            if (!ConfigFileIsMissing(file))
            {
                return ParseSettings(file);
            }
            else
            {
                return default;
            }
        }

        internal virtual void SaveObject(T obj, string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using var sw = new Utf8StringWriter();
            serializer.Serialize(sw, obj);

            FileInfo fi = new FileInfo(fileName);
            if (!fi.Directory.Exists)
            {
                System.IO.Directory.CreateDirectory(fi.DirectoryName);
            }

            using var file = new StreamWriter(fileName, false, Encoding.GetEncoding("UTF-8"));
            file.Write(sw);
        }

        protected static string GetConfigFileAbsolutePath(string path) =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), $"ProxyEnforcer\\{path}");

        private static T ParseSettings(string path)
        {
            using var reader = XmlReader.Create(path, new XmlReaderSettings() { XmlResolver = null });
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(reader);
        }

        private static bool ConfigFileIsMissing(string path) =>
            !File.Exists(path);

        private sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }
    }
}
