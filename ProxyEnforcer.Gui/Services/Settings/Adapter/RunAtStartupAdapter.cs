﻿using Microsoft.Win32;
using System.Diagnostics;

namespace ProxyEnforcer.Gui.Services.Settings.Adapter
{
    internal class RunAtStartupAdapter
    {
        private const string StartupKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private readonly string applicationName;

        public RunAtStartupAdapter(string applicationName)
        {
            this.applicationName = applicationName;
        }

        public void AddApplicationToCurrentUserStartup()
        {
            using RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, true);
            key.SetValue(applicationName, "\"" + Process.GetCurrentProcess().MainModule.FileName + "\"");
        }

        public void RemoveApplicationFromCurrentUserStartup()
        {
            using RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, true);
            key.DeleteValue(applicationName, false);
        }

        public bool IsCurrentStartupEnabled()
        {
            using RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, false);
            return key.GetValue(applicationName) != null;
        }
    }
}
