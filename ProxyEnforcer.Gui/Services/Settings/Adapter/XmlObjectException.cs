﻿using System;

namespace ProxyEnforcer.Gui.Services.Settings.Adapter
{
    [Serializable]
    public class XmlObjectException : Exception
    {
        public XmlObjectException()
        {
        }

        public XmlObjectException(string msg)
            : base(msg)
        {
        }

        public XmlObjectException(string msg, Exception inner)
            : base(msg, inner)
        {
        }

        protected XmlObjectException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext) :
            base(serializationInfo, streamingContext)
        {
        }
    }
}
