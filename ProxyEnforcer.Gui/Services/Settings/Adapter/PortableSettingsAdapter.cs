﻿using NLog;
using ProxyEnforcer.Gui.Services.Settings.Model;
using System;

namespace ProxyEnforcer.Gui.Services.Settings.Adapter
{
    internal class PortableSettingsAdapter : XmlAdapter<PortableSettings>
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();

        internal override PortableSettings LoadObject(string fileName)
        {
            try
            {
                return base.LoadObject(fileName);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw new XmlObjectException("Failed to load portable settings", e);
            }
        }

        internal override void SaveObject(PortableSettings obj, string fileName)
        {
            try
            {
                base.SaveObject(obj, fileName);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw new XmlObjectException("Failed to save portable settings", e);
            }
        }
    }
}
