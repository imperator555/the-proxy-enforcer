﻿using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class PortableSettings
    {
        internal PortableSettings()
        {
            ProxyList = new List<ProxyConfig>();
        }

        public List<ProxyConfig> ProxyList { get; }

        public bool StartMinimized { get; set; }

        public bool ShowNotificationOnProxyChange { get; set; }
    }
}
