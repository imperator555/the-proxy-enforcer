﻿using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class ProxyPlugin
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public List<PluginProperty> Properties { get; set; }
    }
}
