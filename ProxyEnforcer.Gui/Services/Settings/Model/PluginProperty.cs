﻿namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class PluginProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; } // TODO no need to save type because it can be calculated based on the plugin definitions...
    }
}