﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    internal class AppSettings
    {
        internal AppSettings(PortableSettings xml, LocalSettings xml2)
        {
            ProxyList = new ObservableCollection<ProxyConfig>(xml.ProxyList);
            StartMinimized = xml.StartMinimized;
            ShowNotificationOnProxyChange = xml.ShowNotificationOnProxyChange;

            LastActiveProxyName = xml2.LastActiveProxyName;
            LastSystemProxyConfig = xml2.LastSystemProxyConfig;
            DefaultCloseAction = xml2.DefaultCloseAction;
            EnforcingEnabled = xml2.EnforcingEnabled;
            SystemProxyAutoRefreshEnabled = xml2.SystemProxyAutoRefreshEnabled;
            HidingLocalRoutesOnRouteTableEnabled = xml2.HidingLocalRoutesOnRouteTableEnabled;
            SettingsEditorApp = xml2.SettingsEditorApp;
        }

        internal AppSettings()
        {
            RunAtStartup = false;
            StartMinimized = true;
            ShowNotificationOnProxyChange = true;
            EnforcingEnabled = false;
            SystemProxyAutoRefreshEnabled = true;
            HidingLocalRoutesOnRouteTableEnabled = false;
            SettingsEditorApp = "notepad";

            ProxyList = new List<ProxyConfig>
            {
                ProxyConfig.NoProxy
            };
        }

        internal IEnumerable<ProxyConfig> ProxyList { get; set; }

        internal bool StartMinimized { get; set; }

        internal bool ShowNotificationOnProxyChange { get; set; }

        internal bool EnforcingEnabled { get; set; }

        internal string LastActiveProxyName { get; set; }

        internal ProxyConfig LastSystemProxyConfig { get; set; }

        internal CloseAction DefaultCloseAction { get; set; }

        internal bool RunAtStartup { get; set; }

        internal bool SystemProxyAutoRefreshEnabled { get; set; }

        internal bool HidingLocalRoutesOnRouteTableEnabled { get; set; }

        internal string SettingsEditorApp { get; set; }

        internal ProxyConfig GetLastActiveProxy()
        {
            foreach (var pc in ProxyList)
            {
                if (pc.Name == LastActiveProxyName)
                {
                    return pc;
                }
            }

            return null;
        }
    }
}
