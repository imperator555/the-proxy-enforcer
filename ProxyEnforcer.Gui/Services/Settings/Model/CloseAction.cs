﻿namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public enum CloseAction
    {
        CloseWindow, CloseApp, Prompt
    }
}
