﻿namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class ConfigFileSettings
    {
        public LocalSettings LocalSettings { get; set; }

        public PortableSettings PortableSettings { get; set; }
    }
}
