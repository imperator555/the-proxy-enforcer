﻿using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class ProxyConfig
    {
        public static ProxyConfig NoProxy { get; private set; }

        static ProxyConfig()
        {
            NoProxy = new ProxyConfig()
            {
                Name = "no proxy",
                Enabled = false,
                AutomaticDetect = false,
                Address = string.Empty,
                UseAutomaticConfigurationScript = false,
                AutomaticConfigurationScript = string.Empty,
                Port = null
            };
        }

        public string Name { get; set; }
        public bool Enabled { get; set; }
        public bool AutomaticDetect { get; set; }
        public string Address { get; set; }
        public int? Port { get; set; }
        public bool UseAutomaticConfigurationScript { get; set; }
        public string AutomaticConfigurationScript { get; set; }
        public List<string> Exceptions { get; } = new List<string>();
        public List<ProxyPlugin> Plugins { get; } = new List<ProxyPlugin>();
    }
}
