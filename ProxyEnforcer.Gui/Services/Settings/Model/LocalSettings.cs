﻿namespace ProxyEnforcer.Gui.Services.Settings.Model
{
    public class LocalSettings
    {
        public ProxyConfig LastSystemProxyConfig { get; set; }

        public string LastActiveProxyName { get; set; }

        public CloseAction DefaultCloseAction { get; set; }

        public bool EnforcingEnabled { get; set; }

        public bool SystemProxyAutoRefreshEnabled { get; set; }

        public bool HidingLocalRoutesOnRouteTableEnabled { get; set; }

        public string SettingsEditorApp { get; set; }
    }
}
