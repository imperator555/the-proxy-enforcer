﻿using ProxyEnforcer.Gui.Services.ProxyPlugins;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal static class ProxyPluginMappers
    {
        public static UiModel.ProxyPlugin ToUiProxyPlugin(IProxyConfigPlugin proxyPlugin)
        {
            return new UiModel.ProxyPlugin()
            {
                IsEnabled = false,
                Name = proxyPlugin.GetPluginName(),
                Properties = ProxyPluginPropertyMappers.ToUiProxyProperties(proxyPlugin.GetPluginProperties())
            };
        }

        internal static UiModel.ProxyPlugin ToUiProxyPlugin(Settings.Model.ProxyPlugin proxyPlugin)
        {
            return new UiModel.ProxyPlugin()
            {
                IsEnabled = proxyPlugin.IsEnabled,
                Name = proxyPlugin.Name,
                Properties = ProxyPluginPropertyMappers.ToUiProxyProperties(proxyPlugin.Properties)
            };
        }

        internal static Settings.Model.ProxyPlugin ToSettingsProxyPlugin(UiModel.ProxyPlugin proxyPlugin)
        {
            return new Settings.Model.ProxyPlugin()
            {
                IsEnabled = proxyPlugin.IsEnabled,
                Name = proxyPlugin.Name,
                Properties = ProxyPluginPropertyMappers.ToSettingsPluginProperties(proxyPlugin.Properties).ToList()
            };
        }

        public static IEnumerable<UiModel.ProxyPlugin> ToUiProxyPluginList(IEnumerable<IProxyConfigPlugin> proxyPluginList) =>
            proxyPluginList.Select(ToUiProxyPlugin);

        public static IEnumerable<UiModel.ProxyPlugin> ToUiProxyPluginList(IEnumerable<Settings.Model.ProxyPlugin> proxyPluginList) =>
            proxyPluginList.Select(ToUiProxyPlugin);

        internal static IEnumerable<Settings.Model.ProxyPlugin> ToSettingsProxyPluginList(IEnumerable<UiModel.ProxyPlugin> proxyPluginList) =>
            proxyPluginList.Select(ToSettingsProxyPlugin);
    }
}
