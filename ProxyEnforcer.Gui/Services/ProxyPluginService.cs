﻿using ProxyEnforcer.Gui.Services.ProxyPlugins;
using ProxyEnforcer.Gui.Services.ProxyPlugins.CommandLine;
using ProxyEnforcer.Gui.Services.ProxyPlugins.Maven;
using ProxyEnforcer.Gui.Services.ProxyPlugins.System;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal interface IProxyPluginService
    {
        IEnumerable<IProxyConfigPlugin> Plugins { get; }

        IProxyConfigPlugin GetPlugin(string name);
    }

    internal class ProxyPluginService : IProxyPluginService
    {
        private readonly List<IProxyConfigPlugin> plugins = new List<IProxyConfigPlugin>();

        public ProxyPluginService()
        {
            InitPlugins();
        }

        public IEnumerable<IProxyConfigPlugin> Plugins => plugins;

        public IProxyConfigPlugin GetPlugin(string name) =>
            Plugins.Where(x => x.GetPluginName() == name).FirstOrDefault();

        private void InitPlugins()
        {
            plugins.Add(new SystemProxyPlugin());
            plugins.Add(new GitGlobalProxyPlugin());
            plugins.Add(new NpmProxyPlugin());
            plugins.Add(new NugetProxyPlugin());
            plugins.Add(new MavenProxyPlugin());
            plugins.Add(new DebugProxyPlugin());
        }
    }
}
