﻿using System.Xml;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.Maven
{
    internal static class XmlDocumentExtensions
    {
        /// <summary>
        /// Creates a new XmlElement and appends it to the end of the document's children.
        /// </summary>
        /// <param name="document">The XmlDocument to use</param>
        /// <param name="elementName">Name of the created XmlElement</param>
        /// <returns>Returns the newly created XmlElement</returns>
        public static XmlElement CreateAndAppendElement(this XmlDocument document, string elementName)
        {
            var element = document.CreateElement(elementName);
            document.AppendChild(element);

            return element;
        }
    }
}
