﻿using ProxyEnforcer.Gui.UiModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.Maven
{
    internal class MavenProxyPlugin : IProxyConfigPlugin
    {
        private ObservableProxyConfig proxyConfig;

        public void Enable() =>
            SetProxy((null, null, proxyConfig.Address, proxyConfig.Port, proxyConfig.Enabled));

        public ObservableProxyConfig GetActiveProxyConfig()
        {
            var (_, _, Address, Port, Active) = GetProxy();

            return new ObservableProxyConfig()
            {
                Address = Address,
                Port = Port,
                Name = $"maven proxy",
                UseAutomaticConfigurationScript = false,
                IsSetForSystemProxy = false,
                IsSystemProxyConfig = false,
                AutomaticConfigurationScript = "",
                Enabled = Active
            };
        }

        public string GetPluginName() => "maven";

        public IEnumerable<ProxyPluginProperty<object>> GetPluginProperties() => Array.Empty<ProxyPluginProperty<object>>();

        public void SetPluginProperty(string name, object value)
        {
            // ignore
        }

        public void SetProxyConfig(ObservableProxyConfig config) => proxyConfig = config;

        private static (string User, string Password, string Address, int? Port, bool Active) GetProxy()
        {
            if (File.Exists(GetMavenConfigFilePath()))
            {
                XmlDocument settingsDoc = new XmlDocument();
                settingsDoc.Load(GetMavenConfigFilePath());

                return ExtractProxyFromSettingsDocument(settingsDoc);
            }

            return EmptyProxy();
        }

        private static (string User, string Password, string Address, int? Port, bool Active) ExtractProxyFromSettingsDocument(XmlDocument settingsDoc)
        {
            var activeProxy = settingsDoc.SelectSingleNode("/settings/proxies/proxy[active[text() = 'true']]");

            if (activeProxy != null)
            {
                var user = activeProxy.SelectSingleNode("username")?.InnerText;
                var password = activeProxy.SelectSingleNode("password")?.InnerText;
                var address = activeProxy.SelectSingleNode("host")?.InnerText;
                var portText = activeProxy.SelectSingleNode("port")?.InnerText;
                int.TryParse(portText, out int port);

                return (user, password, address, port, true);
            }

            return EmptyProxy();
        }

        private static void SetProxy((string User, string Password, string Address, int? Port, bool Active) proxyConfig)
        {
            if (Directory.Exists(GetMavenConfigFolderPath()))
            {
                XmlDocument settingsDoc = new XmlDocument();

                if (File.Exists(GetMavenConfigFilePath()))
                {
                    settingsDoc.Load(GetMavenConfigFilePath());
                }
                else
                {
                    CreateEmptySettingsDocument(settingsDoc);
                }

                AddProxyToSettingsDocument(settingsDoc, proxyConfig);
                PurgeEmptyProxyNodes(settingsDoc);

                using XmlTextWriter writer = new XmlTextWriter(GetMavenConfigFilePath(), null)
                {
                    Formatting = Formatting.Indented
                };

                settingsDoc.WriteTo(writer);
            }
        }

        private static void CreateEmptySettingsDocument(XmlDocument settingsDoc) =>
            settingsDoc.CreateAndAppendElement("settings");

        private static void AddProxyToSettingsDocument(XmlDocument settingsDoc, (string User, string Password, string Address, int? Port, bool Active) proxyConfig)
        {
            EnsureProxiesNodeExists(settingsDoc);
            DisableExistingProxies(settingsDoc);

            var proxiesNode = settingsDoc.SelectSingleNode("/settings/proxies") as XmlElement;
            var proxyEnforcerHttpProxyNode = EnsureProxyNodeExists(proxiesNode, ProxyProtocol.http);
            var proxyEnforcerHttpsProxyNode = EnsureProxyNodeExists(proxiesNode, ProxyProtocol.https);

            SetProxyNodeValues(proxyEnforcerHttpProxyNode, ProxyProtocol.http, proxyConfig);
            SetProxyNodeValues(proxyEnforcerHttpsProxyNode, ProxyProtocol.https, proxyConfig);
        }

        private static XmlElement EnsureProxyNodeExists(XmlElement proxiesNode, ProxyProtocol protocol)
        {
            if (proxiesNode.SelectSingleNode($"proxy[id[text() = 'proxy-enforcer-{protocol}']]") is not XmlElement possibleProxyNode)
            {
                possibleProxyNode = proxiesNode.CreateAndAppendElement("proxy");
            }

            return possibleProxyNode;
        }

        private static void SetProxyNodeValues(XmlElement proxyNode, ProxyProtocol proto, (string User, string Password, string Address, int? Port, bool Active) proxyConfig)
        {
            proxyNode.CreateAndAppendElementIfNotExists("id").InnerText = $"proxy-enforcer-{proto}";
            proxyNode.CreateAndAppendElementIfNotExists("active").InnerText = proxyConfig.Active.ToString().ToLower();
            proxyNode.CreateAndAppendElementIfNotExists("protocol").InnerText = proto.ToString();
            proxyNode.CreateAndAppendElementIfNotExists("host").InnerText = proxyConfig.Address ?? "";
            proxyNode.CreateAndAppendElementIfNotExists("port").InnerText = proxyConfig.Port.ToString() ?? "";
            proxyNode.CreateAndAppendElementIfNotExists("username").InnerText = proxyConfig.User ?? "";
            proxyNode.CreateAndAppendElementIfNotExists("password").InnerText = proxyConfig.Password ?? "";
        }

        private static void EnsureProxiesNodeExists(XmlDocument settingsDoc)
        {
            var settingsNode = settingsDoc.SelectSingleNode("/settings") as XmlElement;
            settingsNode.CreateAndAppendElementIfNotExists("proxies");
        }

        private static void DisableExistingProxies(XmlDocument settingsDoc)
        {
            foreach (XmlElement proxyNode in GetProxyNodes(settingsDoc))
            {
                var proxyId = proxyNode.SelectSingleNode("id")?.InnerText;

                if ("proxy-enforcer-http" != proxyId && "proxy-enforcer-https" != proxyId)
                {
                    proxyNode.CreateAndAppendElementIfNotExists("active").InnerText = "false";
                }
            }
        }

        private static IEnumerable<XmlElement> GetProxyNodes(XmlDocument settingsDoc)
        {
            foreach (XmlNode someNode in settingsDoc.SelectNodes("/settings/proxies/proxy"))
            {
                if (someNode.NodeType == XmlNodeType.Element)
                {
                    yield return someNode as XmlElement;
                }
            }
        }

        private static void PurgeEmptyProxyNodes(XmlDocument settingsDoc)
        {
            foreach (XmlElement proxyNode in GetProxyNodes(settingsDoc))
            {
                var proxyId = proxyNode.SelectSingleNode("id")?.InnerText;

                if ("proxy-enforcer-http" == proxyId || "proxy-enforcer-https" == proxyId)
                {
                    var emptyNodes = proxyNode.SelectNodes("./*[text() = '']");

                    foreach (XmlNode empty in emptyNodes)
                    {
                        proxyNode.RemoveChild(empty);
                    }
                }
            }

            var proxies = settingsDoc.SelectSingleNode("/settings/proxies");
            var invalidProxies = proxies.SelectNodes("proxy[not(host)]");

            foreach (XmlNode node in invalidProxies)
            {
                proxies.RemoveChild(node);
            }
        }

        private static (string User, string Password, string Address, int? Port, bool Active) EmptyProxy() => (null, null, null, null, false);

        private static string GetMavenConfigFolderPath() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".m2");

        private static string GetMavenConfigFilePath() => Path.Combine(GetMavenConfigFolderPath(), "settings.xml");
    }
}
