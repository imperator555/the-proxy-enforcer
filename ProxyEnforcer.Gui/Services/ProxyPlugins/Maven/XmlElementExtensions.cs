﻿using System.Xml;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.Maven
{
    internal static class XmlElementExtensions
    {
        /// <summary>
        /// Creates a new XmlElement and appends it to the end of this element's children
        /// </summary>
        /// <param name="element">This element</param>
        /// <param name="elementName">The name of the newly created XmlElement</param>
        /// <returns>Return the created XmlElement</returns>
        public static XmlElement CreateAndAppendElement(this XmlElement element, string elementName)
        {
            var createdElement = element.OwnerDocument.CreateElement(elementName);
            element.AppendChild(createdElement);

            return createdElement;
        }

        /// <summary>
        /// Checks wheter an XmlElement exists among this element's children. Creates it if not.
        /// </summary>
        /// <param name="element">This element</param>
        /// <param name="elementName">The name of the element to find</param>
        /// <returns>Returns the newly created XmlElement or the existing child element in case it was already existing</returns>
        public static XmlElement CreateAndAppendElementIfNotExists(this XmlElement element, string elementName)
        {
            if (element.SelectSingleNode(elementName) is not XmlElement existingElement)
            {
                existingElement = element.CreateAndAppendElement(elementName);
            }

            return existingElement;
        }
    }
}
