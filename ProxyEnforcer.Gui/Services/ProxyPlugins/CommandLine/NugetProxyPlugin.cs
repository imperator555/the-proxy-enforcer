﻿using ProxyEnforcer.Gui.UiModel;
using System.Globalization;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.CommandLine
{
    internal class NugetProxyPlugin : AbstractCommandLineProxyPlugin
    {
        private const string setProxyCommand = "nuget config -set {0}_proxy={1}:{2}";
        private const string unsetProxyCommand = "nuget config -set {0}_proxy=\" \"";
        private const string getProxyCommand = "nuget config http_proxy";

        private ObservableProxyConfig proxyConfig;

        public override ObservableProxyConfig GetActiveProxyConfig()
        {
            var (_, _, Address, Port) = GetProxy();

            return new ObservableProxyConfig()
            {
                Address = Address.Replace("\r\n", "").Trim(),
                Port = Port,
                Name = $"nuget proxy",
                UseAutomaticConfigurationScript = false,
                IsSetForSystemProxy = false,
                IsSystemProxyConfig = false,
                AutomaticConfigurationScript = ""
            };
        }

        public override string GetPluginName() => "nuget";

        public override void SetProxyConfig(ObservableProxyConfig config) => proxyConfig = config;

        public override void Enable() =>
            SetProxy((null, null, proxyConfig.Address, proxyConfig.Port));

        private void SetProxy((string User, string Password, string Address, int? Port) settings)
        {
            if (settings.Address == null || !settings.Port.HasValue)
            {
                UnsetProxy();
            }
            else
            {
                RunCommand(string.Format(CultureInfo.InvariantCulture, setProxyCommand, ProxyProtocol.http, settings.Address, settings.Port));
                RunCommand(string.Format(CultureInfo.InvariantCulture, setProxyCommand, ProxyProtocol.https, settings.Address, settings.Port));
            }
        }

        private void UnsetProxy()
        {
            RunCommand(string.Format(unsetProxyCommand, ProxyProtocol.http));
            RunCommand(string.Format(unsetProxyCommand, ProxyProtocol.https));
        }

        private (string User, string Password, string Address, int? Port) GetProxy()
        {
            var output = RunCommand(getProxyCommand);
            return ParseProxyString(output);
        }
    }
}
