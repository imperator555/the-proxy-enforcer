﻿using ProxyEnforcer.Gui.UiModel;
using System.Globalization;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.CommandLine
{
    internal class GitGlobalProxyPlugin : AbstractCommandLineProxyPlugin
    {
        private const string setProxyCommand = "git config --global {0}.proxy {1}:{2}";
        private const string unsetProxyCommand = "git config --global --unset {0}.proxy";
        private const string getProxyCommand = "git config --global --get {0}.proxy";

        private ObservableProxyConfig proxyConfig;

        public override ObservableProxyConfig GetActiveProxyConfig()
        {
            var (_, _, Address, Port) = GetProxy();

            return new ObservableProxyConfig()
            {
                Address = Address,
                Port = Port,
                Name = $"git global proxy",
                UseAutomaticConfigurationScript = false,
                IsSetForSystemProxy = false,
                IsSystemProxyConfig = false,
                AutomaticConfigurationScript = ""
            };
        }

        public override string GetPluginName() => $"git global";

        public override void SetProxyConfig(ObservableProxyConfig config) =>
            proxyConfig = config;

        public override void Enable() =>
            SetProxy((null, null, proxyConfig.Address, proxyConfig.Port));

        private void SetProxy((string User, string Password, string Address, int? Port) settings)
        {
            if (settings.Address == null || !settings.Port.HasValue)
            {
                UnsetProxy();
            }
            else
            {
                RunCommand(string.Format(CultureInfo.InvariantCulture, setProxyCommand, ProxyProtocol.http, settings.Address, settings.Port));
                RunCommand(string.Format(CultureInfo.InvariantCulture, setProxyCommand, ProxyProtocol.https, settings.Address, settings.Port));
            }
        }

        private void UnsetProxy()
        {
            RunCommand(string.Format(CultureInfo.InvariantCulture, unsetProxyCommand, ProxyProtocol.http));
            RunCommand(string.Format(CultureInfo.InvariantCulture, unsetProxyCommand, ProxyProtocol.https));
        }

        private (string User, string Password, string Address, int? Port) GetProxy()
        {
            var output = RunCommand(string.Format(CultureInfo.InvariantCulture, getProxyCommand, ProxyProtocol.http));
            return ParseProxyString(output);
        }
    }
}
