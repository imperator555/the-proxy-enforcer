﻿using ProxyEnforcer.Gui.UiModel;
using System.Globalization;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.CommandLine
{
    internal class NpmProxyPlugin : AbstractCommandLineProxyPlugin
    {
        private const string setHttpProxyCommand = "npm config set proxy {0}:{1}";
        private const string setHttpsProxyCommand = "npm config set https-proxy {0}:{1}";
        private const string unsetHttpProxyCommand = "npm config delete proxy";
        private const string unsetHttpsProxyCommand = "npm config delete https-proxy";
        private const string getProxyCommand = "npm config get proxy";

        private ObservableProxyConfig proxyConfig;

        public override ObservableProxyConfig GetActiveProxyConfig()
        {
            var (_, _, Address, Port) = GetProxy();

            return new ObservableProxyConfig()
            {
                Address = Address,
                Port = Port,
                Name = $"npm proxy",
                UseAutomaticConfigurationScript = false,
                IsSetForSystemProxy = false,
                IsSystemProxyConfig = false,
                AutomaticConfigurationScript = ""
            };
        }

        public override string GetPluginName() => "npm";

        public override void SetProxyConfig(ObservableProxyConfig config) => proxyConfig = config;

        public override void Enable()
        {
            SetProxy((null, null, proxyConfig.Address, proxyConfig.Port));
        }

        private void SetProxy((string User, string Password, string Address, int? Port) settings)
        {
            if (settings.Address == null || !settings.Port.HasValue)
            {
                UnsetProxy();
            }
            else
            {
                RunCommand(string.Format(CultureInfo.InvariantCulture, setHttpProxyCommand, settings.Address, settings.Port));
                RunCommand(string.Format(CultureInfo.InvariantCulture, setHttpsProxyCommand, settings.Address, settings.Port));
            }
        }

        private void UnsetProxy()
        {
            RunCommand(unsetHttpProxyCommand);
            RunCommand(unsetHttpsProxyCommand);
        }

        private (string User, string Password, string Address, int? Port) GetProxy()
        {
            var output = RunCommand(getProxyCommand);
            var parsed = ParseProxyString(output);

            if (parsed.Address != null && parsed.Address.Trim() == "null")
            {
                parsed.Address = null;
            }

            return parsed;
        }
    }
}
