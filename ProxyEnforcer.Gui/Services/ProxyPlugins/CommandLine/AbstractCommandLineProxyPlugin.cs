﻿using NLog;
using ProxyEnforcer.Gui.UiModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.CommandLine
{
    internal abstract class AbstractCommandLineProxyPlugin : IProxyConfigPlugin
    {
        protected static readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected AbstractCommandLineProxyPlugin()
        {
            ExecuteDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        protected string ExecuteDirectoryPath { get; }

        public IEnumerable<ProxyPluginProperty<object>> GetPluginProperties() => Array.Empty<ProxyPluginProperty<object>>();
        public void SetPluginProperty(string name, object value) { }

        public abstract ObservableProxyConfig GetActiveProxyConfig();
        public abstract string GetPluginName();
        public abstract void SetProxyConfig(ObservableProxyConfig config);
        public virtual void Enable() { }

        protected string RunCommand(string command)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                WorkingDirectory = ExecuteDirectoryPath,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = "cmd.exe",
                Arguments = $"/C {command}",
                RedirectStandardOutput = true
            };

            using Process process = new Process { StartInfo = startInfo };
            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            return output;
        }

        protected (string User, string Password, string Address, int? Port) ParseProxyString(string proxyString)
        {
            (string User, string Password, string Address, int? Port) proxy = default;

            if (proxyString.Length > 0)
            {
                var addressAndPort = proxyString;

                if (proxyString.Contains("@"))
                {
                    var split = proxyString.Split(new char[] { '@' });
                    var userNameAndPassword = split[0];
                    addressAndPort = split[1];

                    (proxy.User, proxy.Password) = ExtractUsernameAndPassword(userNameAndPassword);
                }

                (proxy.Address, proxy.Port) = ExtractAddressAndPort(addressAndPort);
            }

            return proxy;
        }

        private static (string User, string Password) ExtractUsernameAndPassword(string rawString)
        {
            const string http = @"http://";
            var userAndPass = rawString;

            if (rawString.StartsWith(http, StringComparison.InvariantCultureIgnoreCase))
            {
                userAndPass = rawString.Remove(0, http.Length);
            }

            var parts = userAndPass.Split(':');

            return (parts[0], parts[1]);
        }

        private static (string Address, int? Port) ExtractAddressAndPort(string rawString)
        {
            (string Address, int? Port) result = default;

            if (rawString.Contains(":"))
            {
                try
                {
                    var parts = rawString.Split(new char[] { ':' });
                    result.Address = parts[0];
                    result.Port = int.Parse(parts[1], CultureInfo.InvariantCulture);
                }
                catch (Exception ex) when (ex is FormatException || ex is OverflowException)
                {
                    logger.Warn($"Failed to parse proxy string: {rawString}");
                }
            }
            else
            {
                result.Address = rawString;
                result.Port = null;
            }

            return result;
        }
    }
}
