﻿using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins
{
    internal interface IProxyConfigPlugin
    {
        /// <summary>
        /// Enables the plugin
        /// Use <see cref="SetProxyConfig"/> to set the active proxy.
        /// </summary>
        void Enable();

        /// <summary>
        /// Returns the name of the plugin which is used to identify it accross the application, so it should stay constant.
        /// </summary>
        /// <returns></returns>
        string GetPluginName();

        /// <summary>
        /// Sets the given config to the underlying object.
        /// </summary>
        /// <param name="config"></param>
        void SetProxyConfig(ObservableProxyConfig config);

        /// <summary>
        /// Gets the current proxy configuration from the underlying object
        /// </summary>
        /// <returns></returns>
        ObservableProxyConfig GetActiveProxyConfig();

        IEnumerable<ProxyPluginProperty<object>> GetPluginProperties();

        void SetPluginProperty(string name, object value);
    }
}
