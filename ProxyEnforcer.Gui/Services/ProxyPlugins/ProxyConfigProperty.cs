﻿namespace ProxyEnforcer.Gui.Services.ProxyPlugins
{
    public abstract class ProxyPluginProperty<T>
    {
        protected ProxyPluginProperty(string name, T value, PropertyType type)
        {
            Name = name;
            Value = value;
            Type = type;
        }

        public string Name { get; }
        public T Value { get; }
        public PropertyType Type { get; }
    }
}