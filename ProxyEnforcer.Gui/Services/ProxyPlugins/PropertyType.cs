﻿namespace ProxyEnforcer.Gui.Services.ProxyPlugins
{
    public enum PropertyType
    {
        String, Boolean, Number
    }
}
