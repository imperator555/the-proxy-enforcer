﻿using ProxyEnforcer.Gui.UiModel;
using System;
using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins
{
    internal class DebugProxyPlugin : IProxyConfigPlugin
    {
        public void Enable()
        {
            throw new NotImplementedException();
        }

        public ObservableProxyConfig GetActiveProxyConfig()
        {
            throw new NotImplementedException();
        }

        public string GetPluginName() => "debug";

        public IEnumerable<ProxyPluginProperty<object>> GetPluginProperties() => Array.Empty<ProxyPluginProperty<object>>();

        public void SetPluginProperty(string name, object value)
        {
            throw new NotImplementedException();
        }

        public void SetProxyConfig(ObservableProxyConfig config)
        {
            throw new NotImplementedException();
        }
    }
}
