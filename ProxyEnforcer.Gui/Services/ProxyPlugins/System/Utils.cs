﻿using ProxyEnforcer.Network.Proxy;
using System;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.System
{
    internal static class Utils
    {
        /// <summary>
        /// Returns false only if changing the configurations may change actual system proxy.
        /// For example: config A has proxy address "a" but the proxy is disabled while config
        ///             B has proxy address "b" but it is disabled as well. Although the addresses
        ///             are different the method will return true (as configs are the 'same'),
        ///             because both proxies are disabled and won't change network behaviour.
        /// NOTE: This method may not be 100% accurate...
        /// </summary>
        /// <returns>true when configs are equal</returns>
        internal static bool LooseCompare(ProxyConfig proxyX, ProxyConfig proxyY)
        {
            if (proxyX == null)
            {
                throw new ArgumentNullException(nameof(proxyX));
            }

            /*
            // when the following conditions hold, a change in the proxy configurations may
            // change the actual proxy
            */

            if (proxyX.AutomaticDetect != proxyY.AutomaticDetect)
            {
                return false;
            }

            if (proxyX.Enabled != proxyY.Enabled)
            {
                return false;
            }

            if (proxyX.UseAutomaticConfigurationScript != proxyY.UseAutomaticConfigurationScript)
            {
                return false;
            }

            // no proxy will be used with both configurations
            if (proxyX.AutomaticDetect == false && proxyX.Enabled == false && proxyX.UseAutomaticConfigurationScript == false &&
                 proxyY.AutomaticDetect == false && proxyY.Enabled == false && proxyY.UseAutomaticConfigurationScript == false)
            {
                return true;
            }

            // when both configs have the proxy enabled and the address or the port is different
            if (proxyX.Enabled && proxyY.Enabled)
            {
                if (proxyX.Address != proxyY.Address || proxyX.Port != proxyY.Port)
                {
                    return false;
                }
            }

            // when both configs use atuo config scripts and the scripts are different
            if (proxyX.UseAutomaticConfigurationScript && proxyY.UseAutomaticConfigurationScript)
            {
                if (proxyX.AutomaticConfigurationScript != proxyY.AutomaticConfigurationScript)
                {
                    return false;
                }
            }

            // in every other case we consider the configs equal
            return true;
        }
    }
}
