﻿using NLog;
using ProxyEnforcer.Gui.UiModel;
using ProxyEnforcer.Network;
using ProxyEnforcer.Network.Proxy;
using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.System
{
    internal class SystemProxyPlugin : IProxyConfigPlugin
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private ObservableProxyConfig proxyConfig;
        private int proxyCheckIntervalMS = 1000;
        private bool enforcingEnabled = false;
        private Enforcer enforcer;

        public delegate void SystemProxyChangedEventHandler(ObservableProxyConfig previous, ObservableProxyConfig current);
        public event SystemProxyChangedEventHandler OnSystemProxyChanged;
        public delegate void EnforcingStateChangedEventHandler(ObservableProxyConfig sender, bool isEnabled);
        public event EnforcingStateChangedEventHandler OnEnforcingStateChanged;

        public SystemProxyPlugin()
        {
            ProxyService.ProxyConfigChanged += HandleProxyConfigChanged;
        }

        public ObservableProxyConfig GetActiveProxyConfig() =>
            ProxyMappers.ToObservableProxyConfig(ProxyService.GetSystemProxyConfig());

        public IEnumerable<ProxyPluginProperty<object>> GetPluginProperties()
        {
            return new ProxyPluginProperty<object>[]
            {
                new EnforcingEnabled(enforcingEnabled),
                new ProxyCheckInterval(proxyCheckIntervalMS)
            };
        }

        public string GetPluginName() => "System";

        public void SetPluginProperty(string name, object value)
        {
            switch (name)
            {
                case nameof(ProxyCheckInterval):
                    SetProxyCheckInterval(value);
                    break;

                case nameof(EnforcingEnabled):
                    SetEnforcingEnabled(value);
                    break;
            }
        }

        public void SetProxyConfig(ObservableProxyConfig config) =>
            proxyConfig = config;

        public void Enable()
        {
            // in case a previous enforcing is running
            Disable();

            SetSystemProxy();

            if (enforcingEnabled)
            {
                enforcer = new Enforcer(ProxyMappers.ToNetworkProxyConfig(proxyConfig), proxyCheckIntervalMS);
                enforcer.Enforce();
                InvokeEnforcingStateChanged(true);
            }
        }

        public void Disable()
        {
            if (enforcer != null)
            {
                enforcer.StopEnforcing();
                enforcer = null;
                InvokeEnforcingStateChanged(false);
            }
        }

        private void HandleProxyConfigChanged(ProxyConfig previous, ProxyConfig current) =>
            OnSystemProxyChanged?.Invoke(ProxyMappers.ToObservableProxyConfig(previous), ProxyMappers.ToObservableProxyConfig(current));

        private void SetProxyCheckInterval(object value)
        {
            if (value is string stringValue)
            {
                proxyCheckIntervalMS = int.Parse(stringValue);
            }
            else
            {
                proxyCheckIntervalMS = (int)value;
            }
        }

        private void SetEnforcingEnabled(object value)
        {
            if (value is string stringValue)
            {
                enforcingEnabled = bool.Parse(stringValue);
            }
            else
            {
                enforcingEnabled = (bool)value;
            }
        }

        private void SetSystemProxy()
        {
            logger.Info($"Setting system proxy to: {proxyConfig}");
            ProxyService.SetSystemProxyConfig(ProxyMappers.ToNetworkProxyConfig(proxyConfig));
        }

        private void InvokeEnforcingStateChanged(bool state) =>
            OnEnforcingStateChanged?.Invoke(proxyConfig, state);
    }

    public class ProxyCheckInterval : ProxyPluginProperty<object>
    {
        public ProxyCheckInterval(int value) : base(nameof(ProxyCheckInterval), value, PropertyType.Number)
        {

        }
    }

    public class EnforcingEnabled : ProxyPluginProperty<object>
    {
        public EnforcingEnabled(bool value) : base(nameof(EnforcingEnabled), value, PropertyType.Boolean)
        {

        }
    }
}
