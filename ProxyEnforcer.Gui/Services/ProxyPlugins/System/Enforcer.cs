﻿using ProxyEnforcer.Network;
using ProxyEnforcer.Network.Proxy;
using System.Threading.Tasks;

namespace ProxyEnforcer.Gui.Services.ProxyPlugins.System
{
    internal class Enforcer
    {
        private bool running = false;

        public Enforcer(ProxyConfig enforcedProxy, int checkInterval)
        {
            EnforcedProxy = enforcedProxy;
            CheckInterval = checkInterval;
        }

        public ProxyConfig EnforcedProxy { get; }

        public int CheckInterval { get; }

        public void Enforce()
        {
            if (!running)
            {
                running = true;

                RunEnforcingLoop();
            }
        }

        public void StopEnforcing() =>
            running = false;

        private async void RunEnforcingLoop()
        {
            while (running)
            {
                if (CurrentSystemProxyChanged())
                {
                    SetSystemProxy();
                }

                await Task.Delay(CheckInterval);
            }
        }

        private bool CurrentSystemProxyChanged()
        {
            var systemProxy = ProxyService.GetSystemProxyConfig();

            return !Utils.LooseCompare(EnforcedProxy, systemProxy);
        }

        private void SetSystemProxy() =>
            ProxyService.SetSystemProxyConfig(EnforcedProxy);
    }
}
