﻿namespace ProxyEnforcer.Gui.Services.ProxyPlugins
{
    internal enum ProxyProtocol
    {
        http,
        https
    }
}
