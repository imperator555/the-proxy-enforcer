﻿using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal static class ProxyMappers
    {
        internal static ObservableProxyConfig ToObservableProxyConfig(Network.Proxy.ProxyConfig proxyConfig)
        {
            var proxy = new ObservableProxyConfig()
            {
                Address = proxyConfig.Address,
                AutomaticConfigurationScript = proxyConfig.AutomaticConfigurationScript,
                AutomaticDetect = proxyConfig.AutomaticDetect,
                Enabled = proxyConfig.Enabled,
                IsSetForSystemProxy = true,
                IsSystemProxyConfig = true,
                Name = "system proxy",
                Port = proxyConfig.Port,
                UseAutomaticConfigurationScript = proxyConfig.UseAutomaticConfigurationScript
            };

            foreach (var item in proxyConfig.Exceptions)
            {
                proxy.AddException(item);
            }

            return proxy;
        }

        internal static ObservableProxyConfig ToObservableProxyConfig(Settings.Model.ProxyConfig proxyConfig)
        {
            var proxy = new ObservableProxyConfig()
            {
                Name = proxyConfig.Name,
                Enabled = proxyConfig.Enabled,
                AutomaticDetect = proxyConfig.AutomaticDetect,
                Address = proxyConfig.Address,
                Port = proxyConfig.Port,
                UseAutomaticConfigurationScript = proxyConfig.UseAutomaticConfigurationScript,
                AutomaticConfigurationScript = proxyConfig.AutomaticConfigurationScript
            };

            foreach (var item in proxyConfig.Exceptions)
            {
                proxy.AddException(item);
            }

            foreach (var plugin in ProxyPluginMappers.ToUiProxyPluginList(proxyConfig.Plugins))
            {
                proxy.Plugins.Add(plugin);
            }

            return proxy;
        }

        internal static Settings.Model.ProxyConfig ToSettingsProxyConfig(ObservableProxyConfig proxyConfig)
        {
            var config = new Settings.Model.ProxyConfig()
            {
                Address = proxyConfig.Address,
                AutomaticConfigurationScript = proxyConfig.AutomaticConfigurationScript,
                AutomaticDetect = proxyConfig.AutomaticDetect,
                Enabled = proxyConfig.Enabled,
                Name = proxyConfig.Name,
                Port = proxyConfig.Port,
                UseAutomaticConfigurationScript = proxyConfig.UseAutomaticConfigurationScript
            };

            foreach (var exception in proxyConfig.Exceptions)
            {
                if (exception.NonEmpty)
                {
                    config.Exceptions.Add(exception.Text);
                }
            }

            foreach (var plugin in ProxyPluginMappers.ToSettingsProxyPluginList(proxyConfig.Plugins))
            {
                config.Plugins.Add(plugin);
            }

            return config;
        }

        internal static Network.Proxy.ProxyConfig ToNetworkProxyConfig(ObservableProxyConfig proxyConfig)
        {
            var proxy = new Network.Proxy.ProxyConfig()
            {
                Address = proxyConfig.Address,
                AutomaticConfigurationScript = proxyConfig.AutomaticConfigurationScript,
                AutomaticDetect = proxyConfig.AutomaticDetect,
                Enabled = proxyConfig.Enabled,
                Port = proxyConfig.Port,
                UseAutomaticConfigurationScript = proxyConfig.UseAutomaticConfigurationScript
            };

            proxy.Exceptions.AddRange(proxyConfig.Exceptions.Where(x => x.NonEmpty).Select(x => x.Text));
            return proxy;
        }

        internal static IEnumerable<ObservableProxyConfig> ToObservableProxyConfigList(IEnumerable<Network.Proxy.ProxyConfig> proxyConfigList) =>
            proxyConfigList.Select(ToObservableProxyConfig);

        internal static IEnumerable<ObservableProxyConfig> ToObservableProxyConfigList(IEnumerable<Settings.Model.ProxyConfig> proxyConfigList) =>
            proxyConfigList.Select(ToObservableProxyConfig);

        internal static IEnumerable<Settings.Model.ProxyConfig> ToSettingsProxyConfigList(IEnumerable<ObservableProxyConfig> proxyConfigList) =>
            proxyConfigList.Select(ToSettingsProxyConfig);

        internal static IEnumerable<Network.Proxy.ProxyConfig> ToNetworkProxyConfigList(IEnumerable<ObservableProxyConfig> proxyConfigList) =>
            proxyConfigList.Select(ToNetworkProxyConfig);
    }
}
