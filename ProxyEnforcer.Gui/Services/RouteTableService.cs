﻿using NLog;
using ProxyEnforcer.Network;
using ProxyEnforcer.Network.RouteTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal class RouteTableService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public RouteTable GetSystemRouteTable() =>
            RouteService.GetSystemRouteTable();

        public void SetSystemRouteTable(RouteTable table)
        {
            var changes = RouteService.SetSystemRouteTable(table);
            LogRouteChanges(changes);

            var errors = changes.CreatedRecordList
                .Aggregate(new List<RouteModificationResult>(),
                    (accumulator, record) =>
                    {
                        accumulator.Add(record);
                        return accumulator;
                    })
                .Any(rec => rec.ErrorCode != 0);

            if (errors)
            {
                throw new ApplicationException("Some route changes failed!");
            }
        }

        private static void LogRouteChanges(RouteModificationResultList result)
        {
            logger.Info("Saving route changes:");

            foreach (var recordResult in result.DeletedRecordList)
            {
                logger.Info($"\tdeleted - errorCode: {recordResult.ErrorCode}; record: {recordResult.Record}");
            }

            foreach (var recordResult in result.CreatedRecordList)
            {
                logger.Info($"\tcreated - errorCode: {recordResult.ErrorCode}; record: {recordResult.Record}");
            }
        }
    }
}
