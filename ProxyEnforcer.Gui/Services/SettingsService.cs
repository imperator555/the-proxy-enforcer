﻿using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.Services.Settings.Repository;

namespace ProxyEnforcer.Gui.Services
{
    internal interface ISettingsService
    {
        string SettingsLocation { get; }

        void ExportPortableSettings(string fileName);
        AppSettings GetSettings();
        AppSettings ImportPortableSettings(string fileName);
        void SetSettings(AppSettings settings);
    }

    internal class SettingsService : ISettingsService
    {
        private readonly SettingsRepository repository = new SettingsRepository();
        private static readonly object syncRoot = new object();

        public string SettingsLocation => SettingsRepository.GetSettingsLocation();

        public AppSettings GetSettings()
        {
            lock (syncRoot)
            {
                return repository.LoadAppSettings();
            }
        }

        public void SetSettings(AppSettings settings)
        {
            lock (syncRoot)
            {
                repository.SaveAppSettings(settings);
            }
        }

        public void ExportPortableSettings(string fileName)
        {
            lock (syncRoot)
            {
                repository.SavePortableSettingsTo(GetSettings(), fileName);
            }
        }

        public AppSettings ImportPortableSettings(string fileName)
        {
            lock (syncRoot)
            {
                return repository.LoadPortableAppSettingsFrom(fileName);
            }
        }
    }
}
