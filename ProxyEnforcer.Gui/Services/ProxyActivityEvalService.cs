﻿using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Services
{
    internal interface IProxyActivityEvalService
    {
        void Evaluate(ObservableProxyConfig proxyConfig);

        void Evaluate(IEnumerable<ObservableProxyConfig> proxyConfigs);
    }

    internal class ProxyActivityEvalService : IProxyActivityEvalService
    {
        private readonly IProxyPluginService proxyPluginService;

        public ProxyActivityEvalService(IProxyPluginService proxyPluginService)
        {
            this.proxyPluginService = proxyPluginService;
        }

        public void Evaluate(ObservableProxyConfig proxyConfig)
        {
            Evaluate(new[] { proxyConfig });
        }

        public void Evaluate(IEnumerable<ObservableProxyConfig> proxyConfigs)
        {
            var enabledPlugins = FindEnabledPlugins(proxyConfigs);
            var currentConfigs = GetCurrentProxyConfigs(enabledPlugins);

            foreach (var proxyConfig in proxyConfigs)
            {
                if (!proxyConfig.EnabledPlugins.Any())
                {
                    proxyConfig.SomeActiveProxiesMatchPreset = false;
                    proxyConfig.AllActiveProxiesMatchPreset = false;

                    continue;
                }

                bool IsPluginActive(ProxyPlugin plugin) => currentConfigs[plugin.Name].Compare(proxyConfig);

                bool anyActive = false;
                bool anyInactive = false;

                foreach (var plugin in proxyConfig.EnabledPlugins)
                {
                    var active = IsPluginActive(plugin);
                    plugin.IsActive = active;

                    if (active)
                    {
                        anyActive = true;
                    }
                    else
                    {
                        anyInactive = true;
                    }
                }

                proxyConfig.SomeActiveProxiesMatchPreset = anyActive && anyInactive;
                proxyConfig.AllActiveProxiesMatchPreset = !anyInactive;
            }
        }

        private IEnumerable<string> FindEnabledPlugins(IEnumerable<ObservableProxyConfig> proxyConfigs)
        {
            return proxyConfigs
                .SelectMany(cfg => cfg.EnabledPlugins)
                .Select(plugin => plugin.Name)
                .Distinct()
                .ToList();
        }

        private IDictionary<string, ObservableProxyConfig> GetCurrentProxyConfigs(IEnumerable<string> enabledPlugins)
        {
            var result = new Dictionary<string, ObservableProxyConfig>();

            foreach (var pluginName in enabledPlugins)
            {
                var plugin = proxyPluginService.GetPlugin(pluginName);
                result.Add(pluginName, plugin.GetActiveProxyConfig());
            }

            return result;
        }
    }
}
