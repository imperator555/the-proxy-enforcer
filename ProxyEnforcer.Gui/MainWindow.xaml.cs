﻿using Hardcodet.Wpf.TaskbarNotification;
using ProxyEnforcer.Gui.Controls.Dialogs;
using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.UiModel;
using System;
using System.Windows;
using System.Windows.Interop;

namespace ProxyEnforcer.Gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static bool firstStart = true;

        private readonly TaskbarIcon taskbarIcon;
        private readonly MainWindowViewModel viewModel;

        public IntPtr Handle => new WindowInteropHelper(this).Handle;

        public MainWindow()
        {
            InitializeComponent();

            taskbarIcon = (TaskbarIcon)FindResource("NotifyIcon");
            viewModel = DataContext as MainWindowViewModel;
            viewModel.OnSystemProxyChanged += HandleProxyConfigChanged;

            OnFirstStart();
        }

        internal void BringToForeground()
        {
            if (WindowState == WindowState.Minimized || Visibility == Visibility.Hidden)
            {
                Show();
                WindowState = WindowState.Normal;
            }

            Activate();
            Topmost = true;
            Topmost = false;
            Focus();
        }

        private void OnFirstStart()
        {
            if (firstStart)
            {
                if (viewModel.StartMinimized)
                {
                    Application.Current.MainWindow.Visibility = Visibility.Collapsed;
                }
            }

            firstStart = false;
        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            switch (viewModel.DefaultCloseAction)
            {
                case CloseAction.Prompt:
                    ClosingWindowDialog cwd = new ClosingWindowDialog
                    {
                        Owner = this
                    };

                    if (cwd.ShowDialog() == true)
                    {
                        if (cwd.DontAskAgain)
                        {
                            viewModel.SaveDefaultCloseAction(cwd.SelectedAction);
                        }

                        if (cwd.SelectedAction == CloseAction.CloseApp)
                        {
                            Application.Current.Shutdown();
                        }
                        else if (cwd.SelectedAction == CloseAction.CloseWindow)
                        {
                            Application.Current.MainWindow.Visibility = Visibility.Collapsed;
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }

                    break;

                case CloseAction.CloseApp:
                    if (!firstStart)
                    {
                        Application.Current.Shutdown();
                    }

                    break;

                case CloseAction.CloseWindow:
                    Application.Current.MainWindow.Visibility = Visibility.Collapsed;
                    e.Cancel = true;
                    break;

                default:
                    return;
            }
        }

        private void HandleProxyConfigChanged(ObservableProxyConfig previous, ObservableProxyConfig current)
        {
            if (viewModel.ShowNotificationOnProxyChange)
            {
                var message = $"script: {current.AutomaticConfigurationScript}\naddress: {current.Address}:{current.Port}";
                taskbarIcon.ShowBalloonTip("Proxy config change", message, default);
            }
        }
    }
}
