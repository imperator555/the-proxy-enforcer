﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.UiModel;
using System;
using System.Windows.Input;

namespace ProxyEnforcer.Gui.Commands
{
    internal class SetProxyCommand : ICommand
    {
        private readonly ISystemProxyService proxyService;

        event EventHandler ICommand.CanExecuteChanged
        {
            add
            {
                ; // ignore
            }

            remove
            {
                ; // ignore
            }
        }

        public SetProxyCommand(ISystemProxyService proxyService)
        {
            this.proxyService = proxyService;
        }

        bool ICommand.CanExecute(object parameter) => true;

        void ICommand.Execute(object parameter) =>
            proxyService.SetSystemProxyConfig(parameter as ObservableProxyConfig);
    }
}