﻿using System;
using System.Windows.Input;

namespace ProxyEnforcer.Gui.Commands
{
    /// <summary>
    /// Simplistic delegate command for the demo.
    /// </summary>
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public Action CommandAction { get; set; }

        public Func<bool> CanExecuteFunc { get; set; }

        public void Execute(object parameter) =>
            CommandAction();

        public bool CanExecute(object parameter) =>
            CanExecuteFunc == null || CanExecuteFunc();
    }
}
