﻿using ProxyEnforcer.Broker;
using Microsoft.Extensions.DependencyInjection;
using ProxyEnforcer.Gui.Commands;
using ProxyEnforcer.Gui.Controls;
using ProxyEnforcer.Gui.Services;

namespace ProxyEnforcer.Gui
{
    internal class RegisteredServices
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IBroker, SimpleBroker>();
            services.AddSingleton<ISystemProxyService, SystemProxyService>();
            services.AddSingleton<ISettingsService, SettingsService>();
            services.AddSingleton<IProxyPluginService, ProxyPluginService>();
            services.AddTransient<IProxyActivityEvalService, ProxyActivityEvalService>();
        }

        public static void RegisterViewModels(IServiceCollection services)
        {
            services.AddTransient<CurrentProxyTabControlViewModel>();
            services.AddTransient<ProxyConfigsTabControlViewModel>();
            services.AddTransient<NotifyIconViewModel>();
            services.AddTransient<PresetListControlViewModel>();
            services.AddTransient<ProxyConfigsTabControlViewModel>();
            services.AddTransient<RouteTableControlViewModel>();
            services.AddTransient<SettingsTabControlViewModel>();
            services.AddTransient<MainWindowViewModel>();
            services.AddTransient<ProxySettingsViewModel>();
        }

        public static void RegisterCommands(IServiceCollection services)
        {
            services.AddGenericFactory<SetProxyCommand>();
        }
    }
}
