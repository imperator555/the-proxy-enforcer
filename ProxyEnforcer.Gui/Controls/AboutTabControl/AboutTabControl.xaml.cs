﻿using NLog;
using ProxyEnforcer.Gui.Controls.Dialogs;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for AboutTabControl.xaml
    /// </summary>
    public partial class AboutTabControl : UserControl
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public AboutTabControl()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types")]
        private void OnSendEmail(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            try
            {
                Process.Start(e.Uri.AbsoluteUri);
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(Application.Current.MainWindow, "Failed to start e-mail application");
                logger.Error(ex);
            }

            e.Handled = true;
        }
    }
}
