﻿using ProxyEnforcer.Gui.UiModel;
using System;

namespace ProxyEnforcer.Gui.Controls
{
    public class SelectedPluginChangedEventArgs : EventArgs
    {
        public SelectedPluginChangedEventArgs(ProxyPlugin plugin)
        {
            Plugin = plugin;
        }

        public ProxyPlugin Plugin { get; }
    }
}
