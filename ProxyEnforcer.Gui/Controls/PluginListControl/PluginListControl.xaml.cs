﻿using NLog;
using ProxyEnforcer.Gui.Controls.Dialogs;
using ProxyEnforcer.Gui.UiModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for PluginListControl.xaml
    /// </summary>
    public partial class PluginListControl : UserControl
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public delegate void SelectedPluginChangedHandler(object sender, SelectedPluginChangedEventArgs plugin);
        public event SelectedPluginChangedHandler OnSelectedPluginChanged;

        public delegate void PluginPropertiesChangedHander(object sender, EventArgs e);
        public event PluginPropertiesChangedHander OnPluginPropertiesChanged;

        private ProxyPlugin selectedPlugin;

        public PluginListControl()
        {
            InitializeComponent();
        }

        public IEnumerable<ProxyPlugin> Plugins
        {
            get => viewModel.Plugins;
            set => viewModel.Plugins = value;
        }

        public bool ShowCheckboxes
        {
            get => viewModel.ShowCheckboxes;
            set => viewModel.ShowCheckboxes = value;
        }

        private void LbPluginList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var lb = (ListBox)sender;
                UnSubscribeFromPluginPropertyChanged();
                selectedPlugin = (ProxyPlugin)lb.SelectedItem;
                SubscribeToPluginPropertyChanged();

                OnSelectedPluginChanged?.Invoke(this, new SelectedPluginChangedEventArgs(selectedPlugin));
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show(Window.GetWindow(this), "An error occured while getting proxy details");
            }
        }

        private void CbPluginSelector_Toggle(object sender, RoutedEventArgs e) =>
            OnPluginPropertiesChanged?.Invoke(this, EventArgs.Empty);

        private void SubscribeToPluginPropertyChanged()
        {
            if (selectedPlugin == null)
            {
                return;
            }

            foreach (var prop in selectedPlugin.Properties)
            {
                prop.PropertyChanged += HandlePluginPropertyChange;
            }
        }

        private void UnSubscribeFromPluginPropertyChanged()
        {
            if (selectedPlugin == null)
            {
                return;
            }

            foreach (var prop in selectedPlugin.Properties)
            {
                prop.PropertyChanged -= HandlePluginPropertyChange;
            }
        }

        private void HandlePluginPropertyChange(object sender, PropertyChangedEventArgs e) =>
            OnPluginPropertiesChanged?.Invoke(this, EventArgs.Empty);
    }
}
