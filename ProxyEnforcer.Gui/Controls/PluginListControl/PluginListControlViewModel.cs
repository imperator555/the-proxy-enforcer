﻿using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Controls
{
    internal class PluginListControlViewModel : ObservableProperty
    {
        private IEnumerable<ProxyPlugin> plugins;
        private bool hideCheckboxes;

        public IEnumerable<ProxyPlugin> Plugins
        {
            get => plugins;

            set
            {
                if (plugins != value)
                {
                    plugins = value;
                    RaisePropertyChanged(nameof(Plugins));
                }
            }
        }

        public bool ShowCheckboxes
        {
            get => hideCheckboxes;

            set
            {
                if (hideCheckboxes != value)
                {
                    hideCheckboxes = value;
                    RaisePropertyChanged(nameof(ShowCheckboxes));
                }
            }
        }
    }
}
