﻿using ProxyEnforcer.Gui.Controls.Dialogs;
using ProxyEnforcer.Network.RouteTables;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for RouteTable.xaml
    /// </summary>
    public partial class RouteTableControl : UserControl
    {
        private readonly RouteTableControlViewModel viewModel;

        public RouteTableControl()
        {
            InitializeComponent();

            viewModel = DataContext as RouteTableControlViewModel;
        }

        private void BtnRefreshClick(object sender, RoutedEventArgs e) =>
            viewModel.RefreshRouteTable();

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            RouteRecordEditorDialog dialog = new RouteRecordEditorDialog
            {
                Title = "Add new route"
            };

            dialog.ShowDialog();

            if (dialog.DialogResult == true)
            {
                viewModel.AddRoute(dialog.RouteRecord);
                viewModel.MarkAsChanged();
            }
        }

        private void BtnEditClick(object sender, RoutedEventArgs e)
        {
            if (lvRouteRecords.SelectedItems.Count > 0)
            {
                var selected = lvRouteRecords.SelectedItems[0] as RouteRecord;
                lvRouteRecords.UnselectAll();

                if (CheckProtocol(selected))
                {
                    RouteRecordEditorDialog dialog = new RouteRecordEditorDialog(selected)
                    {
                        Title = "Edit route",
                    };

                    dialog.ShowDialog();

                    if (dialog.DialogResult == true)
                    {
                        viewModel.MarkAsChanged();
                        CollectionViewSource.GetDefaultView(lvRouteRecords.ItemsSource).Refresh();
                    }

                    dialog.Close();
                }
            }
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            if (lvRouteRecords.SelectedItems.Count > 0)
            {
                List<RouteRecord> routeRecords = new List<RouteRecord>();

                foreach (RouteRecord record in lvRouteRecords.SelectedItems)
                {
                    if (CheckProtocol(record))
                    {
                        routeRecords.Add(record);
                    }
                }

                foreach (RouteRecord record in routeRecords)
                {
                    viewModel.Remove(record);
                }
            }
        }

        private void BtnApplyChangesClick(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel.SaveChanges();
            }
            catch (ApplicationException)
            {
                MessageBoxEx.Show(Application.Current.MainWindow, "Failed to apply some route changes, check the log for details!", "Error");
            }
        }

        private void BtnDiscardChangesClick(object sender, RoutedEventArgs e) =>
            viewModel.DiscardChanges();

        private bool CheckProtocol(RouteRecord record)
        {
            if (record.ForwardProtocol == IpForwardProtocol.NetManagement)
            {
                return true;
            }
            else
            {
                MessageBoxEx.Show(Window.GetWindow(this), "Only records with NetManagement protocol can be edited!");
                return false;
            }
        }
    }
}
