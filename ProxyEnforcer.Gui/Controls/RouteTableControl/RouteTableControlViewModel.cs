﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.UiModel;
using ProxyEnforcer.Network.RouteTables;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Controls
{
    internal class RouteTableControlViewModel : ObservableProperty
    {
        private readonly ISettingsService settingsService;
        private readonly RouteTableService routeTableService = new RouteTableService();

        private RouteTable _routeTable;

        public RouteTableControlViewModel(ISettingsService settingsService)
        {
            this.settingsService = settingsService;

            var settings = settingsService.GetSettings();
            _filterLocalRoutes = settings.HidingLocalRoutesOnRouteTableEnabled;

            RefreshRouteTable();
        }

        private bool _dataSetChanged;
        public bool DataSetChanged
        {
            get => _dataSetChanged;

            private set
            {
                if (_dataSetChanged != value)
                {
                    _dataSetChanged = value;
                    RaisePropertyChanged(nameof(DataSetChanged));
                    RaisePropertyChanged(nameof(RoutesCollection));
                }
            }
        }

        private bool _filterLocalRoutes;
        public bool FilterLocalRoutes
        {
            get => _filterLocalRoutes;

            set
            {
                if (_filterLocalRoutes != value)
                {
                    _filterLocalRoutes = value;
                    RaisePropertyChanged(nameof(RoutesCollection));

                    var settings = settingsService.GetSettings();
                    settings.HidingLocalRoutesOnRouteTableEnabled = _filterLocalRoutes;
                    settingsService.SetSettings(settings);
                }
            }
        }

        public IEnumerable<RouteRecord> RoutesCollection =>
            _routeTable.RouteRecords.Where(rec =>
            {
                return !FilterLocalRoutes || rec.ForwardProtocol != IpForwardProtocol.Local;
            });

        public static bool IsRunAsAdminWarningVisible => !Utils.IsUserElevated();

        internal void RefreshRouteTable()
        {
            _routeTable = routeTableService.GetSystemRouteTable();
            RaisePropertyChanged(nameof(RoutesCollection));
        }

        internal void Remove(RouteRecord record)
        {
            _routeTable.Remove(record);
            DataSetChanged = true;
            RaisePropertyChanged(nameof(RoutesCollection));
        }

        internal void SaveChanges()
        {
            routeTableService.SetSystemRouteTable(_routeTable);
            DataSetChanged = false;
        }

        internal void AddRoute(RouteRecord routeRecord)
        {
            _routeTable.Add(routeRecord);
            RaisePropertyChanged(nameof(RoutesCollection));
            DataSetChanged = true;
        }

        internal void DiscardChanges()
        {
            RefreshRouteTable();
            DataSetChanged = false;
        }

        internal void MarkAsChanged() =>
            DataSetChanged = true;
    }
}
