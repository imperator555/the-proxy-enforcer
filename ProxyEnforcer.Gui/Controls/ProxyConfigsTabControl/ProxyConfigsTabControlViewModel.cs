﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;

namespace ProxyEnforcer.Gui.Controls
{
    internal class ProxyConfigsTabControlViewModel : ObservableProperty
    {
        private readonly ISystemProxyService proxyService;
        private readonly ISettingsService settingsService;
        private readonly IProxyPluginService proxyPluginService;

        private ObservableProxyConfig selectedProxy;
        private UiModel.ProxyPlugin selectedPlugin;
        private bool isSaveEnabled;

        public ObservableProxyConfig SelectedProxy
        {
            get => selectedProxy;
            set
            {
                selectedProxy = value;

                RaisePropertyChanged(nameof(SelectedProxy));
                RaisePropertyChanged(nameof(IsDeleteButtonEnabled));
                RaisePropertyChanged(nameof(IsApplyButtonEnabled));
                RaisePropertyChanged(nameof(IsLoadSystemProxyButtonEnabled));
            }
        }

        public UiModel.ProxyPlugin SelectedPlugin
        {
            get => selectedPlugin;

            set
            {
                selectedPlugin = value;
                RaisePropertyChanged(nameof(SelectedPlugin));
            }
        }

        public bool IsSaveEnabled
        {
            get => isSaveEnabled;

            set
            {
                isSaveEnabled = value;
                RaisePropertyChanged(nameof(IsSaveEnabled));
            }
        }

        public bool IsDeleteButtonEnabled => selectedProxy != null;

        public bool IsApplyButtonEnabled => selectedProxy != null;

        public bool IsLoadSystemProxyButtonEnabled => selectedProxy != null;

        public ProxyConfigsTabControlViewModel(ISystemProxyService proxyService, ISettingsService settingsService, IProxyPluginService proxyPluginService)
        {
            this.proxyService = proxyService;
            this.settingsService = settingsService;
            this.proxyPluginService = proxyPluginService;
        }

        internal void SaveProxyList(IEnumerable<ObservableProxyConfig> presets)
        {
            AppSettings appSettings = settingsService.GetSettings();
            appSettings.ProxyList = ProxyMappers.ToSettingsProxyConfigList(presets);
            settingsService.SetSettings(appSettings);
            IsSaveEnabled = false;

            foreach (var preset in presets)
            {
                preset.MarkSaved(true);
            }
        }

        internal void ExportProxyList(string fileName) =>
            settingsService.ExportPortableSettings(fileName);

        internal void SetSystemProxy(ObservableProxyConfig selectedProxy) =>
            proxyService.SetSystemProxyConfig(selectedProxy);

        internal void ApplyPreset()
        {
            foreach (var plugin in SelectedProxy.Plugins)
            {
                if (plugin.IsEnabled)
                {
                    var pluginInstance = proxyPluginService.GetPlugin(plugin.Name);

                    foreach (var prop in plugin.Properties)
                    {
                        pluginInstance.SetPluginProperty(prop.Name, prop.Value);
                    }

                    pluginInstance.SetProxyConfig(SelectedProxy);
                    pluginInstance.Enable();
                }
            }
        }
    }
}
