﻿using NLog;
using ProxyEnforcer.Gui.Controls.Dialogs;
using System;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for ProxyConfigsTabControl.xaml
    /// </summary>
    public partial class ProxyConfigsTabControl : UserControl
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly ProxyConfigsTabControlViewModel viewModel;

        public ProxyConfigsTabControl()
        {
            InitializeComponent();
            viewModel = DataContext as ProxyConfigsTabControlViewModel;
        }

        private void BtnAddProxy_Click(object sender, RoutedEventArgs e)
        {
            presetListControl.AddNewPreset();
        }

        private void BtnDeleteProxy_Click(object sender, RoutedEventArgs e)
        {
            presetListControl.DeleteSelectedPreset();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel.SaveProxyList(presetListControl.PresetList);
                MessageBoxEx.Show(Window.GetWindow(this), "Proxy list saved successfully!");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show(Window.GetWindow(this), "An error occured while saving the config");
            }
        }

        private void BtnLoadSystemProxy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                presetListControl.LoadSystemProxyIntoSelectedPlugin();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show(Window.GetWindow(this), "An error occured while fetching the system proxy");
            }
        }

        private void BtnApplyPreset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel.ApplyPreset();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show(Window.GetWindow(this), "An error occured while setting proxy");
            }
        }

        private void BtnDiscardConfig_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBoxEx.Show(Window.GetWindow(this), "Are you sure want to discard changes?", "Discard changes", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    presetListControl.Reset();
                    viewModel.IsSaveEnabled = false;
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    MessageBoxEx.Show(Window.GetWindow(this), "An error occured while discarding changes");
                }
            }
        }

        private void BtnImportConfig_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "peExport",
                DefaultExt = ".xml",
                Filter = "Proxy Enforcer config file|*.xml|All files|*.*"
            };

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    presetListControl.ImportPresetsFromFile(dlg.FileName);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    MessageBoxEx.Show(Window.GetWindow(this), "Failed to import config");
                }
            }
        }

        private void BtnExportConfig_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "peExport",
                DefaultExt = ".xml",
                Filter = "Proxy Enforcer config file|*.xml"
            };

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    viewModel.ExportProxyList(dlg.FileName);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    MessageBoxEx.Show(Window.GetWindow(this), "Failed to export config");
                }
            }
        }

        private void PresetListControl_OnSelectedPresetChanged(object sender, SelectedPresetChangedEventArgs e)
        {
            viewModel.SelectedProxy = e.Preset;
            proxySettings.SelectedProxy = e.Preset;
            pluginListControl.Plugins = e.Preset?.Plugins;
        }

        private void PresetListControl_OnPresetPropertiesChanged(object sender, EventArgs e)
        {
            viewModel.IsSaveEnabled = true;
        }

        private void PluginListControl_OnSelectedProxyPluginChanged(object sender, SelectedPluginChangedEventArgs e)
        {
            proxySettings.SelectedPlugin = e.Plugin;
            viewModel.SelectedPlugin = e.Plugin;
        }

        private void PluginListControl_OnPluginPropertiesChanged(object sender, EventArgs e)
        {
            viewModel.IsSaveEnabled = true;
        }
    }
}
