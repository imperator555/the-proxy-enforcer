﻿using ProxyEnforcer.Gui.UiModel;
using ProxyEnforcer.Network;
using ProxyEnforcer.Network.RouteTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace ProxyEnforcer.Gui.Controls.Dialogs
{
    internal class RouteRecordEditorDialogViewModel : ObservableProperty
    {
        private RouteRecord originalRouteRecord;
        private RouteRecord selectedRouteRecord;

        public IEnumerable<NetworkInterface> NetworkInterfaces { get; private set; }

        public IEnumerable<IpForwardType> ForwardTypes { get; private set; }

        public RouteRecord SelectedRouteRecord
        {
            get => selectedRouteRecord;

            set
            {
                selectedRouteRecord = value;
                SaveOriginalRecord();
            }
        }

        public int SelectedInterfaceMetric { get; private set; } = 0;

        public bool IsSelectedRouteRecordChanged =>
            !originalRouteRecord.Equals(selectedRouteRecord);

        public RouteRecordEditorDialogViewModel()
        {
            NetworkInterfaces = NetworkAdapterServices.GetAllNetworkInterfaces().Select(x => x.NetworkInterface);
            ForwardTypes = Enum.GetValues(typeof(IpForwardType)).Cast<IpForwardType>();
            SelectedRouteRecord = new RouteRecord();
        }

        internal void NetworkAdapterUpdated()
        {
            var selectedAdapter = SelectedRouteRecord.ForwardInterface;

            SelectedRouteRecord.ForwardNextHop = selectedAdapter.GetIPProperties().GatewayAddresses.FirstOrDefault()?.Address;
            int metric = NetworkAdapterServices.GetAllNetworkInterfaces()
                .Where(desc => desc.MetricInfo.Index == selectedAdapter.GetIPProperties().GetIPv4Properties().Index)
                .Select(desc => desc.MetricInfo.InterfaceMetric)
                .FirstOrDefault();

            SelectedRouteRecord.ForwardMetric1 = metric;
            SelectedInterfaceMetric = metric;

            RaisePropertyChanged(nameof(SelectedRouteRecord));
            RaisePropertyChanged(nameof(SelectedInterfaceMetric));
        }

        internal void ResetToOriginalRouteRecord()
        {
            SelectedRouteRecord.ForwardDest = originalRouteRecord.ForwardDest;
            SelectedRouteRecord.ForwardInterface = originalRouteRecord.ForwardInterface;
            SelectedRouteRecord.ForwardMask = originalRouteRecord.ForwardMask;
            SelectedRouteRecord.ForwardMetric1 = originalRouteRecord.ForwardMetric1;
            SelectedRouteRecord.ForwardNextHop = originalRouteRecord.ForwardNextHop;
            SelectedRouteRecord.ForwardPolicy = originalRouteRecord.ForwardPolicy;
            SelectedRouteRecord.ForwardType = originalRouteRecord.ForwardType;
        }

        private void SaveOriginalRecord() =>
            originalRouteRecord = new RouteRecord(selectedRouteRecord);
    }
}
