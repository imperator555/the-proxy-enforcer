﻿using ProxyEnforcer.Gui.UiModel;
using ProxyEnforcer.Network.RouteTables;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for RouteRecordEditorDialog.xaml
    /// </summary>
    public partial class RouteRecordEditorDialog : Window
    {
        private bool firstInterfaceChangedEventIgnored = true;

        public RouteRecord RouteRecord
        {
            get
            {
                return viewModel.SelectedRouteRecord;
            }
        }

        public RouteRecordEditorDialog()
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        public RouteRecordEditorDialog(RouteRecord selected) : this()
        {
            viewModel.SelectedRouteRecord = selected;
            firstInterfaceChangedEventIgnored = false;
        }

        private void BtnOkClicked(object sender, RoutedEventArgs e)
        {
            if (this.IsValid())
            {
                DialogResult = viewModel.IsSelectedRouteRecordChanged;
            }
            else
            {
                string errorsFormatted = string.Join("\n", this.GetAllValidationErrors());
                MessageBoxEx.Show(this, $"Fix the errors on the form and try again!\n\n{ errorsFormatted }", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void BtnCancelClicked(object sender, RoutedEventArgs e)
        {
            viewModel.ResetToOriginalRouteRecord();
            DialogResult = false;
        }

        private void CbbInterfaceSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (firstInterfaceChangedEventIgnored)
            {
                viewModel.NetworkAdapterUpdated();
            }

            firstInterfaceChangedEventIgnored = true;
        }
    }
}
