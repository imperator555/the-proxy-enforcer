﻿using ProxyEnforcer.Gui.Services.Settings.Model;
using System.Windows;

namespace ProxyEnforcer.Gui.Controls.Dialogs
{
    /// <summary>
    /// Interaction logic for ClosingWindowDialog.xaml
    /// </summary>
    public partial class ClosingWindowDialog : Window
    {
        public ClosingWindowDialog()
        {
            InitializeComponent();
        }

        public CloseAction SelectedAction { get; private set; }

        public bool DontAskAgain => (bool)chb_dontAskAgain.IsChecked;

        private void OnYesClick(object sender, RoutedEventArgs e)
        {
            SelectedAction = CloseAction.CloseApp;
            DialogResult = true;
        }

        private void OnNoClick(object sender, RoutedEventArgs e)
        {
            SelectedAction = CloseAction.CloseWindow;
            DialogResult = true;
        }

        private void OnCancelClick(object sender, RoutedEventArgs e) =>
            DialogResult = false;
    }
}
