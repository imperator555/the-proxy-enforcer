﻿using ProxyEnforcer.Gui.Commands;
using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.UiModel;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Provides bindable properties and commands for the NotifyIcon. In this sample, the
    /// view model is assigned to the NotifyIcon in XAML. Alternatively, the startup routing
    /// in App.xaml.cs could have created this view model, and assigned it to the NotifyIcon.
    /// </summary>
    internal class NotifyIconViewModel : ObservableProperty
    {
        private readonly IGenericFactory<SetProxyCommand> setProxyCommandFactory;

        public NotifyIconViewModel(ISystemProxyService proxyService, IGenericFactory<SetProxyCommand> setProxyCommandFactory, ISettingsService settingsService)
        {
            // TODO unsubscribe?
            proxyService.OnSystemProxyChanged += HandleSystemProxyChanged;
            this.setProxyCommandFactory = setProxyCommandFactory;
            
            Proxies = ProxyMappers.ToObservableProxyConfigList(settingsService.GetSettings().ProxyList).ToObservableCollection();
        }

        public ObservableCollection<ObservableProxyConfig> Proxies { get; }

        /// <summary>
        /// Shows a window, if none is already open.
        /// </summary>
        public static ICommand ShowWindowCommand =>
            new DelegateCommand
            {
                CanExecuteFunc = () => Application.Current.MainWindow.Visibility != Visibility.Visible,
                CommandAction = () => Application.Current.MainWindow.Visibility = Visibility.Visible
            };

        /// <summary>
        /// Hides the main window. This command is only enabled if a window is open.
        /// </summary>
        public static ICommand HideWindowCommand =>
            new DelegateCommand
            {
                CommandAction = () => Application.Current.MainWindow.Visibility = Visibility.Collapsed,
                CanExecuteFunc = () => Application.Current.MainWindow.Visibility != Visibility.Collapsed
            };

        public ICommand SetProxyFromMenuCmd => setProxyCommandFactory.Create();

        /// <summary>
        /// Shuts down the application.
        /// </summary>
        public static ICommand ExitApplicationCommand =>
            new DelegateCommand { CommandAction = () => Application.Current.Shutdown() };

        private void HandleSystemProxyChanged(ObservableProxyConfig _, ObservableProxyConfig current)
        {
            //throw new NotImplementedException();
        }
    }
}
