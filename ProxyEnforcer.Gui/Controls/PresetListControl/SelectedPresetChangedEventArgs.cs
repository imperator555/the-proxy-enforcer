﻿using ProxyEnforcer.Gui.UiModel;
using System;

namespace ProxyEnforcer.Gui.Controls
{
    public class SelectedPresetChangedEventArgs : EventArgs
    {
        public SelectedPresetChangedEventArgs(ObservableProxyConfig preset)
        {
            Preset = preset;
        }

        public ObservableProxyConfig Preset { get; }
    }
}
