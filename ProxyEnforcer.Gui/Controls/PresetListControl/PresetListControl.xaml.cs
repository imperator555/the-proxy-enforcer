﻿using ProxyEnforcer.Gui.UiModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using static ProxyEnforcer.Gui.Controls.Delegates;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for ProxyConfigListControl.xaml
    /// </summary>
    public partial class PresetListControl : UserControl
    {
        private readonly object syncroot = new object();

        private readonly PresetListControlViewModel viewModel;

        public delegate void SelectedPresetChangedHandler(object sender, SelectedPresetChangedEventArgs e);
        public event SelectedPresetChangedHandler OnSelectredPresetChanged;

        public event PresetPropertiesChangedHandler OnPresetPropertiesChanged
        {
            add
            {
                lock (syncroot)
                {
                    viewModel.OnPresetPropertiesChanged += value;
                }
            }

            remove
            {
                lock (syncroot)
                {
                    viewModel.OnPresetPropertiesChanged -= value;
                }
            }
        }

        public PresetListControl()
        {
            InitializeComponent();

            viewModel = DataContext as PresetListControlViewModel;
        }

        public ObservableProxyConfig SelectedPreset => viewModel.SelectedProxy;

        public ObservableCollection<ObservableProxyConfig> PresetList => viewModel.Presets;

        internal void AddNewPreset()
        {
            viewModel.AddNewProxy();
            OnSelectredPresetChanged?.Invoke(this, new SelectedPresetChangedEventArgs(viewModel.SelectedProxy));
        }

        internal void DeleteSelectedPreset()
        {
            viewModel.DeleteSelectedPreset();
        }

        internal void LoadSystemProxyIntoSelectedPlugin()
        {
            viewModel.LoadSystemProxyIntoSelectedPlugin();
        }

        internal void Reset()
        {
            viewModel.Reset();
        }

        private void LbProxyList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lb = (ListBox)sender;
            var selected = (ObservableProxyConfig)lb.SelectedItem;

            OnSelectredPresetChanged?.Invoke(this, new SelectedPresetChangedEventArgs(selected));
        }

        internal void ImportPresetsFromFile(string fileName)
        {
            viewModel.ImportPresetsFromFile(fileName);
        }
    }
}
