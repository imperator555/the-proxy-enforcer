﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.UiModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using static ProxyEnforcer.Gui.Controls.Delegates;

namespace ProxyEnforcer.Gui.Controls
{
    internal class PresetListControlViewModel : ObservableProperty
    {
        private readonly ISystemProxyService proxyService;
        private readonly ISettingsService settingsService;
        private readonly IProxyPluginService proxyPluginService;
        private readonly IProxyActivityEvalService proxyActivityEvalService;

        private ObservableProxyConfig selectedProxy;

        public event PresetPropertiesChangedHandler OnPresetPropertiesChanged;

        public PresetListControlViewModel(
            ISystemProxyService proxyService, 
            ISettingsService settingsService, 
            IProxyPluginService proxyPluginService, 
            IProxyActivityEvalService proxyActivityEvalService)
        {
            this.proxyService = proxyService;
            this.settingsService = settingsService;
            this.proxyPluginService = proxyPluginService;
            this.proxyActivityEvalService = proxyActivityEvalService;

            this.proxyService.OnSystemProxyChanged += HandleSystemProxyChanged;

            InitProxyList();
        }

        public ObservableCollection<ObservableProxyConfig> Presets { get; private set; }

        public ObservableProxyConfig SelectedProxy
        {
            get => selectedProxy;

            set
            {
                if (selectedProxy != null)
                {
                    selectedProxy.PropertyChanged -= HandleSelectedProxyPropertyChanged;
                    selectedProxy.Exceptions.CollectionChanged -= HandleSelectedProxyExceptionsChanged;
                }

                selectedProxy = value;

                if (selectedProxy != null)
                {
                    selectedProxy.PropertyChanged += HandleSelectedProxyPropertyChanged;
                    selectedProxy.Exceptions.CollectionChanged += HandleSelectedProxyExceptionsChanged;
                }

                RaisePropertyChanged(nameof(SelectedProxy));
            }
        }

        internal void AddNewProxy()
        {
            string proxyName = "new proxy";
            int proxyCounter = 1;

            foreach (ObservableProxyConfig p in Presets.OrderBy(pr => pr.Name))
            {
                if (p.Name == proxyName)
                {
                    ++proxyCounter;
                    proxyName = $"new proxy_{proxyCounter}";
                }
            }

            ObservableProxyConfig opc = new ObservableProxyConfig() { Name = proxyName };
            InitializePreset(opc);

            Presets.Add(opc);
            SelectedProxy = opc;
        }

        internal void DeleteSelectedPreset()
        {
            Presets.Remove(SelectedProxy);
            SelectedProxy = null;
        }

        internal void LoadSystemProxyIntoSelectedPlugin()
        {
            SelectedProxy.MapProperties(proxyService.GetSystemProxyConfig());
        }

        internal void Reset()
        {
            if (Presets != null)
            {
                Presets.CollectionChanged -= HandleProxyListChanged;
            }

            InitProxyList();
            SelectedProxy = null;
            RaisePropertyChanged(nameof(Presets));
        }

        internal void ImportPresetsFromFile(string fileName)
        {
            settingsService.SetSettings(settingsService.ImportPortableSettings(fileName));
            InitProxyList();
            SelectedProxy = null;
            RaisePropertyChanged(nameof(Presets));
            OnPresetPropertiesChanged?.Invoke(this, System.EventArgs.Empty);
        }

        private void InitProxyList()
        {
            Presets = ProxyMappers.ToObservableProxyConfigList(settingsService.GetSettings().ProxyList).ToObservableCollection();
            Presets.CollectionChanged += HandleProxyListChanged;

            foreach (var pc in Presets)
            {
                InitializePreset(pc);
            }

            proxyActivityEvalService.Evaluate(Presets);

            foreach (var preset in Presets)
            {
                preset.MarkSaved();
            }
        }

        private void HandleProxyListChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(Presets));
            OnPresetPropertiesChanged?.Invoke(this, System.EventArgs.Empty);
        }

        private void HandleSystemProxyChanged(ObservableProxyConfig _, ObservableProxyConfig current)
        {
            proxyActivityEvalService.Evaluate(Presets);

            foreach (var proxy in Presets)
            {
                proxy.IsSetForSystemProxy = proxy.Compare(current);
            }

            AppSettings appSettings = settingsService.GetSettings();
            appSettings.LastActiveProxyName = selectedProxy.Name;
            appSettings.LastSystemProxyConfig = ProxyMappers.ToSettingsProxyConfig(proxyService.GetSystemProxyConfig());
            settingsService.SetSettings(appSettings);
        }

        private void HandleSelectedProxyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSetForSystemProxy")
            {
                OnPresetPropertiesChanged?.Invoke(this, System.EventArgs.Empty);
            }
        }

        private void HandleSelectedProxyExceptionsChanged(object sender, NotifyCollectionChangedEventArgs e) =>
            OnPresetPropertiesChanged?.Invoke(this, System.EventArgs.Empty);

        private void InitializePreset(ObservableProxyConfig preset)
        {
            foreach (var plugin in proxyPluginService.Plugins)
            {
                // check if plugin already saved to settings
                if (preset.Plugins.All(p => p.Name != plugin.GetPluginName()))
                {
                    // no, add plugin
                    preset.Plugins.Add(ProxyPluginMappers.ToUiProxyPlugin(plugin));
                }
                else
                {
                    // yes, check missing properties
                    foreach (var possibleProperty in plugin.GetPluginProperties())
                    {
                        var presetPlugin = preset.Plugins.Where(p => p.Name == plugin.GetPluginName()).FirstOrDefault();

                        if (presetPlugin.Properties.All(ppProp => ppProp.Name != possibleProperty.Name))
                        {
                            presetPlugin.Properties.Add(new UiModel.PluginProperty(possibleProperty.Name, possibleProperty.Value.ToString(), possibleProperty.Type.ToString()));
                        }
                    }
                }
            }

            ObservableProxyConfig system = proxyService.GetSystemProxyConfig();

            if (preset.Compare(system))
            {
                preset.IsSetForSystemProxy = true;
            }
        }
    }
}
