﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.Services.ProxyPlugins;
using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Gui.Controls
{
    internal class CurrentProxyTabControlViewModel : ObservableProperty
    {
        private readonly ISystemProxyService proxyService;
        private readonly ISettingsService settingsService;
        private readonly IProxyPluginService proxyPluginService;

        private bool autorefreshEnabled = true;
        private ObservableProxyConfig currentProxy;
        private ProxyPlugin selectedPlugin;

        public CurrentProxyTabControlViewModel(ISystemProxyService proxyService, ISettingsService settingsService, IProxyPluginService proxyPluginService)
        {
            this.proxyService = proxyService;
            this.settingsService = settingsService;
            this.proxyPluginService = proxyPluginService;

            var settings = settingsService.GetSettings();
            autorefreshEnabled = settings.SystemProxyAutoRefreshEnabled;

            SystemProxy = proxyService.GetSystemProxyConfig();
            proxyService.OnSystemProxyChanged += HandleSystemProxyChanged;
        }

        public ObservableProxyConfig SystemProxy
        {
            get => currentProxy;

            private set
            {
                currentProxy = value;
                RaisePropertyChanged(nameof(SystemProxy));
            }
        }

        public IEnumerable<ProxyPlugin> Plugins => InitPlugins();

        public bool AutoRefreshEnabled
        {
            get => autorefreshEnabled;

            set
            {
                if (autorefreshEnabled != value)
                {
                    autorefreshEnabled = value;
                    RaisePropertyChanged(nameof(AutoRefreshEnabled));

                    var settings = settingsService.GetSettings();
                    settings.SystemProxyAutoRefreshEnabled = autorefreshEnabled;
                    settingsService.SetSettings(settings);

                    if (autorefreshEnabled)
                    {
                        SystemProxy = proxyService.GetSystemProxyConfig();
                    }
                }
            }
        }

        public ProxyPlugin SelectedPlugin
        {
            get => selectedPlugin;

            internal set
            {
                if (selectedPlugin != value)
                {
                    selectedPlugin = value;
                    SystemProxy = GetPluginInstance(value).GetActiveProxyConfig();
                }
            }
        }

        private void HandleSystemProxyChanged(ObservableProxyConfig _, ObservableProxyConfig newConfig)
        {
            if (AutoRefreshEnabled && selectedPlugin?.Name == "System")
            {
                SystemProxy = newConfig;
            }
        }

        private IEnumerable<ProxyPlugin> InitPlugins() =>
            ProxyPluginMappers.ToUiProxyPluginList(proxyPluginService.Plugins);

        private IProxyConfigPlugin GetPluginInstance(ProxyPlugin proxyPlugin) =>
            proxyPluginService.Plugins.Where(pl => pl.GetPluginName() == proxyPlugin.Name).First();
    }
}
