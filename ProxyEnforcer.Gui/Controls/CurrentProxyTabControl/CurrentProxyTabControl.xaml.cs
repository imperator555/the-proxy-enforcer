﻿using NLog;
using ProxyEnforcer.Gui.Controls.Dialogs;
using System;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for SystemProxyTabControl.xaml
    /// </summary>
    public partial class CurrentProxyTabControl : UserControl
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly CurrentProxyTabControlViewModel viewModel;

        public CurrentProxyTabControl()
        {
            InitializeComponent();

            viewModel = DataContext as CurrentProxyTabControlViewModel;

            viewModel.PropertyChanged += HandlePropertyChanged;
            ProxySettings.SelectedProxy = viewModel.SystemProxy;

            pluginListControl.Plugins = viewModel.Plugins;
        }

        private void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(viewModel.SystemProxy))
            {
                ProxySettings.SelectedProxy = viewModel.SystemProxy;
            }
        }

        private void PluginListControl_OnSelectedPluginChanged(object sender, SelectedPluginChangedEventArgs plugin)
        {
            try
            {
                viewModel.SelectedPlugin = plugin.Plugin;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show(Window.GetWindow(this), "An error occured while getting proxy details");
            }
        }
    }
}
