﻿using NLog;
using ProxyEnforcer.Gui.Controls.Dialogs;
using System;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for SettingsTabControl.xaml
    /// </summary>
    public partial class SettingsTabControl : UserControl
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly SettingsTabControlViewModel viewModel;

        public SettingsTabControl()
        {
            InitializeComponent();

            viewModel = DataContext as SettingsTabControlViewModel;
        }

        private void BtnOpenSettings_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                viewModel.OpenSettingsInEditor();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBoxEx.Show("An error occured while starting the editor");
            }
        }
    }
}
