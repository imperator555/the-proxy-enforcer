﻿using ProxyEnforcer.Gui.Converters;
using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.UiModel;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProxyEnforcer.Gui.Controls
{
    internal class SettingsTabControlViewModel : ObservableProperty
    {
        private readonly ISettingsService settingsService;

        public SettingsTabControlViewModel(ISettingsService settingsService)
        {
            this.settingsService = settingsService;
        }

        public bool RunOnStartupEnabled
        {
            get => settingsService.GetSettings().RunAtStartup;

            set
            {
                var settings = settingsService.GetSettings();
                settings.RunAtStartup = value;
                settingsService.SetSettings(settings);
                RaisePropertyChanged(nameof(RunOnStartupEnabled));
            }
        }

        public bool StartMinimized
        {
            get => settingsService.GetSettings().StartMinimized;

            set
            {
                AppSettings appSettings = settingsService.GetSettings();

                if (appSettings.StartMinimized != value)
                {
                    appSettings.StartMinimized = value;
                    settingsService.SetSettings(appSettings);
                    RaisePropertyChanged(nameof(StartMinimized));
                }
            }
        }

        public CloseAction DefaultCloseAction
        {
            get => settingsService.GetSettings().DefaultCloseAction;

            set
            {
                AppSettings appSettings = settingsService.GetSettings();

                if (appSettings.DefaultCloseAction != value)
                {
                    appSettings.DefaultCloseAction = value;
                    settingsService.SetSettings(appSettings);
                    RaisePropertyChanged(nameof(DefaultCloseAction));
                }
            }
        }

        public bool ShowNotificationOnProxyChange
        {
            get => settingsService.GetSettings().ShowNotificationOnProxyChange;

            set
            {
                AppSettings appSettings = settingsService.GetSettings();

                if (appSettings.ShowNotificationOnProxyChange != value)
                {
                    appSettings.ShowNotificationOnProxyChange = value;
                    settingsService.SetSettings(appSettings);
                    RaisePropertyChanged(nameof(ShowNotificationOnProxyChange));
                }
            }
        }

        public string EditSettingsApplication
        {
            get => settingsService.GetSettings().SettingsEditorApp;

            set
            {
                AppSettings appSettings = settingsService.GetSettings();

                if (appSettings.SettingsEditorApp != value)
                {
                    appSettings.SettingsEditorApp = value;
                    settingsService.SetSettings(appSettings);
                    RaisePropertyChanged(nameof(EditSettingsApplication));
                }
            }
        }

        public static List<string> PossibleCloseActionValues =>
            CloseActionVisibilityConverter.PossibleValues;

        internal void OpenSettingsInEditor() =>
            OpenFileInEditor(settingsService.SettingsLocation);

        private void OpenFileInEditor(string path)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = settingsService.GetSettings().SettingsEditorApp,
                Arguments = path
            };

            using Process process = new Process { StartInfo = startInfo };
            process.Start();
        }
    }
}
