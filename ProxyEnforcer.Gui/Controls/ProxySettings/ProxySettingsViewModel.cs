﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.UiModel;
using System.ComponentModel;

namespace ProxyEnforcer.Gui.Controls
{
    internal class ProxySettingsViewModel : ObservableProperty
    {
        private readonly IProxyActivityEvalService proxyActivityEvalService;

        private ObservableProxyConfig selectedProxy;
        private ProxyPlugin selectedPlugin;
        private bool editable = true;

        public ObservableProxyConfig SelectedProxy
        {
            get => selectedProxy;

            set
            {
                if (selectedProxy != null)
                {
                    if (selectedProxy.Compare(value) && selectedProxy.Name == value.Name)
                    {
                        return;
                    }

                    selectedProxy.PropertyChanged -= OnSelectedProxyPropertyChange;
                }

                selectedProxy = value;

                if (selectedProxy != null)
                {
                    selectedProxy.PropertyChanged += OnSelectedProxyPropertyChange;
                }

                RaisePropertyChanged(nameof(SelectedProxy));
            }
        }

        public ProxyPlugin SelectedPlugin
        {
            get => selectedPlugin;

            set
            {
                selectedPlugin = value;
                RaisePropertyChanged(nameof(SelectedPlugin));
            }
        }

        public bool Editable
        {
            get => editable;

            set
            {
                editable = value;
                RaisePropertyChanged(nameof(Editable));
            }
        }

        public ProxySettingsViewModel(IProxyActivityEvalService proxyActivityEvalService)
        {
            this.proxyActivityEvalService = proxyActivityEvalService;
        }

        private void OnSelectedProxyPropertyChange(object sender, PropertyChangedEventArgs e)
        {
            proxyActivityEvalService.Evaluate(selectedProxy);

            RaisePropertyChanged(nameof(SelectedProxy));
        }
    }
}
