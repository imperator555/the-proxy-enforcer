﻿using ProxyEnforcer.Gui.UiModel;
using System.Windows;
using System.Windows.Controls;

namespace ProxyEnforcer.Gui.Controls
{
    /// <summary>
    /// Interaction logic for ProxySettings.xaml
    /// </summary>
    public partial class ProxySettings : UserControl
    {
        private readonly ProxySettingsViewModel viewModel;

        public ProxySettings()
        {
            InitializeComponent();

            viewModel = DataContext as ProxySettingsViewModel;
        }

        public ObservableProxyConfig SelectedProxy
        {
            get => viewModel.SelectedProxy;
            set => viewModel.SelectedProxy = value;
        }

        public ProxyPlugin SelectedPlugin
        {
            get => viewModel.SelectedPlugin;
            set => viewModel.SelectedPlugin = value;
        }

        public bool Editable
        {
            get => viewModel.Editable;
            set => viewModel.Editable = value;
        }

        private void BtnDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            Button source = (Button)sender;
            SelectedProxy.RemoveException(CorrespondingItem.GetCorrespondingItem(source));
        }
    }
}
