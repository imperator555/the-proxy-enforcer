﻿using ProxyEnforcer.Gui.Services;
using ProxyEnforcer.Gui.Services.ProxyPlugins.System;
using ProxyEnforcer.Gui.Services.Settings.Model;
using ProxyEnforcer.Gui.UiModel;

namespace ProxyEnforcer.Gui
{
    internal class MainWindowViewModel : ObservableProperty
    {
        private readonly ISettingsService settingsService;
        private readonly ISystemProxyService systemProxyService;

        public CloseAction DefaultCloseAction => settingsService.GetSettings().DefaultCloseAction;

        public event SystemProxyPlugin.SystemProxyChangedEventHandler OnSystemProxyChanged
        {
            add => systemProxyService.OnSystemProxyChanged += value;
            remove => systemProxyService.OnSystemProxyChanged -= value;
        }

        public bool StartMinimized => settingsService.GetSettings().StartMinimized;

        public bool ShowNotificationOnProxyChange => settingsService.GetSettings().ShowNotificationOnProxyChange;

        public MainWindowViewModel(ISettingsService settingsService, ISystemProxyService systemProxyService)
        {
            this.settingsService = settingsService;
            this.systemProxyService = systemProxyService;
        }

        internal void SaveDefaultCloseAction(CloseAction selectedAction)
        {
            AppSettings appSettings = settingsService.GetSettings();
            appSettings.DefaultCloseAction = selectedAction;
            settingsService.SetSettings(appSettings);
        }
    }
}
