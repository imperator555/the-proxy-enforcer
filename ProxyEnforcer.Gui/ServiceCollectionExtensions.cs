﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace ProxyEnforcer.Gui
{
    public static class ServiceCollectionExtensions
    {
        public static void AddGenericFactory<T>(this IServiceCollection services) where T : class
        {
            services.AddTransient<T>();
            services.AddSingleton<Func<T>>(x => () => x.GetService<T>());
            services.AddSingleton<IGenericFactory<T>, GenericFactory<T>>();
        }
    }

    public interface IGenericFactory<T>
    {
        T Create();
    }

    public class GenericFactory<T> : IGenericFactory<T>
    {
        private readonly Func<T> factory;

        public GenericFactory(Func<T> factory)
        {
            this.factory = factory;
        }

        public T Create()
        {
            return factory();
        }
    }
}
