﻿using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ProxyEnforcer.Gui
{
    public partial class App : Application
    {
        private const string UniqueEventName = "e2b494e5-6d44-4576-a71c-b9ee09404a44";
        private const string UniqueMutexName = "10effbc5-ac8a-449f-828d-52cff6ad2d3d";

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private TaskbarIcon notifyIcon;

        private EventWaitHandle eventWaitHandle;
        private Mutex mutex;

        public App()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            DISource.Resolver = (type) => serviceProvider.GetService(type);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            logger.Info("Proxy Enforcer is starting...");

            EnsureSingleRunningInstance();

            AttachUnhandledExceptionHandlers();
            base.OnStartup(e);
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
        }

        protected override void OnExit(ExitEventArgs e)
        {
            logger.Info("Proxy Enforcer is shutting down...");

            notifyIcon.Visibility = Visibility.Collapsed;
            notifyIcon.Icon.Dispose();
            notifyIcon.Dispose(); // the icon would clean up automatically, but this is cleaner

            base.OnExit(e);
        }

        private void ConfigureServices(IServiceCollection services)
        {
            RegisteredServices.RegisterServices(services);
            RegisteredServices.RegisterViewModels(services);
            RegisteredServices.RegisterCommands(services);
        }

        private void EnsureSingleRunningInstance()
        {
            mutex = new Mutex(true, UniqueMutexName, out bool isOwned);
            eventWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset, UniqueEventName);

            if (isOwned)
            {
                // Spawn a thread which will be waiting for our event
                var thread = new Thread(() =>
                    {
                        while (eventWaitHandle.WaitOne())
                        {
                            Current.Dispatcher.BeginInvoke(
                                (Action)(() => ((MainWindow)Current.MainWindow).BringToForeground()));
                        }
                    });

                // It is important mark it as background otherwise it will prevent app from exiting.
                thread.IsBackground = true;

                thread.Start();
                return;
            }

            logger.Info("An another instance is already running, shutting down current instance...");

            // Notify other instance so it could bring itself to foreground.
            eventWaitHandle.Set();

            // Terminate this instance.
            Shutdown();
        }

        private void AttachUnhandledExceptionHandlers()
        {
            // we don't need an unhandled exeption handler if we are running inside
            // the vs.net IDE; it is our "unhandled exception handler" in that case
            if (Debugger.IsAttached)
            {
                return;
            }

            Current.DispatcherUnhandledException += DispatcherUnhandledExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException += DomainUnhandledExceptionHandler;
            TaskScheduler.UnobservedTaskException += UnobservedTaskExceptionHandler;
        }

        private void DispatcherUnhandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            HandleFatalException(e.Exception);
        }

        private void DomainUnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            HandleFatalException((Exception)e.ExceptionObject);
        }

        private void UnobservedTaskExceptionHandler(object sender, UnobservedTaskExceptionEventArgs e)
        {
            HandleFatalException(e.Exception);
        }

        private void HandleFatalException(Exception e)
        {
            mutex.Dispose();
            logger.Fatal(e);
            Current.Shutdown();
        }
    }
}
