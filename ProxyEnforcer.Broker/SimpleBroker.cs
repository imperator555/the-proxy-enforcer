﻿using System;
using System.Collections.Generic;

namespace ProxyEnforcer.Broker
{
    public class SimpleBroker : IBroker
    {
        private readonly IDictionary<Type, SubscriptionContext> subscriptions = new Dictionary<Type, SubscriptionContext>();

        public void Publish<T>(T msg) where T : class
        {
            if (subscriptions.TryGetValue(msg.GetType(), out var context))
            {
                context.Call(msg);
            }
        }

        public ISubscription Subscribe<T>(Callback<T> cb) where T : class
        {
            var callbackType = cb.GetType();
            var messageType = callbackType.GetGenericArguments()[0];

            if (!subscriptions.ContainsKey(messageType))
            {
                subscriptions.Add(messageType, new SubscriptionContext());
            }

            return subscriptions[messageType].AddSubscriber(cb);
        }
    }

    internal class Subscription<T> : ISubscription where T : class
    {
        private readonly Callback<T> subscriber;
        private readonly SubscriptionContext context;

        public Subscription(Callback<T> callback, SubscriptionContext context)
        {
            this.subscriber = callback;
            this.context = context;
        }

        public void Unsubscribe()
        {
            context.RemoveSubscriber(subscriber);
        }
    }

    internal class SubscriptionContext
    {
        private readonly IList<Delegate> subscribers = new List<Delegate>();

        internal ISubscription AddSubscriber<T>(Callback<T> cb) where T : class
        {
            if (!subscribers.Contains(cb))
            {
                subscribers.Add(cb);
            }

            return new Subscription<T>(cb, this);
        }

        internal void RemoveSubscriber<T>(Callback<T> cb) where T : class
        {
            subscribers.Remove(cb);
        }

        internal void Call<T>(T msg) where T : class
        {
            foreach (var subscriber in subscribers)
            {
                subscriber.DynamicInvoke(msg);
            }
        }
    }
}
