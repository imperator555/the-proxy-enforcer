﻿namespace ProxyEnforcer.Broker
{
    public delegate void Callback<T>(T value) where T : class;

    public interface IBroker
    {
        void Publish<T>(T msg) where T : class;
        ISubscription Subscribe<T>(Callback<T> callback) where T : class;
    }

    public interface ISubscription
    {
        void Unsubscribe();
    }
}
