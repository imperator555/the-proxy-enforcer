﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Management.Automation;
using System.Net.Sockets;

namespace ProxyEnforcer.Network.NetworkInterfaces
{
    internal class InterfaceMetricAdapter
    {
        internal static IEnumerable<InterfaceMetricInfo> GetAllMetricInfo()
        {
            List<InterfaceMetricInfo> result = new List<InterfaceMetricInfo>();

            PowerShell ps = PowerShell.Create();
            ps.AddCommand("Set-ExecutionPolicy");
            ps.AddParameter("ExecutionPolicy", "RemoteSigned");
            ps.AddParameter("Scope", "Process");
            ps.Invoke();
            ps.AddCommand("Get-NetIPInterface");
            ps.AddParameter("AddressFamily", "IPv4");
            Collection<PSObject> psObjects = ps.Invoke();

            foreach (PSObject psObject in psObjects)
            {
                InterfaceMetricInfo info = new InterfaceMetricInfo();

                foreach (PSPropertyInfo propInfo in psObject.Properties)
                {
                    switch (propInfo.Name)
                    {
                        case "InterfaceIndex":
                            info.Index = Convert.ToInt32(propInfo.Value);
                            break;

                        case "InterfaceAlias":
                            info.Alias = propInfo.Value as string;
                            break;

                        case "AddressFamily":
                            info.AddressFamily = (AddressFamily)Convert.ToInt32(propInfo.Value);
                            break;

                        case "NlMtu":
                            info.NlMtu = Convert.ToUInt32(propInfo.Value);
                            break;

                        case "InterfaceMetric":
                            info.InterfaceMetric = Convert.ToInt32(propInfo.Value);
                            break;

                        case "AutomaticMetric":
                            info.AutomaticMetric = Convert.ToInt32(propInfo.Value);
                            break;

                        case "Dhcp":
                            info.Dhcp = Convert.ToInt32(propInfo.Value) == 1;
                            break;

                        case "ConnectionState":
                            info.Connected = (ConnectionState)Convert.ToInt32(propInfo.Value);
                            break;

                        default:
                            break;
                    }
                }

                result.Add(info);
            }

            return result;
        }
    }
}
