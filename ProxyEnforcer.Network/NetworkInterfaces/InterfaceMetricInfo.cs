﻿using System.Data;
using System.Net.Sockets;

namespace ProxyEnforcer.Network.NetworkInterfaces
{
    public class InterfaceMetricInfo
    {
        public int Index { get; internal set; }
        public string Alias { get; internal set; }
        public AddressFamily AddressFamily { get; internal set; }
        public uint NlMtu { get; internal set; }
        public int InterfaceMetric { get; internal set; }
        public int AutomaticMetric { get; internal set; }
        public bool Dhcp { get; internal set; }
        public ConnectionState Connected { get; internal set; }
    }
}
