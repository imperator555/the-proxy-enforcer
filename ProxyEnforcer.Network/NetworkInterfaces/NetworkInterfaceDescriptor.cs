﻿using System.Net.NetworkInformation;

namespace ProxyEnforcer.Network.NetworkInterfaces
{
    public class NetworkInterfaceDescriptor
    {
        internal NetworkInterfaceDescriptor(NetworkInterface networkInterface, InterfaceMetricInfo interfaceMetricInfo)
        {
            NetworkInterface = networkInterface;
            MetricInfo = interfaceMetricInfo;
        }

        public NetworkInterface NetworkInterface { get; private set; }

        public InterfaceMetricInfo MetricInfo { get; private set; }
    }
}
