﻿using ProxyEnforcer.Network.RouteTables;

namespace ProxyEnforcer.Network
{
    public static class RouteService
    {
        public static RouteTable GetSystemRouteTable()
        {
            return RouteTableManipulator.GetSystemRouteTable();
        }

        public static RouteModificationResultList SetSystemRouteTable(RouteTable table)
        {
            RouteTableManipulator man = new RouteTableManipulator();
            return man.SetSystemRouteTable(table);
        }
    }
}
