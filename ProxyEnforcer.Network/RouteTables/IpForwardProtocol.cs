﻿namespace ProxyEnforcer.Network.RouteTables
{
    public enum IpForwardProtocol
    {
        /// <summary>
        /// Some other protocol not specified in RFC 1354. 
        /// </summary>
        Other = 1,

        /// <summary>
        /// A local interface. 
        /// </summary>
        Local = 2,

        /// <summary>
        /// A static route. This value is used to identify route information for IP routing set through network 
        /// management such as the Dynamic Host Configuration Protocol (DHCP), the Simple Network Management 
        /// Protocol (SNMP), or by calls to the CreateIpForwardEntry, DeleteIpForwardEntry, or SetIpForwardEntry functions. 
        /// </summary>
        NetManagement = 3,

        /// <summary>
        /// The result of ICMP redirect. 
        /// </summary>
        Icmp = 4,

        /// <summary>
        /// The Exterior Gateway Protocol (EGP), a dynamic routing protocol. 
        /// </summary>
        Egp = 5,

        /// <summary>
        /// The Gateway-to-Gateway Protocol (GGP), a dynamic routing protocol. 
        /// </summary>
        Ggp = 6,

        /// <summary>
        /// The Hellospeak protocol, a dynamic routing protocol. This is a historical entry no longer in use and was 
        /// an early routing protocol used by the original ARPANET routers that ran special software called the 
        /// Fuzzball routing protocol, sometimes called Hellospeak, as described in RFC 891 and RFC 1305. For more 
        /// information, see http://www.ietf.org/rfc/rfc891.txt and http://www.ietf.org/rfc/rfc1305.txt. 
        /// </summary>
        Hello = 7,

        /// <summary>
        /// The Berkeley Routing Information Protocol (RIP) or RIP-II, a dynamic routing protocol. 
        /// </summary>
        Rip = 8,

        /// <summary>
        /// The Intermediate System-to-Intermediate System (IS-IS) protocol, a dynamic routing protocol. The IS-IS 
        /// protocol was developed for use in the Open Systems Interconnection (OSI) protocol suite. 
        /// </summary>
        IsIs = 9,

        /// <summary>
        /// The End System-to-Intermediate System (ES-IS) protocol, a dynamic routing protocol. The ES-IS protocol 
        /// was developed for use in the Open Systems Interconnection (OSI) protocol suite. 
        /// </summary>
        EsIs = 10,

        /// <summary>
        /// The Cisco Interior Gateway Routing Protocol (IGRP), a dynamic routing protocol. 
        /// </summary>
        Cisco = 11,

        /// <summary>
        /// The Bolt, Beranek, and Newman (BBN) Interior Gateway Protocol (IGP) that used the Shortest Path First (SPF) 
        /// algorithm. This was an early dynamic routing protocol. 
        /// </summary>
        Bbn = 12,

        /// <summary>
        /// The Open Shortest Path First (OSPF) protocol, a dynamic routing protocol. 
        /// </summary>
        Ospf = 13,

        /// <summary>
        /// The Border Gateway Protocol (BGP), a dynamic routing protocol. 
        /// </summary>
        Bgp = 14,

        /// <summary>
        /// A Windows specific entry added originally by a routing protocol, but which is now static. 
        /// </summary>
        NtAutoStatic = 10002,

        /// <summary>
        /// A Windows specific entry added as a static route from the routing user interface or a routing command. 
        /// </summary>
        NtStatic = 10006,

        /// <summary>
        /// A Windows specific entry added as a static route from the routing user interface or a routing command, 
        /// except these routes do not cause Dial On Demand (DOD). 
        /// </summary>
        NtStaticNonDod = 10007
    }
}
