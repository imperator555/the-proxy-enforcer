﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace ProxyEnforcer.Network.RouteTables
{
    public class RouteRecord : IEquatable<RouteRecord>
    {
        public RouteRecord()
        {
            SetDefaultValues();
        }

        public RouteRecord(RouteRecord selectedRouteRecord)
        {
            ForwardDest = selectedRouteRecord.ForwardDest;
            ForwardMask = selectedRouteRecord.ForwardMask;
            ForwardPolicy = selectedRouteRecord.ForwardPolicy;
            ForwardNextHop = selectedRouteRecord.ForwardNextHop;
            ForwardInterface = selectedRouteRecord.ForwardInterface;
            ForwardType = selectedRouteRecord.ForwardType;
            ForwardProtocol = selectedRouteRecord.ForwardProtocol;
            ForwardAge = selectedRouteRecord.ForwardAge;
            ForwardNextHopAS = selectedRouteRecord.ForwardNextHopAS;
            ForwardMetric1 = selectedRouteRecord.ForwardMetric1;
            ForwardMetric2 = selectedRouteRecord.ForwardMetric2;
            ForwardMetric3 = selectedRouteRecord.ForwardMetric3;
            ForwardMetric4 = selectedRouteRecord.ForwardMetric4;
            ForwardMetric5 = selectedRouteRecord.ForwardMetric5;
        }

        internal RouteRecord(IpForwardRow data)
        {
            ForwardDest = new IPAddress(data.forwardDest);
            ForwardMask = new IPAddress(data.forwardMask);
            ForwardPolicy = data.forwardPolicy;
            ForwardNextHop = new IPAddress(data.forwardNextHop);
            ForwardInterface = MapNetworkInterface(data.forwardIfIndex);
            ForwardType = (IpForwardType)data.forwardType;
            ForwardProtocol = (IpForwardProtocol)data.forwardProto;
            ForwardAge = data.forwardAge;
            ForwardNextHopAS = data.forwardNextHopAS;
            ForwardMetric1 = data.forwardMetric1;
            ForwardMetric2 = data.forwardMetric2;
            ForwardMetric3 = data.forwardMetric3;
            ForwardMetric4 = data.forwardMetric4;
            ForwardMetric5 = data.forwardMetric5;
        }

        /// <summary>
        /// The destination IPv4 address of the route. An entry with a IPv4 address of 0.0.0.0 is considered a default route.
        /// This member cannot be set to a multicast (class D) IPv4 address.
        /// </summary>
        public IPAddress ForwardDest { get; set; }

        /// <summary>
        /// The IPv4 subnet mask to use with the destination IPv4 address before being compared to the value in the dwForwardDest member.
        /// The dwForwardMask value should be applied to the destination IPv4 address(logical and operation) before a comparison with
        /// the value in the dwForwardDest member.
        /// </summary>
        public IPAddress ForwardMask { get; set; }

        /// <summary>
        /// The set of conditions that would cause the selection of a multi-path route (the set of next hops for a given destination).
        /// This member is typically in IP TOS format. This encoding of this member is described in RFC 1354. For more information, 
        /// see http://www.ietf.org/rfc/rfc1354.txt.
        /// </summary>
        public int ForwardPolicy { get; set; }

        /// <summary>
        /// For remote routes, the IPv4 address of the next system en route. Otherwise, this member should be an IPv4 address of 0.0.0.0.
        /// </summary>
        public IPAddress ForwardNextHop { get; set; }

        /// <summary>
        /// The index of the local interface through which the next hop of this route should be reached.
        /// </summary>
        public NetworkInterface ForwardInterface { get; set; }

        /// <summary>
        /// The route type as described in RFC 1354. For more information, see http://www.ietf.org/rfc/rfc1354.txt.
        /// </summary>
        public IpForwardType ForwardType { get; set; }

        /// <summary>
        /// The protocol or routing mechanism that generated the route as described in RFC 1354. For more information, 
        /// see http://www.ietf.org/rfc/rfc1354.txt. See Protocol Identifiers for a list of possible protocol identifiers 
        /// used by routing protocols.
        /// </summary>
        public IpForwardProtocol ForwardProtocol { get; private set; }

        /// <summary>
        /// The number of seconds since the route was added or modified in the network routing table.
        /// </summary>
        public int ForwardAge { get; private set; }

        /// <summary>
        /// The autonomous system number of the next hop. When this member is unknown or not relevant to the protocol or routing
        /// mechanism specified in dwForwardProto, this value should be set to zero.
        /// This value is documented in RFC 1354.
        /// </summary>
        public int ForwardNextHopAS { get; private set; }

        /// <summary>
        /// The primary routing metric value for this route. The semantics of this metric are determined by the routing protocol
        /// specified in the dwForwardProto member. If this metric is not used, its value should be set to -1.
        /// This value is documented in in RFC 1354.
        /// </summary>
        public int ForwardMetric1 { get; set; }

        /// <summary>
        /// The primary routing metric value for this route. The semantics of this metric are determined by the routing protocol
        /// specified in the dwForwardProto member. If this metric is not used, its value should be set to -1.
        /// This value is documented in in RFC 1354.
        /// </summary>
        public int ForwardMetric2 { get; private set; }

        /// <summary>
        /// The primary routing metric value for this route. The semantics of this metric are determined by the routing protocol
        /// specified in the dwForwardProto member. If this metric is not used, its value should be set to -1.
        /// This value is documented in in RFC 1354.
        /// </summary>
        public int ForwardMetric3 { get; private set; }

        /// <summary>
        /// The primary routing metric value for this route. The semantics of this metric are determined by the routing protocol
        /// specified in the dwForwardProto member. If this metric is not used, its value should be set to -1.
        /// This value is documented in in RFC 1354.
        /// </summary>
        public int ForwardMetric4 { get; private set; }

        /// <summary>
        /// The primary routing metric value for this route. The semantics of this metric are determined by the routing protocol
        /// specified in the dwForwardProto member. If this metric is not used, its value should be set to -1.
        /// This value is documented in in RFC 1354.
        /// </summary>
        public int ForwardMetric5 { get; private set; }

        public bool Equals(RouteRecord other)
        {
            return Equals(ForwardDest, other.ForwardDest)
                && Equals(ForwardMask, other.ForwardMask)
                && Equals(ForwardPolicy, other.ForwardPolicy)
                && Equals(ForwardNextHop, other.ForwardNextHop)
                && Equals(ForwardInterface?.Id, other.ForwardInterface?.Id)
                && Equals(ForwardType, other.ForwardType)
                && Equals(ForwardProtocol, other.ForwardProtocol)
                && Equals(ForwardNextHopAS, other.ForwardNextHopAS)
                && Equals(ForwardMetric1, other.ForwardMetric1);
        }

        public override bool Equals(object other)
        {
            if (other != null && other is RouteRecord)
            {
                return Equals(other as RouteRecord);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + (ForwardDest != null ? ForwardDest.GetHashCode() : 0);
            hash = hash * 31 + (ForwardMask != null ? ForwardMask.GetHashCode() : 0);
            hash = hash * 31 + ForwardPolicy.GetHashCode();
            hash = hash * 31 + (ForwardNextHop != null ? ForwardNextHop.GetHashCode() : 0);
            hash = hash * 31 + (ForwardInterface != null ? ForwardInterface.Id.GetHashCode() : 0);
            hash = hash * 31 + ForwardType.GetHashCode();
            hash = hash * 31 + ForwardProtocol.GetHashCode();
            hash = hash * 31 + ForwardNextHopAS.GetHashCode();
            hash = hash * 31 + ForwardMetric1.GetHashCode();

            return hash;
        }

        public override string ToString()
        {
            return $"[RouteRecord(ForwardDest: {ForwardDest}; ForwardMask: {ForwardMask}; ForwardPolicy: {ForwardPolicy}; " +
                $"ForwardNextHop: {ForwardNextHop}; ForwardInterface: {ForwardInterface?.Name}; ForwardType: {ForwardType}; " +
                $"ForwardProtocol: {ForwardProtocol}; ForwardMetric: {ForwardMetric1})]";
        }

        internal IpForwardRow ToRawFormat()
        {
            IpForwardRow raw = default;

            raw.forwardDest = IPAddressToInt(ForwardDest);
            raw.forwardMask = IPAddressToInt(ForwardMask);
            raw.forwardPolicy = ForwardPolicy;
            raw.forwardNextHop = IPAddressToInt(ForwardNextHop);
            raw.forwardIfIndex = ForwardInterface.GetIPProperties().GetIPv4Properties().Index;
            raw.forwardType = (int)ForwardType;
            raw.forwardProto = (int)ForwardProtocol;
            raw.forwardAge = ForwardAge;
            raw.forwardNextHopAS = ForwardNextHopAS;
            raw.forwardMetric1 = ForwardMetric1;
            raw.forwardMetric2 = ForwardMetric2;
            raw.forwardMetric3 = ForwardMetric3;
            raw.forwardMetric4 = ForwardMetric4;
            raw.forwardMetric5 = ForwardMetric5;

            return raw;
        }

        private void SetDefaultValues()
        {
            ForwardMask = new IPAddress(new byte[] { 255, 255, 255, 255 });
            ForwardPolicy = 0;
            ForwardNextHop = new IPAddress(new byte[] { 0, 0, 0, 0 });
            ForwardInterface = null;
            ForwardType = IpForwardType.Indirect;
            ForwardProtocol = IpForwardProtocol.NetManagement;
            ForwardAge = 0;
            ForwardNextHopAS = 0;
            ForwardMetric1 = int.MaxValue; // should be larger than the metric of the default gateway
            ForwardMetric2 = 0;
            ForwardMetric3 = 0;
            ForwardMetric4 = 0;
            ForwardMetric5 = 0;
        }

        private NetworkInterface MapNetworkInterface(int forwardIfIndex)
        {
            return NetworkAdapterServices.GetAllNetworkInterfaces()
                .Where(
                    intf =>
                    {
                        return intf.NetworkInterface.GetIPProperties().GetIPv4Properties().Index == forwardIfIndex;
                    })
                .Select(x => x.NetworkInterface)
                .FirstOrDefault();
        }

        private static uint IPAddressToInt(IPAddress address)
        {
            byte[] bytes = address.GetAddressBytes();
            return BitConverter.ToUInt32(bytes, 0);
        }
    }
}
