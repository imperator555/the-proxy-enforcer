﻿using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace ProxyEnforcer.Network.RouteTables
{
    internal class RouteTableManipulator
    {
        public static RouteTable GetSystemRouteTable()
        {
            IntPtr fwdTable = IntPtr.Zero;

            try
            {
                int size = 0;
                _ = GetIpForwardTable(fwdTable, ref size, true);
                fwdTable = Marshal.AllocHGlobal(size);
                _ = GetIpForwardTable(fwdTable, ref size, true);
                IpForwardTable table = Marshal.PtrToStructure<IpForwardTable>(fwdTable);
                return new RouteTable(table, fwdTable);
            }
            finally
            {
                Marshal.FreeHGlobal(fwdTable);
            }
        }

        public RouteModificationResultList SetSystemRouteTable(RouteTable table)
        {
            RouteTableComparer comparer = new RouteTableComparer(GetSystemRouteTable(), table);
            comparer.Compare();

            var result = new RouteModificationResultList();

            foreach (RouteRecord record in comparer.RoutesToDelete.Where(RouteRecordFilter))
            {
                int errorCode = RemoveRouteRecord(record);
                result.AddDeletedRecordResult(record, errorCode);
            }

            foreach (RouteRecord record in comparer.RoutesToAdd.Where(RouteRecordFilter))
            {
                int errorCode = AddNewRouteRecord(record);
                result.AddCreatedRecordResult(record, errorCode);
            }

            return result;
        }

        private static int AddNewRouteRecord(RouteRecord routeRecord)
        {
            IntPtr fwdRow = default;

            try
            {
                var raw = routeRecord.ToRawFormat();
                fwdRow = Marshal.AllocHGlobal(Marshal.SizeOf<IpForwardRow>());
                Marshal.StructureToPtr(raw, fwdRow, true);

                return CreateIpForwardEntry(fwdRow);
            }
            finally
            {
                if (fwdRow != default)
                {
                    Marshal.FreeHGlobal(fwdRow);
                }
            }
        }

        private static int RemoveRouteRecord(RouteRecord routeRecord)
        {
            IntPtr fwdRow = default;

            try
            {
                var raw = routeRecord.ToRawFormat();
                fwdRow = Marshal.AllocHGlobal(Marshal.SizeOf<IpForwardRow>());
                Marshal.StructureToPtr(raw, fwdRow, true);

                return DeleteIpForwardEntry(fwdRow);
            }
            finally
            {
                if (fwdRow != default)
                {
                    Marshal.FreeHGlobal(fwdRow);
                }
            }
        }

        private bool RouteRecordFilter(RouteRecord record) =>
            record.ForwardProtocol == IpForwardProtocol.NetManagement;

        [DllImport("iphlpapi", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetIpForwardTable(IntPtr /*PMIB_IPFORWARDTABLE*/ pIpForwardTable, ref int /*PULONG*/ pdwSize, bool bOrder);

        [DllImport("iphlpapi", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int CreateIpForwardEntry(IntPtr /*PMIB_IPFORWARDROW*/ pIpForwardRow);

        [DllImport("iphlpapi", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int DeleteIpForwardEntry(IntPtr /*PMIB_IPFORWARDROW*/ pIpForwardRow);

        private class ForwardEntryAction
        {
            public Action Action { get; }
            public IpForwardRow Row { get; }

            public ForwardEntryAction(Action action, IpForwardRow row)
            {
                Action = action;
                Row = row;
            }
        }

        private enum Action
        {
            Create,
            Delete
        }
    }
}
