﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace ProxyEnforcer.Network.RouteTables
{
    public class RouteTable
    {
        private readonly List<RouteRecord> routeRecords = new List<RouteRecord>();

        internal RouteTable(IpForwardTable table, IntPtr tablePtr)
        {
            IntPtr p = new IntPtr(tablePtr.ToInt64() + Marshal.SizeOf(table.Size));
            for (int i = 0; i < table.Size; ++i)
            {
                routeRecords.Add(new RouteRecord(Marshal.PtrToStructure<IpForwardRow>(p)));
                p = new IntPtr(p.ToInt64() + Marshal.SizeOf(typeof(IpForwardRow)));
            }
        }

        public ReadOnlyCollection<RouteRecord> RouteRecords =>
            routeRecords.AsReadOnly();

        public void Add(RouteRecord record) =>
            routeRecords.Add(record);

        public void Remove(RouteRecord record) =>
            routeRecords.Remove(record);
    }
}
