﻿using System.Collections.Generic;
using System.Linq;

namespace ProxyEnforcer.Network.RouteTables
{
    internal class RouteTableComparer
    {
        private readonly RouteTable originalRouteTable;
        private readonly RouteTable updatedRouteTable;

        internal List<RouteRecord> RoutesToDelete { get; private set; }
        internal List<RouteRecord> RoutesToAdd { get; private set; }

        internal RouteTableComparer(RouteTable original, RouteTable updated)
        {
            originalRouteTable = original;
            updatedRouteTable = updated;

            RoutesToAdd = new List<RouteRecord>();
            RoutesToDelete = new List<RouteRecord>();
        }

        internal void Compare()
        {
            foreach (RouteRecord updatedRecord in updatedRouteTable.RouteRecords)
            {
                var match = originalRouteTable.RouteRecords.FirstOrDefault(original => original.Equals(updatedRecord));

                if (match == default)
                {
                    RoutesToAdd.Add(updatedRecord);
                }
            }

            foreach (RouteRecord originalRecord in originalRouteTable.RouteRecords)
            {
                var match = updatedRouteTable.RouteRecords.FirstOrDefault(updated => updated.Equals(originalRecord));

                if (match == default)
                {
                    RoutesToDelete.Add(originalRecord);
                }
            }
        }
    }
}