﻿namespace ProxyEnforcer.Network.RouteTables
{
    public class RouteModificationResult
    {
        public RouteRecord Record { get; private set; }
        public int ErrorCode { get; private set; }

        public RouteModificationResult(RouteRecord record, int errorCode)
        {
            Record = record;
            ErrorCode = errorCode;
        }
    }
}
