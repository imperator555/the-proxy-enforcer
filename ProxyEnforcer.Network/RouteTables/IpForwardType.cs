﻿namespace ProxyEnforcer.Network.RouteTables
{
    public enum IpForwardType
    {
        /// <summary>
        /// Some other type not specified in RFC 1354. 
        /// </summary>
        Other = 1,

        /// <summary>
        /// An invalid route. This value can result from a route added by an ICMP redirect. 
        /// </summary>
        Invalid = 2,

        /// <summary>
        /// A local route where the next hop is the final destination (a local interface). 
        /// </summary>
        Direct = 3,

        /// <summary>
        /// The remote route where the next hop is not the final destination (a remote destination).
        /// </summary>
        Indirect = 4
    }
}
