﻿using System.Collections.Generic;

namespace ProxyEnforcer.Network.RouteTables
{
    public class RouteModificationResultList
    {
        public IList<RouteModificationResult> CreatedRecordList { get; private set; } = new List<RouteModificationResult>();
        public IList<RouteModificationResult> DeletedRecordList { get; private set; } = new List<RouteModificationResult>();

        internal void AddDeletedRecordResult(RouteRecord record, int errorCode) =>
            DeletedRecordList.Add(new RouteModificationResult(record, errorCode));

        internal void AddCreatedRecordResult(RouteRecord record, int errorCode) =>
            CreatedRecordList.Add(new RouteModificationResult(record, errorCode));
    }
}
