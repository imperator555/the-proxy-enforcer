﻿using System.Runtime.InteropServices;

namespace ProxyEnforcer.Network.RouteTables
{
    [ComVisible(false)]
    [StructLayout(LayoutKind.Sequential)]
    internal struct IpForwardTable
    {
        public uint Size;

        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct)]
        public IpForwardRow[] Table;
    }
}
