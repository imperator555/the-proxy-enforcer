﻿using System.Runtime.InteropServices;

namespace ProxyEnforcer.Network.RouteTables
{
    [ComVisible(false)]
    [StructLayout(LayoutKind.Sequential)]
    internal struct IpForwardRow
    {
        internal uint /*DWORD*/ forwardDest;
        internal uint /*DWORD*/ forwardMask;
        internal int /*DWORD*/ forwardPolicy;
        internal uint /*DWORD*/ forwardNextHop;
        internal int /*DWORD*/ forwardIfIndex;
        internal int /*DWORD*/ forwardType;
        internal int /*DWORD*/ forwardProto;
        internal int /*DWORD*/ forwardAge;
        internal int /*DWORD*/ forwardNextHopAS;
        internal int /*DWORD*/ forwardMetric1;
        internal int /*DWORD*/ forwardMetric2;
        internal int /*DWORD*/ forwardMetric3;
        internal int /*DWORD*/ forwardMetric4;
        internal int /*DWORD*/ forwardMetric5;
    }
}
