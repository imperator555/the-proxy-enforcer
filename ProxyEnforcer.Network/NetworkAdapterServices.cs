﻿using ProxyEnforcer.Network.NetworkInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace ProxyEnforcer.Network
{
    public static class NetworkAdapterServices
    {
        private static List<NetworkInterfaceDescriptor> cachedAdapters;
        private static DateTime cacheCreationTime;

        public static IEnumerable<NetworkInterfaceDescriptor> GetAllNetworkInterfaces()
        {
            if (IsCacheInvalid())
            {
                var adapters = NetworkInterface.GetAllNetworkInterfaces();
                var metrics = InterfaceMetricAdapter.GetAllMetricInfo();

                cachedAdapters = new List<NetworkInterfaceDescriptor>();

                foreach (var adapter in adapters)
                {
                    var metric = metrics.Where(info => info.Index == adapter.GetIPProperties().GetIPv4Properties().Index).FirstOrDefault();
                    cachedAdapters.Add(new NetworkInterfaceDescriptor(adapter, metric));
                }
            }

            return cachedAdapters;
        }

        private static bool IsCacheInvalid()
        {
            if (cacheCreationTime == default || CacheExpired())
            {
                cacheCreationTime = DateTime.Now;
                return true;
            }

            return false;
        }

        private static bool CacheExpired()
        {
            var elapsed = DateTime.Now.Subtract(cacheCreationTime);

            // cache expires in 1 minute
            return elapsed.TotalSeconds > 60;
        }
    }
}
