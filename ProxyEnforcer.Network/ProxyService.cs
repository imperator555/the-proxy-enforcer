﻿using ProxyEnforcer.Network.Proxy;
using static ProxyEnforcer.Network.Proxy.Delegates;

namespace ProxyEnforcer.Network
{
    public static class ProxyService
    {
        private static readonly ProxyChangeDetector changeDetector = new ProxyChangeDetector();

        public static event ProxyChangedEventHandler ProxyConfigChanged
        {
            add
            {
                changeDetector.ProxyConfigChanged += value;
            }

            remove
            {
                changeDetector.ProxyConfigChanged -= value;
            }
        }

        public static void SetSystemProxyConfig(ProxyConfig p)
        {
            ProxyConfig oldConfig = RegistryProxyManipulator.GetSystemProxyConfig();
            RegistryProxyManipulator.SetSystemProxyConfig(p);

            changeDetector.NotifyProxyChange(oldConfig, p);
        }

        public static ProxyConfig GetSystemProxyConfig() =>
            RegistryProxyManipulator.GetSystemProxyConfig();
    }
}
