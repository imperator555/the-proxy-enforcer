﻿namespace ProxyEnforcer.Network.Proxy
{
    public static class Delegates
    {
        public delegate void ProxyChangedEventHandler(ProxyConfig oldConfig, ProxyConfig newConfig);
    }
}
