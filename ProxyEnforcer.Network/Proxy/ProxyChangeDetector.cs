﻿using System.Threading.Tasks;
using static ProxyEnforcer.Network.Proxy.Delegates;

namespace ProxyEnforcer.Network.Proxy
{
    internal class ProxyChangeDetector
    {
        private readonly object syncRoot = new object();

        private event ProxyChangedEventHandler ProxyConfigChangedInternal;

        private ProxyConfig previousConfig = null;
        private bool runPoller = false;

        internal event ProxyChangedEventHandler ProxyConfigChanged
        {
            add
            {
                lock (syncRoot)
                {

                    if (ProxyConfigChangedInternal == null || ProxyConfigChangedInternal?.GetInvocationList().Length == 0)
                    {
                        StartPolling();
                    }

                    ProxyConfigChangedInternal += value;
                }
            }

            remove
            {
                lock (syncRoot)
                {
                    ProxyConfigChangedInternal -= value;

                    if (ProxyConfigChangedInternal?.GetInvocationList().Length == 0)
                    {
                        StopPolling();
                    }
                }
            }
        }

        internal void NotifyProxyChange(ProxyConfig oldConfig, ProxyConfig newConfig)
        {
            Notify(oldConfig, newConfig);
        }

        private Task StartPolling()
        {
            if (!runPoller)
            {
                runPoller = true;
                return Poll();
            }

            return null;
        }

        private void StopPolling()
        {
            if (runPoller)
            {
                runPoller = false;
                previousConfig = null;
            }
        }

        private async Task Poll()
        {
            while (runPoller)
            {
                if (previousConfig == null)
                {
                    previousConfig = RegistryProxyManipulator.GetSystemProxyConfig();
                }
                else
                {
                    ProxyConfig currentConfig = RegistryProxyManipulator.GetSystemProxyConfig();

                    if (!previousConfig.Equals(currentConfig))
                    {
                        Notify(previousConfig, currentConfig);
                    }
                }

                await Task.Delay(1000);
            }
        }

        private void Notify(ProxyConfig oldConfig, ProxyConfig newConfig)
        {
            ProxyConfigChangedInternal?.Invoke(oldConfig, newConfig);
            previousConfig = newConfig;
        }
    }
}
