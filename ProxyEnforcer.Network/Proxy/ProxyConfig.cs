﻿using System.Collections.Generic;

namespace ProxyEnforcer.Network.Proxy
{
    public class ProxyConfig
    {
        private string address;
        private string autoConfigScript;

        public ProxyConfig()
        {
            Exceptions = new List<string>();
        }

        public bool Enabled { get; set; }

        public bool AutomaticDetect { get; set; }

        public string Address
        {
            get => address ?? string.Empty;
            set => address = value;
        }

        public int? Port { get; set; }

        public bool UseAutomaticConfigurationScript { get; set; }

        public string AutomaticConfigurationScript
        {
            get => autoConfigScript ?? string.Empty;
            set => autoConfigScript = value;
        }

        public List<string> Exceptions { get; }

        public new bool Equals(object other)
        {
            return other != null && other is ProxyConfig && Equals(other as ProxyConfig);
        }

        public bool Equals(ProxyConfig other)
        {
            if (other == null)
            {
                return false;
            }

            return Enabled == other.Enabled &&
                AutomaticDetect == other.AutomaticDetect &&
                AddressWithPort == other.AddressWithPort &&
                UseAutomaticConfigurationScript == other.UseAutomaticConfigurationScript &&
                AutomaticConfigurationScript == other.AutomaticConfigurationScript;

            // TODO add exceptions to equality check
        }

        internal string AddressWithPort
        {
            get
            {
                string server = Address;

                if (Port != null)
                {
                    server += ":" + Port;
                }

                return server;
            }
        }

        internal string ExceptionsFormatted =>
            string.Join(";", Exceptions);
    }
}