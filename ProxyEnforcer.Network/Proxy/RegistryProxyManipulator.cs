﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace ProxyEnforcer.Network.Proxy
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility")]
    internal class RegistryProxyManipulator
    {
        /* based on:
         * https://stackoverflow.com/questions/197725/programmatically-set-browser-proxy-settings-in-c-sharp
         * https://stackoverflow.com/questions/1564627/how-to-set-automatic-configuration-script-for-a-dial-up-connection-programmati
         */

        private const int InternetOptionsSettingsChanged = 39;
        private const int InternetOptionsRefresh = 37;

        private const string InternetSettingsSubkey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
        private const string ConnectionsSubkey = "Connections";
        private const string DefaultConnectionSettingsAttr = "DefaultConnectionSettings";
        private const string ProxyServerAddr = "ProxyServer";
        private const string ProxyEnabledAttr = "ProxyEnable";
        private const string ProxyExceptionsKeyName = "ProxyOverride";

        private const byte ProxyEnabledMask = 2;
        private const byte AutomaticConfigMask = 4;
        private const byte AutoDetectMask = 8;
        private const int ConnectionSettingsIndex = 8;
        private const int DynamicContentStartIndex = 12;

        internal static ProxyConfig GetSystemProxyConfig()
        {
            var proxy = new ProxyConfig()
            {
                Address = GetProxyAddress(),
                Port = GetProxyPort(),
                AutomaticDetect = GetAutoDetect(),
                Enabled = GetEnabled(),
                UseAutomaticConfigurationScript = GetUseAutoConfigScript(),
                AutomaticConfigurationScript = GetAutoConfigScript()
            };

            proxy.Exceptions.AddRange(GetExceptions());
            return proxy;
        }

        internal static void SetSystemProxyConfig(ProxyConfig value)
        {
            UpdateInternetSettingsSubKey(value);
            UpdateDefaultConnectionSettingsAttr(value);
            ApplyChanges();
        }

        [DllImport("wininet.dll")]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);

        private static string GetProxyAddress()
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey);
            string address = (string)registry.GetValue(ProxyServerAddr);

            if (address == null)
            {
                return string.Empty;
            }

            if (address.Contains(":"))
            {
                return address.Remove(address.LastIndexOf(':'));
            }
            else
            {
                return address;
            }
        }

        private static int? GetProxyPort()
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey);
            string address = (string)registry.GetValue(ProxyServerAddr);

            if (address != null && address.Contains(":"))
            {
                return int.Parse(address[(address.LastIndexOf(':') + 1)..], CultureInfo.InvariantCulture);
            }
            else
            {
                return null;
            }
        }

        private static bool GetEnabled()
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey);
            int enabled = (int)registry.GetValue(ProxyEnabledAttr);

            return enabled == 1;
        }

        private static bool GetAutoDetect() =>
            CompareWithMask(GetRawProxySettings(), AutoDetectMask);

        private static bool GetUseAutoConfigScript() =>
            CompareWithMask(GetRawProxySettings(), AutomaticConfigMask);

        private static byte GetRawProxySettings() =>
            GetRawConnectionSettings()[ConnectionSettingsIndex];

        private static byte[] GetRawConnectionSettings()
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey + "\\" + ConnectionsSubkey);
            return (byte[])registry.GetValue(DefaultConnectionSettingsAttr);
        }

        private static void SetRawConnectionSettings(byte[] data)
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey + "\\" + ConnectionsSubkey, true);
            registry.SetValue(DefaultConnectionSettingsAttr, data);
        }

        private static bool CompareWithMask(byte subject, byte mask) =>
            (subject & mask) == mask;

        private static string GetAutoConfigScript()
        {
            byte[] data = GetRawConnectionSettings();
            string script = string.Empty;

            int proxyServerLength = data[DynamicContentStartIndex];
            int nextIndex = DynamicContentStartIndex + 4 + proxyServerLength;

            int additionalInfoLength = data[nextIndex];
            nextIndex = nextIndex + 4 + additionalInfoLength;

            int autoConfigScriptLength = data[nextIndex];

            for (int i = 0; i < autoConfigScriptLength; i++)
            {
                script += (char)data[nextIndex + 4 + i];
            }

            return script;
        }

        private static List<string> GetExceptions()
        {
            List<string> result = new List<string>();

            byte[] data = GetRawConnectionSettings();
            string addInfo = string.Empty;

            int proxyServerLength = data[DynamicContentStartIndex];
            int nextIndex = DynamicContentStartIndex + 4 + proxyServerLength;

            int additionalInfoLength = data[nextIndex];
            nextIndex += 4;

            for (int i = 0; i < additionalInfoLength; i++)
            {
                addInfo += (char)data[nextIndex + i];
            }

            result.AddRange(addInfo.Split(';'));

            return result;
        }

        private static void UpdateInternetSettingsSubKey(ProxyConfig p)
        {
            RegistryKey registry = Registry.CurrentUser.OpenSubKey(InternetSettingsSubkey, true);
            registry.SetValue(ProxyServerAddr, p.AddressWithPort);
            registry.SetValue(ProxyEnabledAttr, p.Enabled ? 1 : 0);

            // clear exceptions
            if (p.Exceptions.Count == 0 && registry.GetValue(ProxyExceptionsKeyName) != null)
            {
                registry.DeleteValue(ProxyExceptionsKeyName);
            }
        }

        private static void UpdateDefaultConnectionSettingsAttr(ProxyConfig p)
        {
            byte[] originalData = GetRawConnectionSettings();
            byte[] newData = new byte[DynamicContentStartIndex + p.AddressWithPort.Length + 4 + 4 + p.AutomaticConfigurationScript.Length + 4 + p.ExceptionsFormatted.Length];
            Array.Copy(originalData, newData, DynamicContentStartIndex);

            SetFlagsInDefConnSettings(p, newData);

            int addressStartIndex = DynamicContentStartIndex;
            int exceptionsStartIndex = addressStartIndex + 4 + p.AddressWithPort.Length;
            int autoConfigScriptStartIndex = exceptionsStartIndex + 4 + p.ExceptionsFormatted.Length;

            SetAddressIntoDefConnSettings(addressStartIndex, p.AddressWithPort, newData);
            SetExceptionsIntoDefConnSettings(exceptionsStartIndex, p.ExceptionsFormatted, newData);
            SetAutoConfigIntoDefConnSettings(autoConfigScriptStartIndex, p.AutomaticConfigurationScript, newData);

            SetRawConnectionSettings(newData);
        }

        private static void SetFlagsInDefConnSettings(ProxyConfig p, byte[] newData)
        {
            byte flagPart = newData[ConnectionSettingsIndex];

            if (p.Enabled)
            {
                flagPart = (byte)(flagPart | ProxyEnabledMask);
            }
            else
            {
                flagPart = (byte)(flagPart & ~ProxyEnabledMask);
            }

            if (p.UseAutomaticConfigurationScript)
            {
                flagPart = (byte)(flagPart | AutomaticConfigMask);
            }
            else
            {
                flagPart = (byte)(flagPart & ~AutomaticConfigMask);
            }

            if (p.AutomaticDetect)
            {
                flagPart = (byte)(flagPart | AutoDetectMask);
            }
            else
            {
                flagPart = (byte)(flagPart & ~AutoDetectMask);
            }

            newData[ConnectionSettingsIndex] = flagPart;
        }

        private static void SetAddressIntoDefConnSettings(int startIndex, string proxyAddress, byte[] data)
        {
            int currentIndex = startIndex;
            data[currentIndex] = Convert.ToByte(proxyAddress.Length);

            for (int i = 0; i < proxyAddress.Length; i++)
            {
                currentIndex = startIndex + 4 + i;
                data[currentIndex] = (byte)proxyAddress.ToCharArray()[i];
            }
        }

        private static void SetExceptionsIntoDefConnSettings(int startIndex, string exceptions, byte[] data)
        {
            int currentIndex = startIndex;
            data[currentIndex] = Convert.ToByte(exceptions.Length);

            for (int i = 0; i < exceptions.Length; i++)
            {
                currentIndex = startIndex + 4 + i;
                data[currentIndex] = (byte)exceptions.ToCharArray()[i];
            }
        }

        private static void SetAutoConfigIntoDefConnSettings(int startIndex, string autoConfigScript, byte[] data)
        {
            int currentIndex = startIndex;
            data[currentIndex] = Convert.ToByte(autoConfigScript.Length);

            for (int i = 0; i < autoConfigScript.Length; i++)
            {
                currentIndex = startIndex + 4 + i;
                data[currentIndex] = (byte)autoConfigScript.ToCharArray()[i];
            }
        }

        private static void ApplyChanges()
        {
            InternetSetOption(IntPtr.Zero, InternetOptionsSettingsChanged, IntPtr.Zero, 0);
            InternetSetOption(IntPtr.Zero, InternetOptionsRefresh, IntPtr.Zero, 0);
        }
    }
}